<?php

if (!session_id()) {
    session_start();
}

require_once "../model/UsuarioModel.php";
require_once '../model/LivroModel.php';

if (isset($_POST['cadastrar'])) {
    UsuarioController::cadastrarUsuario();
}

if (isset($_POST["editarPerfil"])) {
    UsuarioController::cadastrarUsuario();
}

if (isset($_POST["cancelarPapel"])){
    UsuarioController::cancelarPapel($_POST["idUsuario"]);
}

if (isset($_POST['autenticar'])) {
    UsuarioController::autenticarUsuario();
}

if (isset($_POST['esqueciMinhaSenha'])) {
    UsuarioController::esquecerSenha();
}

if (isset($_POST['conferirRand'])) {
    UsuarioController::conferirRand();
}

if (isset($_POST['novaSenha'])) {
    UsuarioController::colocarNovaSenha();
}

if (isset($_POST["tornarAdmin"])) {
    UsuarioController::tornarAdmin($_POST["id"]);
}

class UsuarioController {

    public static function cadastrarUsuario() {

        $nomeCompleto = filter_var($_POST['nomeCompleto'], FILTER_SANITIZE_STRING);
        if (!isset($_POST["editarPerfil"])) {
            $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
        }
        if (!isset($_POST["editarPerfil"])) {
            $senha = filter_var($_POST['senha'], FILTER_SANITIZE_STRING);
        }
        $telefone = filter_var($_POST['telefone'], FILTER_SANITIZE_STRING);
        $celular = filter_var($_POST['celular'], FILTER_SANITIZE_STRING);
        $cpf = filter_var($_POST['cpf'], FILTER_SANITIZE_STRING);
        $cep = filter_var($_POST['cep'], FILTER_SANITIZE_STRING);
        $cidade = filter_var($_POST['cidade'], FILTER_SANITIZE_STRING);
        $uf = filter_var($_POST['uf'], FILTER_SANITIZE_STRING);
        $bairro = filter_var($_POST['bairro'], FILTER_SANITIZE_STRING);
        $rua = filter_var($_POST['rua'], FILTER_SANITIZE_STRING);
        $numeroResidencia = filter_var($_POST['numeroResidencia'], FILTER_SANITIZE_STRING);
        $complemento = filter_var($_POST['complemento'], FILTER_SANITIZE_STRING);

        $usuarioModel = new UsuarioModel();

        if (empty($usuarioModel->buscarUsuarioPorEmail($email)) || (isset($_POST["editarPerfil"]))) {

            $usuarioModel->setNome($nomeCompleto);
            if (!isset($_POST["editarPerfil"])) {
                $usuarioModel->setEmail($email);
                $usuarioModel->setSenha($senha);
            }
            $usuarioModel->setTelefone($telefone);

            //novos atributos
            $usuarioModel->setCelular($celular);
            $usuarioModel->setCpf($cpf);
            $usuarioModel->setCep($cep);
            $usuarioModel->setCidade($cidade);
            $usuarioModel->setUf($uf);
            $usuarioModel->setBairro($bairro);
            $usuarioModel->setRua($rua);
            $usuarioModel->setNumeroResidencia($numeroResidencia);
            $usuarioModel->setComplemento($complemento);

            if (!isset($_POST["editarPerfil"])) {
                $usuarioModel->insert();
                $_SESSION['usuario'] = $usuarioModel->autenticar();
                header("Location: ../view/telaPrincipal.php");
            } else {
                $usuarioModel->update($_POST['idUsuario']);
                $_SESSION["sweet"] = "Editado com sucesso.";
                header("Location: ../view/telaMeuPerfil.php");
            }
        } else {
            $_SESSION['sweet'] = "Email informado já existente.";
            if (!isset($_POST["editarPerfil"])) {
                header("location:../view/telaCadastro.php");
            } else {
                header("location:../view/telaEditarPerfil.php");
            }
        }
    }
    
    public static function cancelarPapel($idUsuario){
        
        $usuarioModel = new UsuarioModel();
        $usuarioModel->cancelarPapel($idUsuario);
        $_SESSION["sweet"] = "Usuário Removido.";
        header("location: ../view/telaGerenciarAdmins.php");
    }

    public static function tornarAdmin($id) 
    {
        $usuarioModel = new UsuarioModel();
        $usuarioModel->tornarAdmin($id);
        
        $usuarioModel->setIdUsuario($id);
        $array = $usuarioModel->mostrarUsuariosPorId();
    }

    public static function mostrarTodosAdmins() {

        $usuarioModel = new UsuarioModel();
        $array = $usuarioModel->mostrarTodosUsuarios();

        foreach ($array as $value) {
            if ($value[14] == 1) {
                $results[] = $value;
            }
        }

        return isset($results) ? $results : null;
        //return $results;
    }

    public static function identificarTipo($idUsuario) {

        $usuarioModel = new UsuarioModel();
        return $usuarioModel->identificarTipo($idUsuario);
    }

    public static function autenticarUsuario() {

        $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
        $senha = filter_var($_POST["senha"], FILTER_SANITIZE_STRING);

        $usuarioModel = new UsuarioModel();
        $usuarioModel->setEmail($email);
        $usuarioModel->setSenha($senha);
        $dadosUsuario = $usuarioModel->autenticar();
        if (!empty($dadosUsuario)) {
            $_SESSION['usuario'] = $dadosUsuario;
            header("Location: ../view/telaPrincipal.php");
        } else {
            if ($email == "admin@admin.admin" && $senha == "admin") {
                $_SESSION['admin'] = "admin";
                header("location:../view/telaTodosLivros.php");
            } else {
                $_SESSION['sweet'] = "Ops! Você inseriu dados incorretos.";
                header("location:../view/telaLogin.php");
            }
        }
    }

    public static function buscarUsuariosPorEmail($email) {

        $usuarioModel = new UsuarioModel();
        return $usuarioModel->buscarUsuarioPorEmail($email);
    }

    public static function mostrarUsuariosPorId($idUsuario) {

        $usuarioModel = new UsuarioModel();
        $usuarioModel->setIdUsuario($idUsuario);
        return $usuarioModel->mostrarUsuariosPorId();
    }

    public static function mostrarTodosUsuarios() {

        $usuarioModel = new UsuarioModel();
        return $usuarioModel->mostrarTodosUsuarios();
    }

    public static function esquecerSenha() {

        $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);

        $restultadoBytes = random_bytes(4);
        $resultadoFinal = bin2hex($restultadoBytes);

        $usuarioModel = new UsuarioModel();
        $usuarioModel->setEmail($email);
        $usuarioModel->salvaRand($resultadoFinal);

        $_SESSION["modal"] = "Disponibilizar um TextField para a digitação do código.";
        $_SESSION["email"] = "$email";
        $_SESSION['sweet'] = "Enviamos um email para você.";
        $usuarioModel->enviarEmail($resultadoFinal);
        //header("location:../view/telaEsqueciMinhaSenha.php");
    }

    public static function conferirRand() {

        $rand = filter_var($_POST["rand"], FILTER_SANITIZE_STRING);

        $usuarioModel = new UsuarioModel();
        $usuarioModel->setEmail($_SESSION["email"]);
        if ($usuarioModel->conferirRand($rand)) {
            $_SESSION["modal"] = "Disponibilizar um TextField para a digitação da nova senha";
            $_SESSION['sweet'] = "O código inserido está correto.";
        } else {
            $_SESSION['sweet'] = "O código inserido está incorreto.";
        }
        header("location:../view/telaEsqueciMinhaSenha.php");
    }

    public static function colocarNovaSenha() {


        $senha = filter_var($_POST["senha"], FILTER_SANITIZE_STRING);

        $usuarioModel = new UsuarioModel();
        $usuarioModel->setEmail($_SESSION["email"]);
        $usuarioModel->setSenha($senha);
        $usuarioModel->colocarNovaSenha();
        $_SESSION['sweet'] = "Você modificou sua senha.";
        header("location:../view/telaLogin.php");
    }

}
