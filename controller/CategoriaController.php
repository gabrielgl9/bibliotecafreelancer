<?php

if (!session_id()) {
    session_start();
}

require_once "../model/CategoriaModel.php";

if (isset($_POST["cadastrar"])) {
    CategoriaController::cadastrarCategoria();
}

if (isset($_POST["editar"])) {
    CategoriaController::editarCategoria();
}

if (isset($_POST["deletarCategoria"])) {
    CategoriaController::deletarCategoria();
}

if (isset($_POST["pesquisar"])) {
    CategoriaController::pesquisar();
}

class CategoriaController {

    public static function cadastrarCategoria() {
        $nome = filter_var($_POST["nome"], FILTER_SANITIZE_STRING);
        $categoriaModel = new CategoriaModel();

        if ($categoriaModel->verificaNomeCategoria($nome)) {

            $categoriaModel->setNome($nome);
            $categoriaModel->cadastrar();
        } else {
            echo "Existente";
        }
    }

    public static function editarCategoria() {
        $nome = filter_var($_POST["nome"], FILTER_SANITIZE_STRING);

        $categoriaModel = new CategoriaModel();
        $categoriaModel->setNome($nome);
        $categoriaModel->setIdCategoria($_POST["id"]);
        $categoriaModel->editar();
    }

    public static function pesquisar() {
        $pesquisar = filter_var($_POST["pesquisar"], FILTER_SANITIZE_STRING);

        $categoriaModel = new CategoriaModel();
        $_SESSION["nomePesquisa"] = $pesquisar;
        $_SESSION["countPesquisa"] = count($categoriaModel->pesquisarSemLimit($pesquisar));
        header("location: ../view/telaGerenciarCategorias.php?pesquisa=1");
    }

    public static function pesquisarPorPagination($pagina) {

        $categoriaModel = new CategoriaModel();
        return $categoriaModel->pesquisar($_SESSION["nomePesquisa"], $pagina);
    }

    public static function mostrarAutoComplete() {
        $categoriaModel = new CategoriaModel();
        return $categoriaModel->mostrarAutoComplete();
    }

    public static function mostrarTodasCategoriasParaCadastro() {
        
        $categoriaModel = new CategoriaModel();
        return $categoriaModel->mostrarAutoComplete();
    }

    public static function mostrarTodasCategorias($pagina) {

        $categoriaModel = new CategoriaModel();
        return $categoriaModel->mostrarTodasCategorias($pagina);
    }

    public static function mostrarCategoriasPorLivro($idLivro) {
        $categoriaModel = new CategoriaModel();
        return $categoriaModel->mostrarCategoriasPorLivro($idLivro);
    }

    public static function deletarCategoria() {

        $idCategoria = filter_var($_POST["idCategoria"], FILTER_SANITIZE_NUMBER_INT);

        $categoriaModel = new CategoriaModel();
        $categoriaModel->setIdCategoria($idCategoria);
        if ($categoriaModel->deletarCategoria()) {
            $_SESSION['sweet'] = "Categoria excluida com sucesso.";
        } else {
            $_SESSION['sweet'] = "Falha ao remover a categoria.";
        }
        header("location:../view/telaGerenciarCategorias.php");
    }

}
