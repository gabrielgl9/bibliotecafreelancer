<?php
require_once 'LivroController.php';
require_once "CategoriaController.php";
$var = $_SESSION['usuario'];
if (isset($_GET["value"])) {

    $array = LivroController::mostrarLivrosPorPesquisa($_GET["value"]);

    $_SESSION["value"] = $_GET["value"];

    if (isset($_GET["pesquisa"])):
        $pesquisa = $_GET["pesquisa"];
    else:
        $pesquisa = 1;
    endif;


    $inicial = ($pesquisa - 1) * 5;
    $final = $inicial + 5;
    $contador = 0;

    if (!empty($array)):
        foreach ($array as $valorPag):
            if ($contador >= $inicial and $contador < $final):
                $arrayPag[] = $valorPag;
            endif;
            $contador++;
        endforeach;
        ?>

        <head>
            <meta charset="UTF-8">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
            <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
            <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
            <title>Inicial</title>
        </head>
        <script>
            $(document).ready(function () {
                $('.modal').modal();
                $('.sidenav').sidenav();
                $(".dropdown-trigger").dropdown();
                $(".collection-header").click(function () {
                    $("tr").hide();
                    $("tr").show();
                });
                $(".collection-item").click(function () {
                    $("tbody tr").hide();
                    var catClick = $(this).attr("name");
                    $($(".categorias")).each(function (idx, el) {
                        if (catClick === $(el).attr("name")) {
                            $("tr").each(function (idx2, el2) {
                                if ($(el2).attr("name") === $(el).attr("title")) {
                                    $("tbody").show();
                                    $(el2).show();
                                }
                            });
                        }
                    });
                });
                $("input.autocomplete").autocomplete({
                    data: {
        <?php
        if (!empty($array)):
            foreach ($array as $valor):
                echo "'$valor[1]' : null,";
            endforeach;
        endif;
        ?>
                    },
                    limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
                    minLength: 1 // The minimum length of the input for the autocomplete to start. Default: 1.
                });
                $("#pesquisa").focus(function () {
                    $(this).keyup(function () {
                        $.get("../controller/recebeAutoComplete.php", {"value": $("#pesquisa").val()}, function (data) {
                            $("#dados").html(data);
                        });
                    });
                });
            });</script>


        <table class="centered responsive-table">
            <thead>
                <tr>
                    <th>Título</th>
                    <th>Autor</th>
                    <th>Estado</th>
                    <th></th>
                    <th></th>

                </tr>
            </thead>

            <tbody>
                <?php
                if (!empty($arrayPag)):
                    $x = 0;
                    foreach ($arrayPag as $val):
                        ?>
                        <tr name="<?= $val[0]; ?>">
                            <td><?php echo $val[1]; ?></td>
                            <td><?php echo $val[2]; ?></td>
                            <td>
                                <?php
                                $disponivel = false;
                                if ($val[10] == 0):
                                    echo "Disponível";
                                    $disponivel = true;
                                endif;
                                if ($val[10] == 1):
                                    echo "Indisponível";
                                endif;
                                if ($val[10] == 2):
                                    echo "Reservado";
                                endif;
                                ?>
                            </td>
                            <td><a class="modal-trigger" href="#modalVisibility<?php echo $val[0]; ?>"><i class="material-icons prefix blue-text">visibility</i></a></td>
                            <?php
                            $dono = UsuarioController::buscarUsuariosPorEmail($val[9]);
                            if ($disponivel and $var[2] != $dono[2]) {
                                $disponivel = false;
                                ?>
                                <td>
                                    <form method="POST" action="../controller/LivroController.php">
                                        <input type="hidden" name="reservar" value="reservar"/>
                                        <input type="hidden" name="idLivro" value="<?php echo $val[0]; ?>"/>
                                        <input type="hidden" name="idUsuario" value="<?php echo $var[0]; ?>"/>
                                        <button class="btn waves-effect waves-light orange white-text" type="submit" name="action">Reservar
                                        </button>
                                    </form>
                                </td>
                            <?php } else { ?>
                                <td></td>
                            <?php }
                            ?>
                            <!-- MODAL VISIBILITY -->
                    <div id="modalVisibility<?php echo $val[0]; ?>" class="modal" name="boi">
                        <div class="modal-content">
                            <div class="row">
                                <div class="col s6" style="border: 1px solid grey">
                                    <h4 class="center"><?php echo $val[1]; ?></h4>
                                    <?php echo "<b>Autor: </b>" . $val[2]; ?> <br>
                                    <?php echo "<b>Edição: </b>" . $val[3]; ?> <br>
                                    <?php echo "<b>Editora: </b>" . $val[4]; ?> <br>
                                    <?php echo "<b>Número de páginas: </b>" . $val[5]; ?> <br>
                                    <?php
                                    echo "<b>Categorias: </b>";
                                    $categorias = CategoriaController::mostrarCategoriasPorLivro($val[0]);
                                    if (!empty($categorias)):
                                        foreach ($categorias as $valorr):
                                            ?>
                                            <div class="categorias" title="<?= $val[0]; ?>" name="<?php echo $valorr[0]; ?>"> <?php echo "♦ " . $valorr[1]; ?></div><?php
                                        endforeach;
                                    endif;
                                    ?>
                                    <?php echo "<b>Disponibilidade: </b>" . $val[6]; ?> <br>
                                    <?php
                                    echo "<b>Estado: </b>";
                                    if ($val[9] == 0) {
                                        echo 'Disponível';
                                    }
                                    if ($val[9] == 1) {
                                        echo 'Indisponível';
                                    }
                                    if ($val[9] == 2) {
                                        echo 'Reservado';
                                    }
                                    ?> <br>
                                    <hr>
                                    <?php echo "<b>Pertence à: </b>" . $dono[1]; ?> <br>
                                    <?php echo "<b>Telefone: </b>" . $dono[4]; ?> <br>
                                    <?php echo "<b>Email: </b>" . $dono[2]; ?> <br>
                                    <?php
                                    $historico = LivroController::mostrarHistoricoPorLivro($val[0]);
                                    if (!empty($historico)) {
                                        echo "<hr><b><center>Histórico do livro</center> </b>";
                                        foreach ($historico as $v) {
                                            if (!empty($v[2])) {
                                                $usuarioHistorico = UsuarioController::mostrarUsuariosPorId($v[1]);
                                                echo "<b>Utilizado por: </b>" . $usuarioHistorico[1] . "<br>";
                                                echo "<b>Email: </b>" . $usuarioHistorico[2] . "<br>";
                                                echo "<b>Data inicial: </b>" . $v[3] . "<br>";
                                                echo "<b>Data da entrega: </b>" . $v[2] . "<br>";
                                                echo "<b>Data limite da entrega: </b>" . $v[4] . "<br><br>";
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                                <div class="col s4 right" style="">
                                    <h4 class="center">Capa do livro</h4>
                                    <?php if (isset($val[11]) && !empty($val[11])) {
                                        ?>

                                        <img src="../upload/<?php echo $val[11]; ?>" class="responsive-img" style="height: 250px; border: 1px solid grey"/>
                                        <?php
                                    } else {
                                        ?>
                                        <div class="modal-content">
                                            <center><img src="../imagens/ExclamacaoPreto.png" style="height: 100px;"/></center><br>
                                            <h4 class="center blue-grey-text">Não encontramos nenhuma imagem da capa deste livro.</h4>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="" class="modal-close waves-effect waves-green btn-flat">Fechar</a>
                        </div>
                    </div>
                </tr>
                <?php
            endforeach;
        endif;
    endif;
    ?>
    </tbody>
    </table>
    <div class="row">
        <?php
        if (count($array) > 0) {
            $numero = ((count($array) - 0.1) / 5);
            $numeroPagina = floor((count($array) - 0.1) / 5);
        } else {
            $numero = 1;
            $numeroPagina = 1;
        }
        //echo $numeroPagina;
        if ($numero > 1) {
            ?>
            <ul class="pagination center">
                <?php if ($pesquisa == 1) { ?>
                    <li class="disabled"><i class="material-icons">chevron_left</i></li>
                <?php } else { ?>
                    <li class="waves-effect"><a href="telaPrincipal.php?pesquisa=<?= $pesquisa - 1; ?>"><i class="material-icons">chevron_left</i></a></li>
                <?php } ?>

                <?php
                for ($x = 0; $x <= $numeroPagina; $x++) {
                    if ($pesquisa == $x + 1) {
                        ?>
                        <li class="active"><a href="#!"><?= $x + 1; ?></a></li>
                    <?php } else { ?>
                        <li class="waves-effect"><a href="telaPrincipal.php?pesquisa=<?= $x + 1; ?>"><?= $x + 1; ?></a></li>
                        <?php
                    }
                }
                if ($numeroPagina + 1 == $pesquisa) {
                    ?>
                    <li class="disabled"><i class="material-icons">chevron_right</i></li>
                <?php } else { ?>
                    <li class="waves-effect"><a href="telaPrincipal.php?pesquisa=<?= $pesquisa + 1; ?>"><i class="material-icons">chevron_right</i></a></li>
                <?php } ?>
            </ul>
        <?php } ?>
    </div>

    <?php
} elseif (isset($_GET["valueEmp"])) {

    $array = LivroController::mostrarEmprestimosPorPesquisa($_GET["valueEmp"]);
    if (!empty($array)):
        ?>
        <table class="centered responsive-table">
            <thead>
                <tr>
                    <th>Título do livro</th>
                    <th>Usuário</th>
                    <th>Data inicial</th>
                    <th>Data a ser entregue</th>
                    <th></th>
                </tr>
            </thead>

            <tbody>
                <?php
                foreach ($array as $value):
                    $dadosUsuario = UsuarioController::mostrarUsuariosPorId($value[0]);
                    $dadosLivro = LivroController::mostrarLivrosPorId($value[1]);
                    ?>
                    <tr>
                        <td><?php echo $dadosLivro[1]; ?></td>
                        <td><?php echo $dadosUsuario[1]; ?></td>
                        <td><?php echo $value[2]; ?></td>
                        <td><?php echo $value[3]; ?></td>
                        <td><a class="modal-trigger" href="#modalVisibility<?php echo $value[1]; ?>"><i class="material-icons prefix blue-text">visibility</i></a></td>
                    </tr>

                    <!-- MODAL VISIBILITY -->
                <div id="modalVisibility<?php echo $value[1]; ?>" class="modal">
                    <div class="modal-content">
                        <?php echo "<h4 class='center blue-grey-text'>" . $dadosLivro[1] . "</h4>"; ?> <br>
                        <?php $val = UsuarioController::mostrarUsuariosPorId($value[0]); ?>
                        <?php echo "<b>Está sendo usado por: </b>" . $val[1]; ?> <br>
                        <?php echo "<b>Email: </b>" . $val[2]; ?> <br>
                        <?php echo "<b>Telefone: </b>" . $val[4]; ?> <br>
                        <?php echo "<b>Celular: </b>" . $val[5]; ?> <br>

                    </div>
                    <div class="modal-footer">
                        <a href="" class="modal-close waves-effect waves-green btn-flat">Fechar</a>
                    </div>
                </div>

                <?php
            endforeach;
            ?>
        </tbody>
        </table>
        <?php
    endif;
}elseif (isset($_GET["valueSeusLivros"])) {

    $array = LivroController::mostrarLivrosPorPesquisa($_GET["valueSeusLivros"]);

    $contador = 0;
    if (!empty($array)):
        foreach ($array as $livros):
            if ($livros[9] == $var[2]):
                $seusLivros[] = $livros;
                $verifica = 1;
            endif;
            $contador++;
        endforeach;
    endif;

    if (!isset($verifica)) {
        $_SESSION["pesquisa"] = "nada";
    } else {
        $_SESSION["pesquisa"] = $seusLivros;
    }
    ?>
    <?php
} elseif (isset($_GET["valueCat"])) {

    $array = LivroController::mostrarLivrosPorCategoria($_GET["valueCat"]);
    $_SESSION["idCat"] = $_GET["valueCat"];
    echo json_encode($array);
    //echo "abc";
    //echo $_GET["valueCat"];
} elseif (isset($_GET["valueCat2"])) {
    ?>
    <script>
        $(document).ready(function () {
            $('.modal').modal();
        });
    </script>
    <?php
    $array = json_decode($_GET["valueCat2"]);

    $inicial = 0;
    $final = $inicial + 5;
    $contador = 0;

    foreach ($array as $valorPag):
        if ($contador >= $inicial and $contador < $final):
            $arrayPag[] = $valorPag;
        endif;
        $contador++;
    endforeach;
    ?>

    <table class = "centered responsive-table">
        <thead>
            <tr>
                <th>Título</th>
                <th>Autor</th>
                <th>Estado</th>
                <th></th>
                <th></th>

            </tr>
        </thead>

        <tbody>
            <?php
            foreach ($arrayPag as $val):
                ?>

                <tr name="<?= $val[1]; ?>">
                    <td><?php echo $val[2]; ?></td>
                    <td><?php echo $val[3]; ?></td>
                    <td>
                        <?php
                        $disponivel = false;
                        if ($val[11] == 0):
                            echo "Disponível";
                            $disponivel = true;
                        endif;
                        if ($val[11] == 1):
                            echo "Indisponível";
                        endif;
                        if ($val[11] == 2):
                            echo "Reservado";
                        endif;
                        ?>
                    </td>
                    <td><a class="modal-trigger" href="#modalVisibility<?php echo $val[1]; ?>"><i class="material-icons prefix blue-text">visibility</i></a></td>
                    <?php
                    $dono = UsuarioController::buscarUsuariosPorEmail($val[10]);
                    if ($disponivel and $var[2] != $dono[2]) {
                        $disponivel = false;
                        ?>
                        <td>
                            <form method="POST" action="../controller/LivroController.php">
                                <input type="hidden" name="reservar" value="reservar"/>
                                <input type="hidden" name="idLivro" value="<?php echo $val[1]; ?>"/>
                                <input type="hidden" name="idUsuario" value="<?php echo $var[0]; ?>"/>
                                <button class="btn waves-effect waves-light orange white-text" type="submit" name="action">Reservar
                                </button>
                            </form>
                        </td>
                    <?php } else { ?>
                        <td></td>
                    <?php }
                    ?>

                    <!-- MODAL VISIBILITY -->
            <div id="modalVisibility<?php echo $val[1]; ?>" class="modal" name="boi">
                <div class="modal-content">
                    <div class="row">
                        <div class="col s6" style="border: 1px solid grey">
                            <h4 class="center"><?php echo $val[2]; ?></h4>
                            <?php echo "<b>Autor: </b>" . $val[3]; ?> <br>
                            <?php echo "<b>Edição: </b>" . $val[4]; ?> <br>
                            <?php echo "<b>Editora: </b>" . $val[5]; ?> <br>
                            <?php echo "<b>Número de páginas: </b>" . $val[6]; ?> <br>
                            <?php
                            echo "<b>Categorias: </b>";
                            $categorias = CategoriaController::mostrarCategoriasPorLivro($val[1]);
                            if (!empty($categorias)):
                                foreach ($categorias as $valorr):
                                    ?>
                                    <div class="categorias" title="<?= $val[1]; ?>" name="<?php echo $valorr[0]; ?>"> <?php echo "♦ " . $valorr[1]; ?></div><?php
                                endforeach;
                            endif;
                            ?>
                            <?php echo "<b>Disponibilidade: </b>" . $val[7]; ?> <br>
                            <?php
                            echo "<b>Estado: </b>";
                            if ($val[10] == 0) {
                                echo 'Disponível';
                            }
                            if ($val[10] == 1) {
                                echo 'Indisponível';
                            }
                            if ($val[10] == 2) {
                                echo 'Reservado';
                            }
                            ?> <br>
                            <hr>
                            <?php echo "<b>Pertence à: </b>" . $dono[1]; ?> <br>
                            <?php echo "<b>Telefone: </b>" . $dono[4]; ?> <br>
                            <?php echo "<b>Email: </b>" . $dono[2]; ?> <br>

                            <?php
                            $historico = LivroController::mostrarHistoricoPorLivro($val[1]);
                            if (!empty($historico)) {
                                echo "<hr><b><center>Histórico do livro</center> </b>";
                                foreach ($historico as $v) {
                                    if (!empty($v[2])) {
                                        $usuarioHistorico = UsuarioController::mostrarUsuariosPorId($v[1]);
                                        echo "<b>Utilizado por: </b>" . $usuarioHistorico[1] . "<br>";
                                        echo "<b>Email: </b>" . $usuarioHistorico[2] . "<br>";
                                        echo "<b>Data inicial: </b>" . $v[3] . "<br>";
                                        echo "<b>Data da entrega: </b>" . $v[2] . "<br>";
                                        echo "<b>Data limite da entrega: </b>" . $v[4] . "<br><br>";
                                    }
                                }
                            }
                            ?>
                        </div>
                        <div class="col s4 right">
                            <?php if (isset($val[12]) && !empty($val[12])) {
                                ?>
                                <h4 class="center">Capa do livro</h4>
                                <img src="../upload/<?php echo $val[12]; ?>" class="responsive-img" style="height: 250px; border: 1px solid grey"/>
                                <?php
                            } else {
                                ?>
                                <div class="modal-content">
                                    <h4 class="center blue-grey-text">Não encontramos nenhuma imagem da capa deste livro.</h4>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="" class="modal-close waves-effect waves-green btn-flat">Fechar</a>
                </div>
            </div>
        </tr>

        <?php
    endforeach;
    ?>
    </tbody>
    </table>
    <div class="row">
        <?php
        if (isset($_GET["var"])) {
            if ($_GET["var"] == 1) {
                
                $pagina = 1;


                if (count($array) > 0) {
                    $numero = ((count($array) - 0.1) / 5);
                    $numeroPagina = floor((count($array) - 0.1) / 5);
                } else {
                    $numero = 1;
                    $numeroPagina = 1;
                }
                //echo $numeroPagina;
                if ($numero > 1) {
                    ?>
                    <ul class="pagination center " id="paginacao">
                        <?php if ($pagina == 1) { ?>
                            <li class="disabled"><i class="material-icons">chevron_left</i></li>
                        <?php } else { ?>
                            <li class="waves-effect"><a href="telaPrincipal.php?categoria=<?= $pagina - 1; ?>"><i class="material-icons">chevron_left</i></a></li>
                        <?php } ?>

                        <?php
                        for ($x = 0; $x <= $numeroPagina; $x++) {
                            if ($pagina == $x + 1) {
                                ?>
                                <li class="active"><a href="#!"><?= $x + 1; ?></a></li>
                            <?php } else { ?>
                                <li class="waves-effect page"><a href="telaPrincipal.php?categoria=<?= $x + 1; ?>"><?= $x + 1; ?></a></li>
                                <?php
                            }
                        }
                        if ($numeroPagina + 1 == $pagina) {
                            ?>
                            <li class="disabled"><i class="material-icons">chevron_right</i></li>
                        <?php } else { ?>
                            <li class="waves-effect"><a href="telaPrincipal.php?categoria=<?= $pagina + 1; ?>"><i class="material-icons">chevron_right</i></a></li>
                        <?php } ?>
                    </ul>
                    <?php
                }
            }
        } elseif (!isset($_GET["pesquisa"])) {

            if (count($array) > 0) {
                $numero = ((count($array) - 0.1) / 5);
                $numeroPagina = floor((count($array) - 0.1) / 5);
            } else {
                $numero = 1;
                $numeroPagina = 1;
            }
            //echo $numeroPagina;
            if ($numero > 1) {
                ?>
                <ul class="pagination center " id="paginacao">
                    <?php if ($pagina == 1) { ?>
                        <li class="disabled"><i class="material-icons">chevron_left</i></li>
                    <?php } else { ?>
                        <li class="waves-effect"><a href="telaPrincipal.php?pagina=<?= $pagina - 1; ?>"><i class="material-icons">chevron_left</i></a></li>
                    <?php } ?>

                    <?php
                    for ($x = 0; $x <= $numeroPagina; $x++) {
                        if ($pagina == $x + 1) {
                            ?>
                            <li class="active"><a href="#!"><?= $x + 1; ?></a></li>
                        <?php } else { ?>
                            <li class="waves-effect page"><a href="telaPrincipal.php?pagina=<?= $x + 1; ?>"><?= $x + 1; ?></a></li>
                            <?php
                        }
                    }
                    if ($numeroPagina + 1 == $pagina) {
                        ?>
                        <li class="disabled"><i class="material-icons">chevron_right</i></li>
                    <?php } else { ?>
                        <li class="waves-effect"><a href="telaPrincipal.php?pagina=<?= $pagina + 1; ?>"><i class="material-icons">chevron_right</i></a></li>
                    <?php } ?>
                </ul>
                <?php
            }
        } else {
            $pagina = $_GET["pesquisa"];
            if (count($array) > 0) {
                $numero = ((count($array) - 0.1) / 5);
                $numeroPagina = floor((count($array) - 0.1) / 5);
            } else {
                $numero = 1;
                $numeroPagina = 1;
            }
            //echo $numeroPagina;
            if ($numero > 1) {
                ?>
                <ul class="pagination center">
                    <?php if ($pagina == 1) { ?>
                        <li class="disabled"><i class="material-icons">chevron_left</i></li>
                    <?php } else { ?>
                        <li class="waves-effect"><a href="telaPrincipal.php?pesquisa=<?= $pagina - 1; ?>"><i class="material-icons">chevron_left</i></a></li>
                    <?php } ?>

                    <?php
                    for ($x = 0; $x <= $numeroPagina; $x++) {
                        if ($pagina == $x + 1) {
                            ?>
                            <li class="active"><a href="#!"><?= $x + 1; ?></a></li>
                        <?php } else { ?>
                            <li class="waves-effect"><a href="telaPrincipal.php?pesquisa=<?= $x + 1; ?>"><?= $x + 1; ?></a></li>
                            <?php
                        }
                    }
                    if ($numeroPagina + 1 == $pagina) {
                        ?>
                        <li class="disabled"><i class="material-icons">chevron_right</i></li>
                    <?php } else { ?>
                        <li class="waves-effect"><a href="telaPrincipal.php?pesquisa=<?= $pagina + 1; ?>"><i class="material-icons">chevron_right</i></a></li>
                        <?php } ?>
                </ul>
                <?php
            }
        }
        ?>
    </div>
    <?php
}
?>


