<?php

if (!session_id()) {
    session_start();
}

require_once "../model/LivroModel.php";
require_once "../model/UsuarioModel.php";
require_once "../controller/UsuarioController.php";
require_once "../model/CategoriaModel.php";


if (isset($_POST['cadastrarLivro'])) {
    LivroController::cadastrarLivro();
}

if (isset($_POST["pesquisarEmprestimos"])) {
    LivroController::mostrarEmprestimosPorPesquisa($_POST["pesquisa"]);
}

if (isset($_POST["pesquisarSeusLivros"])) {
    LivroController::mostrarSeusLivrosPorPesquisa($_POST["pesquisa"], $_POST["email"]);
}

if (isset($_POST['reservar'])) {
    LivroController::reservarLivro();
}

if (isset($_POST["pesquisar"])) {
    LivroController::pesquisarTodosLivros($_POST["pesquisa"]);
}

if (isset($_POST['emprestarLivro'])) {
    LivroController::emprestarLivro();
}

if (isset($_POST["entregarLivro"])) {
    LivroController::entregarLivro();
}

if (isset($_POST['deletarLivro'])) {
    LivroController::deletarLivro();
}

if (isset($_POST['removerLivroReservado'])) {
    LivroController::removerLivroReservado();
}

if (isset($_GET['idLivro'])) {
    LivroController::deletarArquivo($_GET['idLivro']);
}

class LivroController {

    public static function cadastrarLivro() {

        $titulo = filter_var($_POST["titulo"], FILTER_SANITIZE_STRING);
        $autor = filter_var($_POST["autor"], FILTER_SANITIZE_STRING);
        $edicao = filter_var($_POST["edicao"], FILTER_SANITIZE_STRING);
        $editora = filter_var($_POST["editora"], FILTER_SANITIZE_STRING);
        $disponibilidade = filter_var($_POST["disponibilidade"], FILTER_SANITIZE_STRING);
        if (isset($_SESSION["usuario"])) {
            $nomeProp = filter_var($_POST["nomeCompleto"], FILTER_SANITIZE_STRING);
            $emailProp = filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);
            $telefoneProp = filter_var($_POST["telefone"], FILTER_SANITIZE_STRING);
        } else {
            $usuarioPeloAdmin = UsuarioController::mostrarUsuariosPorId($_POST["idUsuario"]);
            $nomeProp = $usuarioPeloAdmin[1];
            $emailProp = $usuarioPeloAdmin[2];
            if (!empty($usuarioPeloAdmin[4])) {
                $telefoneProp = $usuarioPeloAdmin[4];
            } else {
                $telefoneProp = $usuarioPeloAdmin[5];
            }
        }
        $numPag = filter_var($_POST["numPag"], FILTER_SANITIZE_STRING);

        $categoriaModel = new CategoriaModel();

        $livroModel = new LivroModel();
        $livroModel->setTitulo($titulo);
        $livroModel->setAutor($autor);
        $livroModel->setEdicao($edicao);
        $livroModel->setEditora($editora);
        $livroModel->setDisponibilidade($disponibilidade);
        $livroModel->setNumPag($numPag);
        $livroModel->setNomeProp($nomeProp);
        $livroModel->setTelefoneProp($telefoneProp);
        $livroModel->setEmailProp($emailProp);

        if (!isset($_POST['editarLivro'])) {

            $lastId = $livroModel->insert();
            foreach ($_POST["categoria"] as $value) {
                $categoriaModel->salvaRelacionamento($value, $lastId);
            }
            if (isset($_FILES['arquivo']['name']) && !empty($_FILES['arquivo']['name'])) {
                $novoNome = self::concatenaNomeComId($_FILES['arquivo'], $lastId);
                $caminho = '../upload/' . $novoNome;
                $livroModel->colocarImagem($lastId, $novoNome, null);
                if ($_FILES['arquivo']['type'] == "image/gif" || $_FILES['arquivo']['type'] == "image/jpeg" || $_FILES['arquivo']['type'] == "image/jpg" || $_FILES['arquivo']['type'] == "image/png") {
                    if ($_FILES['arquivo']['size'] < 10485760 && move_uploaded_file($_FILES['arquivo']['tmp_name'], $caminho)) {
                        $livroModel->colocarImagem($lastId, $novoNome);
                        $_SESSION['sweet'] = "Livro cadastrado com sucesso.";
                        if (isset($_SESSION["usuario"])) {
                            header("location:../view/telaPrincipal.php");
                        } else {
                            header("location:../view/telaTodosLivros.php");
                        }
                    } else {
                        $_SESSION['sweet'] = "Arquivo muito pesado!";
                        header("location:../view/telaCadastrarLivro.php");
                    }
                } else {
                    $_SESSION['sweet'] = "Arquivo inválido! Você deve enviar uma imagem.";
                    header("location:../view/telaCadastrarLivro.php");
                }
            } else {
                $_SESSION['sweet'] = "Livro cadastrado com sucesso.";
                if (isset($_SESSION["usuario"])) {
                    header("location:../view/telaPrincipal.php");
                } else {
                    header("location:../view/telaTodosLivros.php");
                }
            }
        } else {
            $idLivro = filter_var($_POST["idLivro"], FILTER_SANITIZE_NUMBER_INT);
            $livroModel->setIdLivro($idLivro);
            $lastId = $livroModel->editarLivro();
            $categoriaModel->removerRelacionamentos($lastId);

            foreach ($_POST["categoria"] as $value) {
                $categoriaModel->salvaRelacionamento($value, $lastId);
            }
            if (isset($_FILES['arquivo']['name']) && !empty($_FILES['arquivo']['name'])) {
                $novoNome = self::concatenaNomeComId($_FILES['arquivo'], $lastId);
                $caminho = '../upload/' . $novoNome;
                if ($_FILES['arquivo']['type'] == "image/gif" || $_FILES['arquivo']['type'] == "image/jpeg" || $_FILES['arquivo']['type'] == "image/jpg" || $_FILES['arquivo']['type'] == "image/png") {
                    if ($_FILES['arquivo']['size'] < 10485760 && move_uploaded_file($_FILES['arquivo']['tmp_name'], $caminho)) {
                        $arr = self::mostrarLivrosPorId($idLivro);
                        $livroModel->colocarImagem($lastId, $novoNome, $arr[11]);
                        $_SESSION['sweet'] = "Livro editado com sucesso.";
                        if (isset($_SESSION["usuario"])) {
                            header("location:../view/telaSeusLivros.php");
                        } else {
                            header("location:../view/telaTodosLivros.php");
                        }
                    } else {
                        $_SESSION['sweet'] = "Arquivo muito pesado!";
                        header("location:../view/telaEditarLivro.php");
                    }
                } else {
                    $_SESSION['sweet'] = "Arquivo inválido! Você deve enviar uma imagem.";
                    header("location:../view/telaEditarLivro.php");
                }
            } else {
                $_SESSION['sweet'] = "Livro editado com sucesso.";
                if (isset($_SESSION["usuario"])) {
                    header("location:../view/telaSeusLivros.php");
                } else {
                    header("location:../view/telaTodosLivros.php");
                }
            }
        }
    }

    public static function deletarArquivo($idLivro) {

        $livroModel = new LivroModel();
        $arr = self::mostrarLivrosPorId($idLivro);
        $livroModel->removerArquivo($idLivro, $arr[11]);
        $_SESSION['sweet'] = "Arquivo Removido.";
        header("location:../view/telaEditarLivro.php");
    }

    public static function mostrarHistoricoPorLivro($idLivro) {

        $livroModel = new LivroModel();
        return $livroModel->mostrarhistoricoPorLivro($idLivro);
    }

    public static function mostrarLivrosReservados($idUsuario) {

        $livroModel = new LivroModel();
        return $livroModel->mostrarLivrosReservados($idUsuario);
    }

    public static function mostrarLivrosPorId($idLivro) {

        $livroModel = new LivroModel();
        $livroModel->setIdLivro($idLivro);
        return $livroModel->mostrarLivrosPorId();
    }

    public static function mostrarLivrosPorPesquisa($valor) {

        $livroModel = new LivroModel();
        return $livroModel->mostrarLivrosPorPesquisa($valor);
    }

    public static function mostrarPorPagination($pagina) {

        $livroModel = new LivroModel();
        return $livroModel->mostrarTodosLivrosPorPagination($pagina);
    }

    public static function mostrarTodosLivros() {

        $livroModel = new LivroModel();
        return $livroModel->mostrarTodosLivros();
    }

    public static function mostrarEmprestimos() {

        $livroModel = new LivroModel();
        return $livroModel->mostrarEmprestimos();
    }

    public static function mostrarEmprestimosPorPagination($pagina) {

        $livroModel = new LivroModel();
        return $livroModel->mostrarEmprestimosPorPagination($pagina);
    }

    public static function pesquisarEmprestimosPorPagination($pagina) {

        $livroModel = new LivroModel();
        return $livroModel->pesquisarEmprestimosPorPagination($_SESSION["nomePesquisa"], $pagina);
    }

    public static function mostrarEmprestimosPorPesquisa($valor) {

        $pesquisar = filter_var($valor, FILTER_SANITIZE_STRING);
        $livroModel = new LivroModel();
        $_SESSION["nomePesquisa"] = $pesquisar;
        $_SESSION["countPesquisa"] = count($livroModel->pesquisarEmprestimosSemLimit($pesquisar));
        header("location: ../view/telaGerenciarEmprestimos.php?pesquisa=1");
    }

    public static function mostrarEmprestimosPorId($idUsuario) {

        $livroModel = new LivroModel();
        return $livroModel->mostrarEmprestimosPorId($idUsuario);
    }

    public static function mostrarLivrosEmprestados($nomeCompleto, $email) {

        $livroModel = new LivroModel();
        return $livroModel->mostrarLivrosEmprestados($nomeCompleto, $email);
    }

    public static function mostrarLivrosPorPessoa($nomeCompleto, $email) {

        $livroModel = new LivroModel();
        return $livroModel->mostrarLivrosPorPessoa($nomeCompleto, $email);
    }
    
    public static function mostrarLivrosPorCategoria($categoria){
        
        $livroModel = new LivroModel();
        return $livroModel->mostrarLivrosPorCategoria($categoria);
    }

    public static function pesquisarTodosLivros($valor) {

        $pesquisar = filter_var($valor, FILTER_SANITIZE_STRING);
        $livroModel = new LivroModel();
        $_SESSION["nomePesquisa"] = $pesquisar;
        $_SESSION["countPesquisa"] = count($livroModel->pesquisarSemLimit($pesquisar));
        header("location: ../view/telaTodosLivros.php?pesquisa=1");
    }

    public static function pesquisarTodosLivrosPorPagination($pagina) {

        $livroModel = new LivroModel();
        return $livroModel->pesquisarTodosLivrosPorPagination($_SESSION["nomePesquisa"], $pagina);
    }

    public static function pesquisarSeusLivrosPorPagination($pagina, $email) {

        $livroModel = new LivroModel();
        return $livroModel->pesquisarSeusLivrosPorPagination($_SESSION["nomePesquisa"], $pagina, $email);
    }
    
    public static function mostrarSeusLivrosPorPagination($pagina, $email){
        
        $livroModel = new LivroModel();
        return $livroModel->mostrarSeusLivrosPorPagination($pagina, $email);
    }

    public static function mostrarSeusLivrosPorPesquisa($valor, $email) {

        $pesquisar = filter_var($valor, FILTER_SANITIZE_STRING);
        $livroModel = new LivroModel();
        $_SESSION["nomePesquisa"] = $pesquisar;
        $_SESSION["countPesquisa"] = count($livroModel->pesquisarSeusLivrosSemLimit($pesquisar, $email));
        header("location: ../view/telaSeusLivros.php?pesquisa=1");
    }

    public static function deletarLivro() {

        $idLivro = filter_var($_POST["idLivro"], FILTER_SANITIZE_NUMBER_INT);

        $livroModel = new LivroModel();
        $livroModel->setIdLivro($idLivro);
        $livroModel->deletarLivro();
        $_SESSION['sweet'] = "Livro excluído com sucesso.";
        if (isset($_SESSION["usuario"])) {
            header("location:../view/telaSeusLivros.php");
        } else {
            header("location:../view/telaTodosLivros.php");
        }
    }

    public static function removerLivroReservado() {

        $idLivro = filter_var($_POST["idLivro"], FILTER_SANITIZE_NUMBER_INT);

        $livroModel = new LivroModel();
        $livroModel->setIdLivro($idLivro);
        $livroModel->removerLivroReservado();
        if (!isset($_POST["idUsuario"])) {
            $_SESSION['sweet'] = "Livro removido da lista de reservas.";
            header("location:../view/telaLivrosReservados.php");
        } else {
            $_SESSION['sweet'] = "O empréstimo do livro foi recusado!";
            header("location:../view/telaSeusLivros.php");
        }
    }

    public static function reservarLivro() {

        $idUsuario = filter_var($_POST["idUsuario"], FILTER_SANITIZE_NUMBER_INT);
        $idLivro = filter_var($_POST["idLivro"], FILTER_SANITIZE_NUMBER_INT);

        $livroModel = new LivroModel();
        $contEmprestimo = count($livroModel->mostrarEmprestimosPorId($idUsuario));
        $contReservas = count($livroModel->mostrarLivrosReservados($idUsuario));
        if ($contEmprestimo < 5 and $contReservas < 5) {
            $livroModel->reservarLivro($idUsuario, $idLivro);
            /*
              $livroModel->setIdLivro($idLivro);
              $array = $livroModel->mostrarLivrosPorId();
              $usuarioModel = new UsuarioModel();
              $usuarioModel->enviarEmailComArray($array);
             */
            $_SESSION['sweet'] = "Livro reservado com sucesso.";
        } else {
            $_SESSION['sweet'] = "Você já está com muitas reservas ou empréstimos!";
            //cancele algumas reservas ou entregue os livros que você pegou emprestado
        }
        header("location:../view/telaPrincipal.php");
    }

    public static function mostrarReservaDoLivro($idLivro) {
        $livroModel = new LivroModel();
        return $livroModel->mostrarReservaDoLivro($idLivro);
    }

    public static function entregarLivro() {

        $livroModel = new LivroModel();
        $livroModel->setIdLivro($_POST["idLivro"]);
        $livroModel->removerEmprestimo();
        $_SESSION['sweet'] = "A devolução do livro foi concluída.";
        header("location:../view/telaSeusLivros.php");
    }

    public static function emprestarLivro() {

        $idLivro = filter_var($_POST["idLivro"], FILTER_SANITIZE_NUMBER_INT);
        $idUsuario = filter_var($_POST["idUsuario"], FILTER_SANITIZE_NUMBER_INT);

        $livroModel = new LivroModel();
        $livroModel->setIdLivro($idLivro);
        $array = $livroModel->mostrarLivrosPorId();
        if ($array[10] == 2) {
            $livroModel->emprestarLivro($idUsuario, $array[6]);
            $livroModel->removerLivroReservadoQuandoEmprestado();
            /*
                $dataReserva = $livroModel->removerLivroReservadoQuandoEmprestado();
                $livroModel->emprestarLivro($idUsuario, $array[6], $dataReserva);
            */
            
            
            /*
              foreach ($livroModel->mostrarEmprestimosPorId($idUsuario) as $value){
              if ($value[1] == $idLivro){
              $dataEntrega = $value[3];
              }
              }
              $usuarioModel = new UsuarioModel();
              $usuarioModel->setIdUsuario($idUsuario);
              $leitor = $usuarioModel->mostrarUsuariosPorId();
              $usuarioModel->enviarEmailParaLeitor($leitor, $array, $dataEntrega);
             */
            $_SESSION['sweet'] = "Livro emprestado com sucesso.";
            header("location:../view/telaSeusLivros.php");
        }
    }

    private function concatenaNomeComId($texto, $codigo) {

        $posicao = strpos($texto["name"], '.');
        $parte1 = substr($texto["name"], 0, $posicao);
        $parte1 = $parte1 . $codigo;
        $parte2 = substr($texto["name"], $posicao);
        return $parte1 . $parte2;
    }

}
