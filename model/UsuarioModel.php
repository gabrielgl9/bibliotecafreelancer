<?php


class UsuarioModel {

    //novos e att
    private $idUsuario;
    private $nome;
    private $email;
    private $telefone;
    private $senha;
    private $celular;
    private $cpf;
    private $cep;
    private $cidade;
    private $bairro;
    private $uf;
    private $rua;
    private $numeroResidencia;
    private $complemento;

    function getIdUsuario() {
        return $this->idUsuario;
    }

    function getNome() {
        return $this->nome;
    }

    function getEmail() {
        return $this->email;
    }

    function getTelefone() {
        return $this->telefone;
    }

    function getSenha() {
        return $this->senha;
    }

    function getCelular() {
        return $this->celular;
    }

    function getCpf() {
        return $this->cpf;
    }

    function getCep() {
        return $this->cep;
    }

    function getCidade() {
        return $this->cidade;
    }

    function getUf() {
        return $this->uf;
    }

    function getBairro() {
        return $this->bairro;
    }

    function getRua() {
        return $this->rua;
    }

    function getNumeroResidencia() {
        return $this->numeroResidencia;
    }

    function getComplemento() {
        return $this->complemento;
    }

    function setIdUsuario($idUsuario) {
        $this->idUsuario = $idUsuario;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setTelefone($telefone) {
        $this->telefone = $telefone;
    }

    function setSenha($senha) {
        $this->senha = $senha;
    }

    function setCelular($celular) {
        $this->celular = $celular;
    }

    function setCpf($cpf) {
        $this->cpf = $cpf;
    }

    function setCep($cep) {
        $this->cep = $cep;
    }

    function setCidade($cidade) {
        $this->cidade = $cidade;
    }

    function setUf($uf) {
        $this->uf = $uf;
    }

    function setBairro($bairro) {
        $this->bairro = $bairro;
    }

    function setRua($rua) {
        $this->rua = $rua;
    }

    function setNumeroResidencia($numeroResidencia) {
        $this->numeroResidencia = $numeroResidencia;
    }

    function setComplemento($complemento) {
        $this->complemento = $complemento;
    }

    //att
    public function insert() {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("INSERT INTO usuario (nome, email, senha, telefone, celular, cpf, cep, uf, cidade, bairro, rua, numeroResidencia, complemento) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bindParam(1, $this->nome);
        $stmt->bindParam(2, $this->email);
        $stmt->bindValue(3, password_hash("$this->senha", PASSWORD_DEFAULT));
        $stmt->bindParam(4, $this->telefone);
        $stmt->bindParam(5, $this->celular);
        $stmt->bindParam(6, $this->cpf);
        $stmt->bindParam(7, $this->cep);
        $stmt->bindParam(8, $this->uf);
        $stmt->bindParam(9, $this->cidade);
        $stmt->bindParam(10, $this->bairro);
        $stmt->bindParam(11, $this->rua);
        $stmt->bindParam(12, $this->numeroResidencia);
        $stmt->bindParam(13, $this->complemento);
        $stmt->execute();
        
        
        $this->enviarEmailNewUser();

        return $con->lastInsertId();
    }

    //att
    public function autenticar() {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("SELECT * FROM usuario WHERE email = ?");
        $stmt->bindParam(1, $this->email);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                if (password_verify($this->senha, $row->senha)) {
                    $results = array($row->idUsuario, $row->nome, $row->email, $row->senha, $row->telefone, $row->celular, $row->cpf, $row->cep, $row->uf, $row->cidade, $row->bairro, $row->rua, $row->numeroResidencia, $row->complemento);
                }
            }
        }
        if (!isset($results)) {
            $results = null;
        }
        return $results;
    }
    
    public function identificarTipo($idUsuario){
        
        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("SELECT * FROM usuario WHERE idUsuario = ? AND tipo = 1");
        $stmt->bindParam(1, $idUsuario);
        $stmt->execute();

        $results = 0;
        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                    $results = 1;
            }
        }
        
        return $results;
    }
    
    public function cancelarPapel($idUsuario){
        
        $con = new PDO ("mysql:host=localhost; dbname=biblioteca", "root", "");
        $stmt = $con->prepare("UPDATE usuario SET tipo = 0 WHERE idUsuario = ? ");
        $stmt->bindParam(1, $idUsuario);
        $stmt->execute();
        
    }

    public function tornarAdmin($id) {
        
        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("UPDATE usuario SET tipo = 1 WHERE idUsuario = ?");
        $stmt->bindParam(1, intval($id));
        $stmt->execute();
        echo intval($id);
    }

    //novo
    public function update($idUsuario) {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("UPDATE usuario set nome = ?, telefone = ?, celular = ?, cpf = ?, cep = ?, uf = ?, cidade = ?, bairro = ?, rua = ?, numeroResidencia = ?, complemento = ? WHERE idUsuario = ?");
        $stmt->bindParam(1, $this->nome);
        $stmt->bindParam(2, $this->telefone);
        $stmt->bindParam(3, $this->celular);
        $stmt->bindParam(4, $this->cpf);
        $stmt->bindParam(5, $this->cep);
        $stmt->bindParam(6, $this->uf);
        $stmt->bindParam(7, $this->cidade);
        $stmt->bindParam(8, $this->bairro);
        $stmt->bindParam(9, $this->rua);
        $stmt->bindParam(10, $this->numeroResidencia);
        $stmt->bindParam(11, $this->complemento);
        $stmt->bindParam(12, $idUsuario);
        $stmt->execute();

        if ($stmt) {
            return true;
        }

        return false;
    }

    //att
    public function buscarUsuarioPorEmail($email) {
        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("SELECT * FROM usuario WHERE email = ?");
        $stmt->bindParam(1, $email);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results = array($row->idUsuario, $row->nome, $row->email, $row->senha, $row->telefone, $row->celular, $row->cpf, $row->cep, $row->uf, $row->cidade, $row->bairro, $row->rua, $row->numeroResidencia, $row->complemento);
            }
        }
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }
    
    public function verificarLivroReservado($email, $idLivro) {
        
        $arrayUsuario = $this->buscarUsuarioPorEmail($email);
        $livromodel = new LivroModel();
        $livrosReservados = $livromodel->mostrarLivrosReservados($arrayUsuario[0]);
        
        print_r($livrosReservados);
        foreach ($livrosReservados as $value) {
            if ($value[0] == $idLivro){
                return true;
            }
        }
        
        return false;
        
    }

    public function mostrarUsuariosPorId() {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("SELECT * FROM usuario WHERE idUsuario = ?");
        $stmt->bindParam(1, $this->idUsuario);
        $stmt->execute();
        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
               
                $results = array($row->idUsuario, $row->nome, $row->email, $row->senha, $row->telefone, $row->celular, $row->cpf, $row->cep, $row->uf, $row->cidade, $row->bairro, $row->rua, $row->numeroResidencia, $row->complemento);
            }
        }
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }
    
    public function mostrarTodosUsuarios() {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("SELECT * FROM usuario ORDER BY nome ASC");
        $stmt->bindParam(1, $this->idUsuario);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->idUsuario, $row->nome, $row->email, $row->senha, $row->telefone, $row->celular, $row->cpf, $row->cep, $row->uf, $row->cidade, $row->bairro, $row->rua, $row->numeroResidencia, $row->complemento, $row->tipo);
            }
        }
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }
    
    public function enviarEmailNewUser(){
        
        $from = "suporte@escritoriomovel.com"; 
        $to = $this->email; 
        $subject = "Sistema Biblioteca"; 
        $message = " 
        <html>
            <head>
                <title>Nonsensical Latin</title>
            </head>
            <body>
                <p> Informamos que um novo(a) usuário(a) foi registrado em nosso sistema, veja os detalhes abaixo: <br><br>
                    Nome: $this->nome <br>
                    Email: $this->email <br>
                    Telefone: $this->telefone <br>
                    Celular: $this->celular <br>
                    CPF: $this->cpf <br>
                    CEP: $this->cep <br>
                    UF: $this->uf <br>
                    Cidade: $this->cidade <br>
                    Bairro: $this->bairro <br>
                    Rua: $this->rua <br>
                    Numero da Residência: $this->numeroResidencia <br>
                    Complemento: $this->complemento <br>
                </p>
            </body>
        </html>
        ";

        // To send HTML mail, the Content-type header must be set
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        mail($to, $subject, $message, $headers);
        mail("erica@escritoriomovel.com", $subject, $message, $headers);

    }

    public function enviarEmail($codigo) {

        ini_set('display_errors', 1);
        error_reporting(E_ALL);

        $from = "gabrielgl13@hotmail.com"; //email da empresa
        $to = $_SESSION['email'];
        $subject = "Esqueceu sua senha";
        $message = "O codigo: " . $codigo;
        $headers = "De:" . $from; // duvida

        mail($to, $subject, $message, $headers);

        echo "A mensagem de e-mail foi enviada. Lá estará um codigo, insira ele aqui para modificar sua senha!";
        header("location:../view/telaEsqueciMinhaSenha.php");
    }

    public function colocarNovaSenha() {

        $con = new PDO("mysql:host=localhost;dbname=id6859346_biblioteca", "id6859346_gabrielgl9", "gabrielgl9@");
        $stmt = $con->prepare("UPDATE usuario SET senha = ?, rand = ? WHERE email = ?");
        $stmt->bindParam(1, password_hash("$this->senha", PASSWORD_DEFAULT));
        $var = null;
        $stmt->bindParam(2, $var);
        $stmt->bindParam(3, $this->email);
        $stmt->execute();
    }

    public function salvaRand($resultadoFinal) {

        $con = new PDO("mysql:host=localhost;dbname=id6859346_biblioteca", "id6859346_gabrielgl9", "gabrielgl9@");
        $stmt = $con->prepare("UPDATE usuario SET rand = ? WHERE email = ?");
        $stmt->bindParam(1, $resultadoFinal);
        $stmt->bindParam(2, $this->email);
        $stmt->execute();
    }

    public function conferirRand($rand) {

        $con = new PDO("mysql:host=localhost;dbname=id6859346_biblioteca", "id6859346_gabrielgl9", "gabrielgl9@");
        $stmt = $con->prepare("SELECT * FROM usuario WHERE rand = ? AND email = ?");
        $stmt->bindParam(1, $rand);
        $stmt->bindParam(2, $this->email);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results = "codigo correto";
            }
        }
        if ($results == "codigo correto") {
            return true;
        } else {
            return false;
        }
    }

}
