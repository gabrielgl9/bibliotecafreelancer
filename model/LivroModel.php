<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LivroModel
 *
 * @author Gabriel
 */
class LivroModel {

    private $idLivro;
    private $titulo;
    private $autor;
    private $edicao;
    private $editora;
    private $disponibilidade;
    private $nomeProp;
    private $telefoneProp;
    private $emailProp;
    private $numPag;

    function getIdLivro() {
        return $this->idLivro;
    }

    function getTitulo() {
        return $this->titulo;
    }

    function getAutor() {
        return $this->autor;
    }

    function getEdicao() {
        return $this->edicao;
    }

    function getEditora() {
        return $this->editora;
    }

    function getDisponibilidade() {
        return $this->disponibilidade;
    }

    function getNomeProp() {
        return $this->nomeProp;
    }

    function getTelefoneProp() {
        return $this->telefoneProp;
    }

    function getEmailProp() {
        return $this->emailProp;
    }

    function getNumPag() {
        return $this->numPag;
    }

    function setIdLivro($idLivro) {
        $this->idLivro = $idLivro;
    }

    function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    function setAutor($autor) {
        $this->autor = $autor;
    }

    function setEdicao($edicao) {
        $this->edicao = $edicao;
    }

    function setEditora($editora) {
        $this->editora = $editora;
    }

    function setDisponibilidade($disponibilidade) {
        $this->disponibilidade = $disponibilidade;
    }

    function setNomeProp($nomeProp) {
        $this->nomeProp = $nomeProp;
    }

    function setTelefoneProp($telefoneProp) {
        $this->telefoneProp = $telefoneProp;
    }

    function setEmailProp($emailProp) {
        $this->emailProp = $emailProp;
    }

    function setNumPag($numPag) {
        $this->numPag = $numPag;
    }

    public function insert() {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("INSERT INTO livro (titulo, autor, edicao, editora, numPag, disponibilidade, nomeProp, telefoneProp, emailProp, estado) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, 0)");
        $stmt->bindParam(1, $this->titulo);
        $stmt->bindParam(2, $this->autor);
        $stmt->bindParam(3, $this->edicao);
        $stmt->bindParam(4, $this->editora);
        $stmt->bindParam(5, $this->numPag);
        $stmt->bindParam(6, $this->disponibilidade);
        $stmt->bindParam(7, $this->nomeProp);
        $stmt->bindParam(8, $this->telefoneProp);
        $stmt->bindParam(9, $this->emailProp);
        $stmt->execute();

        return $con->lastInsertId();
    }

    public function emprestarLivro($idUsuario, $tempo, $dataReserva) {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("INSERT INTO emprestimo (idUsuario_emprestimo, idLivro, data_inicial, data_final, data_reserva) VALUES (?, ?, ?, ?, ?)");
        $stmt->bindParam(1, $idUsuario);
        $stmt->bindParam(2, $this->idLivro);

        $dataInicial = date('d/m/Y');
        $stmt->bindParam(3, $dataInicial);


        if ($tempo == "Mensal") {
            $timestamp = strtotime("+1 month");
        }

        if ($tempo == "Bimestral") {
            $timestamp = strtotime("+2 month");
        }

        if ($tempo == "Trimestral") {
            $timestamp = strtotime("+3 month");
        }

        $datafinal = date('d/m/Y', $timestamp);
        $stmt->bindParam(4, $datafinal);

        $stmt->execute();

        $stmt2 = $con->prepare("UPDATE livro set estado = 1 WHERE idLivro = ?");
        $stmt2->bindParam(1, $this->idLivro);
        $stmt2->execute();

        self::criarHistorico($this->idLivro, $idUsuario, $dataInicial, $datafinal);
    }

    public function removerEmprestimo() {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("DELETE FROM emprestimo WHERE idLivro = ?");
        $stmt->bindparam(1, $this->idLivro);
        $stmt->execute();

        $stmt2 = $con->prepare("UPDATE livro set estado = 0 WHERE idLivro = ?");
        $stmt2->bindParam(1, $this->idLivro);
        $stmt2->execute();

        self::finalizarHistorico($this->idLivro);
    }

    public function mostrarEmprestimosPorPesquisa($pesquisar) {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("SELECT * FROM emprestimo, livro WHERE livro.idLivro = emprestimo.idLivro AND livro.titulo like (?) ORDER BY data_final ASC");
        $pesquisar = "%" . $pesquisar . "%";
        $stmt->bindParam(1, $pesquisar);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->idUsuario_emprestimo, $row->idLivro, $row->data_inicial, $row->data_final);
            }
        }
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function mostrarEmprestimosPorPagination($pagina) {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $paginaInicial = $pagina * 5 - 5;
        $stmt = $con->prepare("SELECT * FROM emprestimo ORDER BY data_final ASC LIMIT $paginaInicial, 5");
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->idUsuario_emprestimo, $row->idLivro, $row->data_inicial, $row->data_final);
            }
        }
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function mostrarEmprestimos() {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("SELECT * FROM emprestimo ORDER BY data_final ASC");
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->idUsuario_emprestimo, $row->idLivro, $row->data_inicial, $row->data_final);
            }
        }
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function mostrarEmprestimosPorId($idUsuario) {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("SELECT * FROM emprestimo, livro WHERE livro.idLivro = emprestimo.idLivro AND emprestimo.idUsuario_emprestimo = ? ORDER BY titulo ASC");
        $stmt->bindParam(1, $idUsuario);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->idUsuario_emprestimo, $row->idLivro, $row->data_inicial, $row->data_final, $row->titulo, $row->autor, $row->nomeArq);
            }
        }
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function mostrarLivrosEmprestados($nomeCompleto, $email) {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");

        $stmt2 = $con->prepare("SELECT * FROM emprestimo, livro WHERE livro.idLivro = emprestimo.idLivro AND livro.emailProp = ? ORDER BY titulo ASC");
        $stmt2->bindParam(1, $email);
        $stmt2->execute();

        if ($stmt2) {
            while ($row2 = $stmt2->fetch(PDO::FETCH_OBJ)) {
                $results2[] = array($row2->idUsuario_emprestimo, $row2->idLivro, $row2->data_inicial, $row2->data_final, $row2->titulo, $row2->autor, $row2->edicao, $row2->editora, $row2->disponibilidade, $row2->numPag, $row2->nomeArq);
            }
        }

        if (!empty($results2)) {
            return $results2;
        } else {
            return null;
        }
    }

    public function mostrarLivrosPorCategoria($categoria) {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");

        $stmt = $con->prepare("SELECT * FROM livro, categoria, livro_categoria WHERE livro.idLivro = livro_categoria.idLivro AND livro_categoria.idCategoria = categoria.idCategoria AND categoria.idCategoria = ? ORDER BY titulo, autor ASC");
        $stmt->bindParam(1, $categoria);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->idCategoria, $row->idLivro, $row->titulo, $row->autor, $row->edicao, $row->editora, $row->numPag, $row->disponibilidade, $row->nomeProp, $row->telefoneProp, $row->emailProp, $row->estado, $row->nomeArq);
            }
        }

        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function mostrarLivrosPorPesquisa($valor) {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");

        $stmt = $con->prepare("SELECT * FROM livro WHERE titulo LIKE (?) OR autor LIKE (?) ORDER BY titulo, autor, edicao, editora, numPag, disponibilidade, telefoneProp, emailProp, estado, nomeArq ASC");
        $pesquisar = "%" . $valor . "%";
        $pesquisar2 = "%" . $valor . "%";
        $stmt->bindParam(1, $pesquisar);
        $stmt->bindParam(2, $pesquisar2);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->idLivro, $row->titulo, $row->autor, $row->edicao, $row->editora, $row->numPag, $row->disponibilidade, $row->nomeProp, $row->telefoneProp, $row->emailProp, $row->estado, $row->nomeArq);
            }
        }

        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function mostrarhistoricoPorLivro($idLivro) {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");

        $stmt = $con->prepare("SELECT * FROM historico WHERE idLivro = ?");
        $stmt->bindParam(1, $idLivro);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->idLivro, $row->idUsuario, $row->data_saida, $row->data_entrada, $row->data_limite);
            }
        }

        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function criarHistorico($idLivro, $idUsuario, $dataInicial, $dataLimite) {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("INSERT INTO historico (idLivro, idUsuario, data_entrada, data_limite) VALUES (?, ?, ?, ?)");
        $stmt->bindParam(1, $idLivro);
        $stmt->bindParam(2, $idUsuario);
        $stmt->bindParam(3, $dataInicial);
        $stmt->bindParam(4, $dataLimite);
        $stmt->execute();
    }

    public function finalizarHistorico($idLivro) {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("UPDATE historico SET data_saida = ? WHERE idLivro = ?");
        $dataSaida = date('d/m/Y');
        $stmt->bindParam(1, $dataSaida);
        $stmt->bindParam(2, $idLivro);
        $stmt->execute();
    }

    public function deletarHistorico($idLivro) {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("DELETE FROM historico WHERE idLivro = ?");
        $stmt->bindParam(1, $idLivro);
        $stmt->execute();
    }

    public function reservarLivro($idUsuario, $idLivro) {

        $dataInicial = date('d/m/Y');
        $timestamp = strtotime("+7 day");
        $datafinal = date('d/m/Y', $timestamp);


        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("INSERT INTO reservar (idUsuario, idLivro, data_inicial, data_final) VALUES (?, ?, ?, ?)");
        $stmt->bindParam(1, $idUsuario);
        $stmt->bindParam(2, $idLivro);
        $stmt->bindParam(3, $dataInicial);
        $stmt->bindParam(4, $datafinal);
        $stmt->execute();

        $stmt2 = $con->prepare("UPDATE livro SET estado = 2 WHERE idLivro = ?");
        $stmt2->bindParam(1, $idLivro);
        $stmt2->execute();
    }

    public function removerLivroReservado() {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("DELETE FROM reservar WHERE idLivro = ?");
        $stmt->bindparam(1, $this->idLivro);
        $stmt->execute();

        $stmt2 = $con->prepare("UPDATE livro set estado = 0 WHERE idLivro = ?");
        $stmt2->bindParam(1, $this->idLivro);
        $stmt2->execute();
    }

    public function removerLivroReservadoQuandoEmprestado() {

        $livroReservado = $this->mostrarReservaDoLivro($this->idLivro);
        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("DELETE FROM reservar WHERE idLivro = ?");
        $stmt->bindparam(1, $this->idLivro);
        $stmt->execute();
        
        return $livroReservado[5];
    }

    public function mostrarLivrosReservados($idUsuario) {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("SELECT * FROM livro, reservar WHERE livro.idLivro = reservar.idLivro AND reservar.idUsuario = ? ORDER BY titulo ASC");
        $stmt->bindParam(1, $idUsuario);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->idLivro, $row->titulo, $row->autor, $row->edicao, $row->editora, $row->numPag, $row->disponibilidade, $row->nomeProp, $row->telefoneProp, $row->emailProp, $row->estado, $row->nomeArq, $row->data_inicial, $row->data_final);
            }
        }
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function mostrarReservaDoLivro($idLivro) {
        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("SELECT * FROM usuario, reservar WHERE usuario.idUsuario = reservar.idUsuario AND idLivro = ?");
        $stmt->bindParam(1, $idLivro);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results = array($row->idUsuario, $row->nome, $row->email, $row->telefone, $row->celular, $row->data_inicial);
            }
        }

        return isset($results) ? $results : null;
    }

    public function mostrarTodosLivros() {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("SELECT * FROM livro ORDER BY titulo ASC");
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->idLivro, $row->titulo, $row->autor, $row->edicao, $row->editora, $row->numPag, $row->disponibilidade, $row->nomeProp, $row->telefoneProp, $row->emailProp, $row->estado, $row->nomeArq);
            }
        }
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function mostrarTodosLivrosPorPagination($pagina) {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $paginaInicial = $pagina * 5 - 5;
        $stmt = $con->prepare("SELECT * FROM livro ORDER BY titulo ASC LIMIT $paginaInicial, 5");
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->idLivro, $row->titulo, $row->autor, $row->edicao, $row->editora, $row->numPag, $row->disponibilidade, $row->nomeProp, $row->telefoneProp, $row->emailProp, $row->estado, $row->nomeArq);
            }
        }
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function pesquisarTodosLivrosPorPagination($valor, $pagina) {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $paginaInicial = $pagina * 5 - 5;
        $stmt = $con->prepare("SELECT * FROM livro WHERE titulo like(?) ORDER BY titulo ASC LIMIT $paginaInicial , 5");
        $pesquisar = "%" . $valor . "%";
        $stmt->bindParam(1, $pesquisar);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->idLivro, $row->titulo, $row->autor, $row->edicao, $row->editora, $row->numPag, $row->disponibilidade, $row->nomeProp, $row->telefoneProp, $row->emailProp, $row->estado, $row->nomeArq);
            }
        }
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public static function pesquisarEmprestimosPorPagination($pesquisar, $pagina) {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");

        $paginaInicial = $pagina * 5 - 5;
        $stmt = $con->prepare("SELECT * FROM emprestimo, livro WHERE livro.idLivro = emprestimo.idLivro AND livro.titulo like (?) ORDER BY data_final ASC LIMIT $paginaInicial, 5");
        $pesquisar = "%" . $pesquisar . "%";
        $stmt->bindParam(1, $pesquisar);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->idUsuario_emprestimo, $row->idLivro, $row->data_inicial, $row->data_final);
            }
        }
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public static function pesquisarEmprestimosSemLimit($pesquisar) {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("SELECT * FROM emprestimo, livro WHERE livro.idLivro = emprestimo.idLivro AND livro.titulo like (?) ORDER BY data_final ASC");
        $pesquisar = "%" . $pesquisar . "%";
        $stmt->bindParam(1, $pesquisar);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->idUsuario_emprestimo, $row->idLivro, $row->data_inicial, $row->data_final);
            }
        }
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public static function pesquisarSemLimit($pesquisar) {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("SELECT * FROM livro WHERE titulo like(?) ORDER BY titulo ASC");
        $pesquisar = "%" . $pesquisar . "%";
        $stmt->bindParam(1, $pesquisar);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->idLivro, $row->titulo, $row->autor, $row->edicao, $row->editora, $row->numPag, $row->disponibilidade, $row->nomeProp, $row->telefoneProp, $row->emailProp, $row->estado, $row->nomeArq);
            }
        }
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function mostrarLivrosPorPessoa($nomeCompleto, $email) {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");

        $stmt2 = $con->prepare("SELECT * FROM livro WHERE livro.emailProp = ? ORDER BY titulo ASC");
        $stmt2->bindParam(1, $email);
        $stmt2->execute();

        if ($stmt2) {
            while ($row2 = $stmt2->fetch(PDO::FETCH_OBJ)) {
                $results2[] = array($row2->idLivro, $row2->titulo, $row2->autor, $row2->edicao, $row2->editora, $row2->numPag, $row2->disponibilidade, $row2->nomeProp, $row2->telefoneProp, $row2->emailProp, $row2->estado, $row2->nomeArq);
            }
        }

        if (!empty($results2)) {
            return $results2;
        } else {
            return null;
        }
    }

    public function pesquisarSeusLivrosPorPagination($pesquisa, $pagina, $email) {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");

        $paginaInicial = ($pagina - 1) * 5;
        $stmt2 = $con->prepare("SELECT * FROM livro WHERE livro.emailProp = ? AND titulo like (?) ORDER BY titulo ASC LIMIT $paginaInicial , 5");
        $stmt2->bindParam(1, $email);
        $pesquisar = "%" . $pesquisa . "%";
        $stmt2->bindParam(2, $pesquisar);
        $stmt2->execute();

        if ($stmt2) {
            while ($row2 = $stmt2->fetch(PDO::FETCH_OBJ)) {
                $results2[] = array($row2->idLivro, $row2->titulo, $row2->autor, $row2->edicao, $row2->editora, $row2->numPag, $row2->disponibilidade, $row2->nomeProp, $row2->telefoneProp, $row2->emailProp, $row2->estado, $row2->nomeArq);
            }
        }

        if (!empty($results2)) {
            return $results2;
        } else {
            return null;
        }
    }

    public function mostrarSeusLivrosPorPagination($pagina, $email) {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");

        $paginaInicial = ($pagina - 1) * 5;
        $stmt2 = $con->prepare("SELECT * FROM livro WHERE livro.emailProp = ? ORDER BY titulo ASC LIMIT $paginaInicial , 5");
        $stmt2->bindParam(1, $email);
        $stmt2->execute();

        if ($stmt2) {
            while ($row2 = $stmt2->fetch(PDO::FETCH_OBJ)) {
                $results2[] = array($row2->idLivro, $row2->titulo, $row2->autor, $row2->edicao, $row2->editora, $row2->numPag, $row2->disponibilidade, $row2->nomeProp, $row2->telefoneProp, $row2->emailProp, $row2->estado, $row2->nomeArq);
            }
        }

        if (!empty($results2)) {
            return $results2;
        } else {
            return null;
        }
    }

    public function pesquisarSeusLivrosSemLimit($pesquisar, $email) {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");

        $pesquisar = "%" . $pesquisar . "%";
        $stmt = $con->prepare("SELECT * FROM livro WHERE livro.emailProp = ? AND titulo LIKE (?) ORDER BY titulo ASC");
        $stmt->bindParam(1, $email);
        $stmt->bindParam(2, $pesquisar);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->idLivro, $row->titulo, $row->autor, $row->edicao, $row->editora, $row->numPag, $row->disponibilidade, $row->nomeProp, $row->telefoneProp, $row->emailProp, $row->estado, $row->nomeArq);
            }
        }

        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function mostrarLivrosPorId() {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("SELECT * FROM livro WHERE idLivro = ? ORDER BY titulo ASC");
        $stmt->bindParam(1, $this->idLivro);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results = array($row->idLivro, $row->titulo, $row->autor, $row->edicao, $row->editora, $row->numPag, $row->disponibilidade, $row->nomeProp, $row->telefoneProp, $row->emailProp, $row->estado, $row->nomeArq);
            }
        }
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function colocarImagem($id, $novoNome, $nomeAntigo) {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("UPDATE livro SET nomeArq = ? WHERE idLivro = ?");
        $stmt->bindParam(1, $novoNome);
        $stmt->bindParam(2, $id);
        $stmt->execute();


        unlink("../upload/" . $nomeAntigo);
    }

    public function removerArquivo($idLivro, $nomeArquivo) {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("UPDATE livro SET nomeArq = ? WHERE idLivro = ?");
        $var = null;
        $stmt->bindParam(1, $var);
        $stmt->bindParam(2, $idLivro);
        $stmt->execute();

        unlink("../upload/" . $nomeArquivo);
    }

    public function editarLivro() {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("UPDATE livro set titulo = ?, autor = ?, edicao = ?, editora = ?, numPag = ?, disponibilidade = ?, nomeProp = ?, telefoneProp = ?, emailProp = ? WHERE idLivro = ?");
        $stmt->bindParam(1, $this->titulo);
        $stmt->bindParam(2, $this->autor);
        $stmt->bindParam(3, $this->edicao);
        $stmt->bindParam(4, $this->editora);
        $stmt->bindParam(5, $this->numPag);
        $stmt->bindParam(6, $this->disponibilidade);
        $stmt->bindParam(7, $this->nomeProp);
        $stmt->bindParam(8, $this->telefoneProp);
        $stmt->bindParam(9, $this->emailProp);
        $stmt->bindParam(10, $this->idLivro);
        $stmt->execute();

        return $this->idLivro;
    }

    public function deletarLivro() {

        $livro = $this->mostrarLivrosPorId();

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("DELETE FROM livro WHERE idLivro = ?");
        $stmt->bindparam(1, $this->idLivro);
        $stmt->execute();

        if (!empty($livro[11])) {
            unlink("../upload/$livro[11]");
        }

        if ($livro[10] == 1) {
            $this->removerEmprestimo();
        }

        if ($livro[10] == 2) {
            $this->removerLivroReservado();
        }

        $this->deletarHistorico($this->idLivro);

        $categoriaModel = new CategoriaModel();
        $categoriaModel->removerRelacionamentos($this->idLivro);

        if ($stmt) {

            return true;
        }

        return false;
    }

}
