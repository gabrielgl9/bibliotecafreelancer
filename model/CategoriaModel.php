<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Categoria
 *
 * @author Servidor
 */
class CategoriaModel {

    private $nome;
    private $idCategoria;

    function getIdCategoria() {
        return $this->idCategoria;
    }

    function setIdCategoria($idCategoria) {
        $this->idCategoria = $idCategoria;
    }

    function getNome() {
        return $this->nome;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    public function cadastrar() {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("INSERT INTO categoria (nome) VALUES (?) ");
        $stmt->bindParam(1, $this->nome);
        $stmt->execute();
    }
    
    public function editar ()
    {
        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("UPDATE categoria SET nome = ? WHERE idCategoria = ? ");
        $stmt->bindParam(1, $this->nome);
        $stmt->bindValue(2, $this->idCategoria);
        $stmt->execute();
    }

    public function deletarCategoria() {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("DELETE FROM categoria WHERE idCategoria = ?");
        $stmt->bindparam(1, $this->idCategoria);
        $stmt->execute();

        if ($stmt) {
            return true;
        }

        return false;
    }
    
    public function mostrarAutoComplete() {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");

        $stmt = $con->prepare("SELECT * FROM categoria ORDER BY nome");
        
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->idCategoria, $row->nome);
            }
        }
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }
    

    public function mostrarTodasCategorias($pagina) {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $paginaInicial = $pagina * 10 - 10;
        $paginaFinal = $pagina * 10;
        $stmt = $con->prepare("SELECT * FROM categoria ORDER BY nome LIMIT $paginaInicial , $paginaFinal");
        
        //$stmt->bindParam(1, $paginaInicial);
        //$stmt->bindValue(2, $paginaFinal);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->idCategoria, $row->nome);
            }
        }
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }

    public function mostrarCategoriasPorLivro($idLivro) {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("SELECT * FROM categoria, livro_categoria, livro WHERE livro.idLivro = livro_categoria.idLivro AND livro_categoria.idCategoria = categoria.idCategoria AND livro.idLivro = ?");
        $stmt->bindParam(1, $idLivro);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results[] = array($row->idCategoria, $row->nome);
            }
        }
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }
    
    public function pesquisarSemLimit($pesquisar) {
        
        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("SELECT * FROM categoria WHERE nome like (?) ORDER BY nome");
        $pesquisar = "%" . $pesquisar . "%";
        $stmt->bindParam(1, $pesquisar);
        $stmt->execute();
        
        if ($stmt){
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)){
                $results[] = array($row->idCategoria, $row->nome);
            }
        }else{
        }
        
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }
    
    public function pesquisar($pesquisar, $pagina)
    {
        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $paginaInicial = $pagina * 10 - 10;
        $paginaFinal = $pagina * 10;
        $stmt = $con->prepare("SELECT * FROM categoria WHERE nome like (?) ORDER BY nome LIMIT $paginaInicial , $paginaFinal");
        $pesquisar = "%" . $pesquisar . "%";
        $stmt->bindParam(1, $pesquisar);
        $stmt->execute();
        
        if ($stmt){
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)){
                $results[] = array($row->idCategoria, $row->nome);
            }
        }else{
        }
        
        if (!empty($results)) {
            return $results;
        } else {
            return null;
        }
    }
    
    public function verificaNomeCategoria($nome)
    {
        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("SELECT * FROM categoria WHERE nome = ?");
        $stmt->bindParam(1, $nome);
        $stmt->execute();
        
        if ($stmt){
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)){
                return false;
            }
        }
        
        return true;
    }

    public function salvaRelacionamento($idCategoria, $idLivro) {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("INSERT INTO livro_categoria (idLivro, idCategoria) VALUES (?, ?) ");
        $stmt->bindParam(1, $idLivro);
        $stmt->bindParam(2, $idCategoria);
        $stmt->execute();
    }

    public function removerRelacionamentos($idLivro) {

        $con = new PDO("mysql:host=localhost;dbname=biblioteca", "root", "");
        $stmt = $con->prepare("DELETE FROM livro_categoria WHERE idLivro = ?");
        $stmt->bindparam(1, $idLivro);
        $stmt->execute();

        if ($stmt) {
            return true;
        }

        return false;
    }

}
