<?php 
    header ("location: view/telaLogin.php");
?>
<html>
    <head>
        <meta charset="UTF-8">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <script type="text/javascript" src="../static/main.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Biblioteca</title>
    </head>
    <body>
        <a href="view/telaLogin.php" class="red-text" style="font-size: 20px;">Log in</a><br>
        <a href="view/telaCadastro.php" class="red-text" style="font-size: 20px;">Cadastre-se</a>
        <h5><i class="material-icons">keyboard_arrow_up</i>Para iniciar a aplicação, clique em um dos links acima. ** Faça isso após ler as informações **</h5>
        <br><br>
        <hr>

        <h3 style="margin:10px;" class="center">Informações</h3><br>

        <div style="font-size: 20px; margin:10px;">

            <p>
                Terminei as tarefas que a senhora me passou. Surgiram duas novas dúvidas<br><br>
                
<HR>
            <h4 class="center-align"><b>IMPORTANTE:</b> </h4>Para entrar como ator usuário, deve-se realizar o cadastro. <br>Para entrar como Administrador, vá em log in e digite no campo email: <b>admin@admin.admin</b> e na senha: <b>admin</b>
            <br><br>• <b>ISSO SERÁ TEMPORÁRIO.</B><br><br><hr>



            <p>
                ♦ <b>Dúvida 1:</b> Devo colocar uma funcionalidade para gravar as informações do log in? Algo parecido com "Lembrar de mim".

                <br><br>
                ♦ <b>Dúvida 2:</b> Será preciso colocar um rodapé informando telefone, email, endereço, entre outros assuntos sobre a associação? Ou talvez colocar um "Entre em contato" nessa página inicial?

                <br><br>
                

            </p>
        </div>
    </body>
</html>
