<?php
//alterei
session_start();
?>
<html>
    <head>
        <meta charset="UTF-8">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.0/jquery.mask.js"></script>
        <script type="text/javascript" src="../static/webService/webService.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Cadastro</title>
    </head>
    <body class="grey lighten-5">
        <script>
            $(document).ready(function () {
                $('.mascara-telefone').mask('(00)00000-0000', {reverse: false});
                $('.mascara-cep').mask('00000-000', {reverse: false});
                $('.mascara-cpf').mask('000.000.000-00', {reverse: false});
            });
        </script>
        <nav>
            <div class="nav-wrapper teal">
                <div class="col s12">
                    <a href="telaLogin.php" class="breadcrumb">Autenticação</a>
                    <a href="" class="breadcrumb">Registrar</a>
                </div>
            </div>
        </nav>
        <h3 class="center blue-grey-text">Registro de usuário</h3><br>
        <div class="row">
            <div class="col s6 m6 l6 offset-s3 offset-m3 offset-l3 card-panel">

                <form class="col s12" method="post" action="../controller/UsuarioController.php">
                    <input type="hidden" name="cadastrar" value="cadastrar"/>
                    <div class="row">
                        <div class="input-field col s6">
                            <i class="material-icons prefix">account_circle</i>
                            <input id="nomeCompleto" type="text" name="nomeCompleto" class="validate" required maxlength="35">
                            <label for="nomeCompleto">Nome completo</label>
                        </div>
                        <div class="input-field col s6">
                            <i class="material-icons prefix">settings_applications</i>
                            <input id="cpf" type="tel" class="mascara-cpf" name="cpf" class="validate" required maxlength="14">
                            <label for="cpf">CPF</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <i class="material-icons prefix">phone_android</i>
                            <input id="celular" type="text" class="mascara-telefone" name="celular" class="validate" maxlength="35">
                            <label for="celular">Celular</label>
                        </div>
                        <div class="input-field col s6">
                            <i class="material-icons prefix">phone</i>
                            <input id="telefone" type="tel" class="mascara-telefone" name="telefone" class="validate" maxlength="14">
                            <label for="telefone">Telefone</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <i class="material-icons prefix">email</i>
                            <input id="email" type="email" name="email" class="validate" required maxlength="35"> 
                            <label for="email">Email</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <i class="material-icons prefix">lock</i>
                            <input id="senha" type="password" name="senha" class="validate" required maxlength="25">
                            <label for="senha">Senha</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <i class="material-icons prefix">home</i>
                            <input id="cep" type="text" name="cep" class="mascara-cep" class="validate" required maxlength="9">
                            <label for="cep">Cep</label>
                        </div>
                    </div>
                    <div style="margin-top: -80px;">
                        <div class="row">
                            <div class="input-field col s6">
                                <i class="material-icons prefix" style="margin-top:8%;">public</i>
                                <label for="uf">Estado
                                    <input  id="uf" type="text" name="uf" value="" class="validate">
                                </label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix" style="margin-top:8%;">location_city</i>

                                <label>Cidade
                                    <input id="cidade" type="text" name="cidade" value="" class="validate">
                                </label>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="input-field col s12">
                                <i class="material-icons prefix" style="margin-top:4%;">domain</i>
                                <label>Bairro
                                    <input id="bairro" type="text" name="bairro" value="" class="validate">
                                </label>
                            </div>
                        </div><br>

                        <div class="row">
                            <div class="input-field col s12">
                                <!--domain -->
                                <i class="material-icons prefix" style="margin-top:4%;">streetview</i>
                                <label>Rua
                                    <input name="rua" type="text" value="" id="rua" size="60" />
                                </label>
                            </div>
                        </div>
                    </div><br><br><br>
                    <div class="row margin-top: 10%;">
                         <div class="input-field col s6">
                            <i class="material-icons prefix">add_location</i>
                            <input id="numeroResidencia" type="number" name="numeroResidencia" class="validate" required maxlength="10">
                            <label for="numeroResidencia">Número de residência</label>
                        </div>
                        <div class="input-field col s6">
                            <i class="material-icons prefix">settings_applications</i>
                            <input id="complemento" type="text" name="complemento" class="validate" maxlength="8">
                            <label for="complemento">Complemento</label>
                        </div>
                    </div>
                    <button class="btn waves-effect waves-light col s12" type="submit" name="action">Cadastrar
                    </button>
                </form>
            </div>
        </div>

        <?php
        if (!empty($_SESSION['sweet']) && isset($_SESSION['sweet'])):
            echo "<script> swal('" . $_SESSION['sweet'] . "', 'Falha ao cadastrar', 'error');</script>";
            $_SESSION['sweet'] = null;
        endif;
        ?>



    </body>
</html>
