<?php
if (!session_id()) {
    session_start();
}

if (!isset($_SESSION["usuario"])) {
    header("location:../controller/sessionDestroy.php");
}


$array = UsuarioController::mostrarUsuariosPorId($_SESSION["usuario"][0]);
if (isset($_POST["faleConosco"])):

    $assunto = filter_var($_POST["assunto"], FILTER_SANITIZE_STRING);
    $texto = filter_var($_POST["texto"], FILTER_SANITIZE_STRING);
    $_SESSION["sweet"] = "Mensagem Enviada com Sucesso!";
    //Agora aguarde enquanto nós analisamos sua dúvida.

    $from = "suporte@escritoriomovel.com";
    $to = "suporte@escritoriomovel.com";
    $subject = "Sistema Biblioteca: Usuário com algum problema.";
    $message = " 
        <html>
            <head>
                <title>Nonsensical Latin</title>
            </head>
            <body>
                <p> O Usuário de nome <b>$array[1]</b> e email <b>$array[2]</b> está com problemas em $assunto.<br><br>Sua Mensagem: <b>$texto</b>  </p>
            </body>
        </html>
        ";

    // To send HTML mail, the Content-type header must be set
    $headers = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
    mail($to, $subject, $message, $headers);

endif;
?>



<html>
    <head>
        <meta charset="UTF-8">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <script type="text/javascript" src="../static/main.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Fale Conosco</title>
    </head>

<?php
if (!empty($_SESSION['sweet']) && isset($_SESSION['sweet'])) {
    if ($_SESSION['sweet'] == "Mensagem Enviada com Sucesso!") {
        echo "<script>alert('oi')</script>";
        echo "<script> swal('" . $_SESSION['sweet'] . "', 'Agora aguarde enquanto nós analisamos sua dúvida.', 'success');</script>";
        $_SESSION['sweet'] = null;
    }
}
?>

    <style>
        .textao{
            border: 1px solid grey;
            height: 300px;
        }
    </style>
    <body class="grey lighten-5">
        <nav>
            <div class="nav-wrapper teal">
                <div class="col s12">
                    <a href="telaPrincipal.php" class="breadcrumb">Menu Principal</a>
                    <a href="" class="breadcrumb">Fale Conosco</a>
                </div>
            </div>
        </nav>
        <h3 class="center blue-grey-text">Fale Conosco</h3><br><br>
        <div class="row">
            <div class="col s6 m6 l6 offset-s3 offset-m3 offset-l3"  id="limit">
                <form class="col s12" method="post" action="telaFaleConosco.php">
                    <input type="hidden" name="faleConosco" value="faleConosco"/>

                    <div class="row">
                        <div class="input-field col s12">
                            <i class="material-icons prefix">Assunto</i>
                            <input id="assunto" type="text" name="assunto" class="validate" maxlength="35"> 
                            <label for="assunto">Assunto</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <textarea id="texto" name="texto" maxlength="500" class="textao" class="materialize-textarea"></textarea>
                            <label for="texto">Informe o problema ocorrido</label>
                        </div>
                    </div>
                    <button class="btn waves-effect waves-light col s12" type="submit" name="action">Enviar Mensagem
                    </button>
                </form>
            </div>
        </div>

    </body>
</html>
