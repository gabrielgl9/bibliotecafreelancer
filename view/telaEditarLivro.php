<?php
if (!session_id()) {
    session_start();
}
require_once "../controller/CategoriaController.php";
?>
<html>
    <head>
        <meta charset="UTF-8">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.0/jquery.mask.js"></script>
        <script type="text/javascript" src="../static/main.js"></script>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Editar Livro</title>
    </head>

    <script>
        $(document).ready(function () {
            $('.mascara-telefone').mask('(00)00000-0000', {reverse: false});
        });
    </script>

    <body class="grey lighten-5">
        <nav>
            <div class="nav-wrapper teal">
                <div class="col s12">
                    <?php if (!isset($_SESSION["usuario"])): ?>

                        <a href="telaTodosLivros.php" class="breadcrumb">Todos os livros</a>
                        <a href="" class="breadcrumb">Editar livro</a>
                    <?php else: ?>
                        <a href="telaPrincipal.php" class="breadcrumb">Menu Principal</a>
                        <a href="telaSeusLivros.php" class="breadcrumb">Seus livros</a>
                        <a href="" class="breadcrumb">Editar livro</a>
                    <?php endif; ?>

                </div>
            </div>
        </nav>

        <?php
        require_once '../controller/LivroController.php';

        if (isset($_POST["idLivro"])) {
            $_SESSION["id"] = filter_var($_POST["idLivro"], FILTER_SANITIZE_NUMBER_INT);
        }

        $array = LivroController::mostrarLivrosPorId($_SESSION["id"]);
        ?>
        <h3 class="center blue-grey-text">Editar Livro</h3><br>
        <div class="row">
            <div class="col s10 m8 l6 offset-s1 offset-m2 offset-l3 card-panel">

                <form class="col s12" method="post" action="../controller/LivroController.php" enctype="multipart/form-data">
                    <input type="hidden" name="cadastrarLivro" value="cadastrarLivro"/>
                    <input type="hidden" name="editarLivro" value="editarLivro"/>
                    <input type="hidden" name="MAX_FILE_SIZE" value="10485760">
                    <input type="hidden" name="idLivro" value="<?php echo $array[0]; ?>"/> <!-- colocar o ID -->
                    <div class="row">
                        <div class="input-field col s12">
                            <i class="material-icons prefix">create</i>
                            <input id="titulo" value="<?php echo $array[1]; ?>" type="text" name="titulo" class="validate" maxlength="80">
                            <label for="titulo">Título</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <i class="material-icons prefix">person</i>
                            <input id="autor" value="<?php echo $array[2]; ?>" type="text" name="autor" class="validate" maxlength="40">
                            <label for="autor">Autor</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <i class="material-icons prefix">book</i>
                            <input id="editora" value="<?php echo $array[4]; ?>" type="text" name="editora" class="validate" maxlength="40">
                            <label for="editora">Editora</label>
                        </div>
                        <div class="input-field col s6">
                            <i class="material-icons prefix">find_in_page</i>
                            <input id="edicao" type="text" value="<?php echo $array[3]; ?>" name="edicao" class="validate" maxlength="10">
                            <label for="edicao">Edição</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <i class="material-icons prefix">confirmation_number</i>
                            <input id="numPag" value="<?php echo $array[5]; ?>" type="number" name="numPag" class="validate" required maxlength="40">
                            <label for="numPag">Número de páginas</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="file-field input-field col s12">
                            <div class="btn">
                                <span>Arquivo</span>
                                <input id="arquivo" name="arquivo" type="file" multiple>
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text" placeholder="Faça upload de uma imagem.">
                            </div>
                        </div>
                        <?php if (isset($array[11]) && !empty($array[11])): ?>
                            <a class="modal-trigger" href="#modalCapa<?php echo $array[0]; ?>">Clique aqui para ver o arquivo atual</a>
                        <?php endif; ?>
                    </div>

                    <!-- MODAL CAPA -->
                    <div id="modalCapa<?php echo $array[0]; ?>" class="modal">

                        <img src="../upload/<?php echo $array[11]; ?>" class="responsive-img" />
                        <div class="modal-footer">
                            <center class='white-text'>

                                <a href="../controller/LivroController.php?idLivro=<?php echo $array[0]; ?>" class="modal-close waves-effect waves-green btn-small red">Remover arquivo</a>
                                <a class="modal-close waves-effect waves-green btn-small">Fechar</a>
                            </center>
                        </div>

                    </div>

                    <div class="input-field col s12">
                        <?php
                        $array2 = CategoriaController::mostrarTodasCategoriasParaCadastro();
                        $array3 = CategoriaController::mostrarCategoriasPorLivro($array[0]);

                        if (!empty($array3)):
                            foreach ($array2 as $value) :
                                $a = 0;
                                foreach ($array3 as $valor) :
                                    if ($valor[0] == $value[0]) :
                                        $a = 1;
                                    endif;
                                endforeach;
                                if ($a != 1) :
                                    $valoresSemChecked[] = $value;
                                endif;
                            endforeach;
                            ?>
                            <select multiple name="categoria[]">
                                <option value="" disabled selected>Escolha o(s) gênero(s) do livro: </option>
                                <?php
                                foreach ($array3 as $valores):
                                    ?>
                                    <option selected value = "<?= $valores[0]; ?>"><?= $valores[1]; ?></option>
                                    <?php
                                endforeach;
                                foreach ($valoresSemChecked as $value):
                                    ?>
                                    <option value = "<?= $value[0]; ?>"><?= $value[1]; ?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                            <label>Gênero: </label>
                        <?php else: ?>
                            <select multiple name="categoria[]">
                                <option value="" disabled selected>Escolha o(s) gênero(s) do livro: </option>
                                <?php
                                if (!empty($array2)):
                                    foreach ($array2 as $value):
                                        ?>
                                        <option value = "<?= $value[0]; ?>"><?= $value[1]; ?></option>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </select>
                            <label>Gênero: </label>
                        <?php endif; ?>
                    </div>


                    <span class="grey-text">Disponibilidade:</span><br><br>
                    <div class="row center">
                        <label style="padding-right: 10%;">
                            <?php if ($array[6] == "Mensal") { ?>
                                <input name="disponibilidade" value="Mensal" type="radio" checked/>
                            <?php } else { ?>
                                <input name="disponibilidade" value="Mensal" type="radio"/>
                            <?php } ?>
                            <span>Mensal </span>
                        </label>
                        <label style="padding-right: 10%;">
                            <?php if ($array[6] == "Bimestral") { ?>
                                <input name="disponibilidade" value="Bimestral" type="radio" checked/>
                            <?php } else { ?>
                                <input name="disponibilidade" value="Bimestral" type="radio"/>
                            <?php } ?>
                            <span>Bimestral </span>
                        </label>
                        <label style="padding-right: 10%;">
                            <?php if ($array[6] == "Trimestral") { ?>
                                <input name="disponibilidade" value="Trimestral" type="radio" checked/>
                            <?php } else { ?>
                                <input name="disponibilidade" value="Trimestral" type="radio"/>
                            <?php } ?>
                            <span>Trimestral </span>
                        </label>
                    </div>
                    <?php
                    if (!isset($_SESSION["usuario"])):
                        ?>
                        <h5 class="teal-text" style="font-family: Comic Sans MS">Pertence à:</h5>

                        <!--<div class="row">
                            <div class="input-field col s6">
                                <i class="material-icons prefix">account_circle</i>
                                <input id="nomeCompleto" value="<?php // echo $array[7];       ?>" type="text" name="nomeCompleto" class="validate" maxlength="35">
                                <label for="nomeCompleto">Nome completo</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">phone</i>
                                <input id="telefone" class="mascara-telefone" value="<?php // echo $array[8];       ?>" type="tel" name="telefone" class="validate" maxlength="15">
                                <label for="telefone">Telefone</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <i class="material-icons prefix">email</i>
                                <input id="email" value="<?php //echo $array[9];       ?>" type="email" name="email" class="validate" maxlength="35">
                                <label for="email">Email</label>
                            </div>
                        </div>-->

                        <div class="input-field col s12">
                            <select name="idUsuario">
                                <?php
                                $usuarioEdit = UsuarioController::buscarUsuariosPorEmail($array[9]);
                                ?>
                                <option value="<?= $usuarioEdit[0]; ?>" selected><?= $usuarioEdit[1]; ?></option>
                                <?php
                                $usuario = UsuarioController::mostrarTodosUsuarios();
                                foreach ($usuario as $valor):
                                    if ($valor[0] != $usuarioEdit[0]):
                                        ?> 
                                        <option value="<?= $valor[0]; ?>"><?= $valor[1]; ?></option>
                                        <?php
                                    endif;
                                endforeach;
                                ?>
                            </select>
                            <label>Selecione o usuário proprietário</label>
                        </div>
                        <?php
                    else:
                        require_once '../controller/UsuarioController.php';
                        $array = UsuarioController::mostrarUsuariosPorId($_SESSION["usuario"][0]);
                        ?>
                        <input type="hidden" name="nomeCompleto" value="<?php echo $array[1]; ?>">
                        <input type="hidden" name="email" value="<?php echo $array[2]; ?>">
                        <input type="hidden" name="telefone" value="<?php echo $array[4]; ?>">
                    <?php
                    endif;
                    ?>
                    <div class="row">
                        <button class="btn waves-effect waves-light col s12" type="submit" name="action">Editar
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <?php
        if (!empty($_SESSION['sweet']) && isset($_SESSION['sweet'])) {
            if ($_SESSION['sweet'] == "Arquivo muito pesado!" || $_SESSION['sweet'] == "Arquivo inválido! Você deve enviar uma imagem.") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'Falha no upload!', 'error');</script>";
                $_SESSION['sweet'] = null;
            }
            if ($_SESSION['sweet'] == "Arquivo Removido.") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'Este livro está sem foto de capa', 'success');</script>";
                $_SESSION['sweet'] = null;
            }
        }
        ?>
    </body>
</html>
