<?php
if (!session_id()) {
    session_start();
}
?>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Esqueci minha senha</title>
    </head>
    <body class="grey lighten-5">

        <nav>
            <div class="nav-wrapper teal">
                <div class="col s12">
                    <a href="telaLogin.php" class="breadcrumb">Log in</a>
                    <a href="" class="breadcrumb">Esqueci minha senha</a>
                </div>
            </div>
        </nav>
        <h3 class="center blue-grey-text">Esqueceu sua senha? Siga as instruções!</h3><br>
        <div class="row">
            <div class="col s6 m6 l4 offset-s3 offset-m3 offset-l4 card-panel">

                <form class="col s12" method="post" action="../controller/UsuarioController.php">
                    <?php
                    if (!empty($_SESSION['modal']) && isset($_SESSION['modal'])):
                        if ($_SESSION['modal'] == "Disponibilizar um TextField para a digitação do código."):
                            ?>
                            <input type="hidden" name="conferirRand" value="conferirRand"/>
                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="material-icons prefix">vpn_key</i>
                                    <input id="rand" type="text" name="rand" class="validate" required maxlength="8">
                                    <label for="rand">Digite o código aqui</label>
                                </div>
                            </div>
                            <?php
                            $_SESSION['modal'] = null;
                        else:
                            ?>
                            <input type="hidden" name="novaSenha" value="novaSenha"/>
                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="material-icons prefix">lock</i>
                                    <input id="senha" type="password" name="senha" class="validate" required maxlength="25">
                                    <label for="senha">Nova senha</label>
                                </div>
                            </div>
                        <?php
                        endif;
                    else:
                        ?>
                        <input type="hidden" name="esqueciMinhaSenha" value="esqueciMinhaSenha"/>
                        <div class="row">
                            <div class="input-field col s12">
                                <i class="material-icons prefix">account_circle</i>
                                <input id="email" type="email" name="email" class="validate" required maxlength="35">
                                <label for="email">Informe o seu e-mail</label>
                            </div>
                        </div>
                    <?php
                    endif;
                    ?>


                    <center>
                        <button class="btn waves-effect waves-light center" type="submit" name="action">enviar
                            <i class="material-icons right">send</i>
                        </button>
                    </center>
                </form>
            </div>
        </div>


        <?php
        if (!empty($_SESSION['sweet']) && isset($_SESSION['sweet'])):
            if ($_SESSION["sweet"] == "Enviamos um email para você.") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'Insira no campo de texto, o codigo que lhe enviamos por email!', 'success');</script>";
                $_SESSION['sweet'] = null;
            }
            if ($_SESSION["sweet"] == "O código inserido está correto.") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'Agora insira no campo de texto abaixo, a sua nova senha', 'success');</script>";
                $_SESSION['sweet'] = null;
            }
            if ($_SESSION["sweet"] == "O código inserido está incorreto.") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'O código que lhe enviamos é outro!', 'error');</script>";
                $_SESSION['sweet'] = null;
            }
            

        endif;
        ?>


    </body>
</html>
