<?php
if (!session_id()) {
    session_start();
}

if (isset($_SESSION["modal"])) {
    session_destroy();
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Login</title>
    </head>
    <body class="grey lighten-5">


        <h3 class="center blue-grey-text">Autenticação de usuário</h3><br>
        <div class="row">
            <div class="col s6 m6 l4 offset-s3 offset-m3 offset-l4 card-panel">

                <form class="col s12" method="post" action="../controller/UsuarioController.php">
                    <input type="hidden" name="autenticar" value="autenticar"/>
                    <div class="row">
                        <div class="input-field col s12">
                            <img src="../imagens/BibliotecaLogin.PNG" alt="" class="responsive-img valign profile-image-login col s12">
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <i class="material-icons prefix">account_circle</i>
                            <input id="email" type="email" name="email" class="validate" required maxlength="35">
                            <label for="email">Email</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <i class="material-icons prefix">lock</i>
                            <input id="senha" type="password" name="senha" class="validate" required maxlength="15">
                            <label for="senha">Senha</label>
                        </div>
                        <span style="margin-left: 4%;"><a href="telaCadastro.php">Cadastre-se aqui</a></span>
                        <span class="right" style="margin-left: 4%; margin-right: 4%;"><a href="telaEsqueciMinhaSenha.php">Esqueci minha senha</a></span>
                    </div>
                    <button class="btn waves-effect waves-light col s12" type="submit" name="action">Entrar
                    </button>
                </form>
            </div>
        </div>

        <div class="row" style="margin: 30px;">
            <div class="col s4 left">
                <img class="responsive-img" src="../imagens/Logo Nucleo Mulher REDUZIDO.png" />
            </div>
            <div class="col s4 center">
                <img class="responsive-img" src="../imagens/escritoriomovel.png" style="width: 50%;"/><br>
                <span class="red-text">Desenvolvido por Erica Grellert e Gabriel Viegas</span>
            </div>
            <div class="col s3 right">
                <img class="responsive-img" src="../imagens/ACPA REDUZIDO.png" />
            </div>
        </div>

        <?php
        if (!empty($_SESSION['sweet']) && isset($_SESSION['sweet'])):
            if ($_SESSION["sweet"] == "Você modificou sua senha.") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'Agora você tem uma nova senha!', 'success');</script>";
                $_SESSION['sweet'] = null;
                if (isset($_SESSION["modal"])) {
                    session_destroy();
                }
            } else {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'Você digitou errado a sua senha!', 'error');</script>";
                $_SESSION['sweet'] = null;
            }
        endif;
        ?>


    </body>
</html>
