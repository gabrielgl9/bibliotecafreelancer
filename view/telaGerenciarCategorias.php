<?php
if (!session_id()) {
    session_start();
}

if (!isset($_SESSION["admin"])) {
    header("location:../controller/sessionDestroy.php");
}

require_once '../controller/CategoriaController.php';
?>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <script type="text/javascript" src="../static/main.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Gerenciar Categorias</title>
    </head>
    
    <script>
        $(document).ready(function () {
            $('.modal').modal();
            $('.sidenav').sidenav();

            $("input.autocomplete").autocomplete({
            data: {
                <?php
                $todasCategorias = CategoriaController::mostrarAutoComplete();
                if (!empty($todasCategorias)):
                    foreach ($todasCategorias as $valor):
                        echo "'$valor[1]' : null,";
                    endforeach;
                endif;
                ?>
            },
                    limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
                    minLength: 1 // The minimum length of the input for the autocomplete to start. Default: 1.
            });

            $("#cadastrar").click(function () {
                swal("Digite o nome da categoria:", {
                    content: "input",
                }).then((value) => {
                    if (value !== '' && value.replace(/ /g, "") !== "") {
                        $.ajax({
                        url: '../controller/CategoriaController.php',
                                type: 'post',
                                data: {
                                cadastrar: "cadastrar",
                                        nome: value
                                }
                        }).done(function (data) {
                            if (data == "Existente"){
                                swal("Já existe uma categoria com esse nome! ");
                            } else{
                                swal("Você cadastrou a categoria: " + value).then(function () {
                                    location.reload();
                                });
                            }
                        });
                    } else {
                        swal('Cadastro de disciplina inválido! ');
                    }
                });
            });
        });
                
    </script>

    <body class="grey lighten-5">
        <nav>
            <div class="nav-wrapper teal">
                <div class="col s12">
                    <a href="telaPrincipal.php" class="breadcrumb">Menu Principal</a>
                    <a href="telaGerenciarCategorias.php" class="breadcrumb">Gerenciar Categorias</a>
                </div>
            </div>
        </nav>


        <?php
        if (isset($_GET["pagina"])) {
            $pagina = $_GET["pagina"];
        } else {
            $pagina = 1;
        }
        ?>

        <h3 class="center blue-grey-text">Gerenciar Categorias</h3><br>
        <div class="row">

            <!-- Pesquisar categoria -->
            <div class="col s8">
                <form method="POST" action="../controller/CategoriaController.php">
                    <input type="hidden" name="pesquisar" value="pesquisar" >
                    <div class="input-field col s5 m4 l3">
                        <input type="text" autocomplete="off" name="pesquisar" style="border: 1px solid" placeholder="Digite o nome da categoria" id="autocomplete-input" class="autocomplete black-text"/>
                    </div>
                    <div class="input-field col s2">
                        <button class="btn waves-effect waves-light blue darken-4 btn-large" style="margin-left: -17px;" type="submit" name="action">Pesquisar</button>
                    </div>
                </form>
            </div>
            <div class="col s4">
                <a id="cadastrar" class="btn-large waves-effect waves-light blue">Cadastrar Categoria</a>
            </div>

        </div><br>



        <div class="row">
            <div class="col s10 offset-s1">
                <?php
                //aqui estava um IF
                if (isset($_GET["pesquisa"])) {
                    $array = CategoriaController::pesquisarPorPagination($_GET["pesquisa"]);
                } else {
                    $array = CategoriaController::mostrarTodasCategorias($pagina);
                }

                if (!empty($array)):
                    ?>
                    <table class="centered responsive-table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nome</th>
                                <th>Remover</th>
                                <th>Editar</th>

                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            foreach ($array as $value):
                                ?>
                                <tr>
                                    <td><?php echo $value[0]; ?></td>
                                    <td><?php echo $value[1]; ?></td>
                                    <td><a class="modal-trigger" href="#modalRemove<?php echo $value[0]; ?>"><i class="material-icons prefix red-text">delete</i></a></td>
                                    <td><a class="waves-effect waves-light" onclick="editar('<?= $value[0]; ?>', '<?= $value[1]; ?>')"><i class="material-icons prefix green-text">edit</i></a></td>

                                    <!-- MODAL DELETE -->
                            <div id="modalRemove<?php echo $value[0]; ?>" class="modal">
                                <div class="modal-content">
                                    <center><img src="../imagens/Ponto de exclamacao.jpg" style="height: 100px;"/></center><br>
                                    <h4 class="center blue-grey-text">Você tem certeza disso?</h4>
                                    <p class="center blue-grey-text">A Categoria  "<?php echo $value[1]; ?>" será removida e isso não poderá ser revertido. </p>
                                    <center class='white-text'>
                                        <form method="POST" action="../controller/CategoriaController.php">
                                            <input type="hidden" name="deletarCategoria" value="deletarCategoria"/>
                                            <input type="hidden" name="idCategoria" value="<?php echo $value[0]; ?>"/>
                                            <button class="modal-close waves-effect waves-light btn-small blue" type="submit" name="action">Confirmar</button>
                                        </form>
                                        <a class="modal-close waves-effect waves-light btn-small red">Cancelar</a>
                                    </center>
                                </div>
                            </div>
                            </tr>
                            <?php
                        endforeach;
                        ?>
                        </tbody>
                    </table>
                    <?php
//$_SESSION["pesquisa"] = null;
                endif;
                ?>
            </div>
        </div>

        <div class="row center">



            <?php
            if (isset($array)) {
                if (isset($_GET["pesquisa"])) {


                    if ($_SESSION["countPesquisa"] > 0) {
                        $numeroPagina = floor(($_SESSION["countPesquisa"] - 0.1) / 10);
                    } else {
                        $numeroPagina = 1;
                    }
                    ?>
                    <ul class="pagination">
                        <?php if ($_GET["pesquisa"] == 1) { ?>
                            <li class="disabled"><i class="material-icons">chevron_left</i></li>
                        <?php } else { ?>
                            <li class="waves-effect"><a href="telaGerenciarCategorias.php?pesquisa=<?= $_GET["pesquisa"] - 1; ?>"><i class="material-icons">chevron_left</i></a></li>
                        <?php } ?>

                        <?php
                        for ($x = 0; $x <= $numeroPagina; $x++) {
                            if ($_GET["pesquisa"] == $x + 1) {
                                ?>
                                <li class="active"><a href="#!"><?= $x + 1; ?></a></li>
                            <?php } else { ?>
                                <li class="waves-effect"><a href="telaGerenciarCategorias.php?pesquisa=<?= $x + 1; ?>"><?= $x + 1; ?></a></li>
                                <?php
                            }
                        }
                        if ($numeroPagina + 1 == $_GET["pesquisa"]) {
                            ?>
                            <li class="disabled"><i class="material-icons">chevron_right</i></li>
                        <?php } else { ?>
                            <li class="waves-effect"><a href="telaGerenciarCategorias.php?pesquisa=<?= $_GET["pesquisa"] + 1; ?>"><i class="material-icons">chevron_right</i></a></li>
                        <?php } ?>
                    </ul>
                    <?php
                } else {


                    //verifica numero de paginas. Se não houver categorias, não posso fazer 0/10, pois haverá uma página;
                    if (count($todasCategorias) > 0) {
                        $numero = (count($todasCategorias) - 0.1) / 10;
                        $numeroPagina = floor((count($todasCategorias) - 0.1) / 10);
                    } else {
                        $numero = 1;
                        $numeroPagina = 1;
                    }
                    if ($numero > 1) {
                        ?>
                        <ul class="pagination">
                            <?php if ($pagina == 1) { ?>
                                <li class="disabled"><i class="material-icons">chevron_left</i></li>
                            <?php } else { ?>
                                <li class="waves-effect"><a href="telaGerenciarCategorias.php?pagina=<?= $pagina - 1; ?>"><i class="material-icons">chevron_left</i></a></li>
                            <?php } ?>

                            <?php
                            for ($x = 0; $x <= $numeroPagina; $x++) {
                                if ($pagina == $x + 1) {
                                    ?>
                                    <li class="active"><a href="#!"><?= $x + 1; ?></a></li>
                                <?php } else { ?>
                                    <li class="waves-effect"><a href="telaGerenciarCategorias.php?pagina=<?= $x + 1; ?>"><?= $x + 1; ?></a></li>
                                    <?php
                                }
                            }
                            if ($numeroPagina + 1 == $pagina) {
                                ?>
                                <li class="disabled"><i class="material-icons">chevron_right</i></li>
                            <?php } else { ?>
                                <li class="waves-effect"><a href="telaGerenciarCategorias.php?pagina=<?= $pagina + 1; ?>"><i class="material-icons">chevron_right</i></a></li>
                                <?php } ?>
                        </ul>

                        <?php
                    }
                }
            }
            ?>
        </div>

        <?php
        if (!empty($_SESSION['sweet']) && isset($_SESSION['sweet'])):
            if ($_SESSION['sweet'] == "Categoria excluida com sucesso.") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'A categoria não pertence mais a esta lista e também não poderá ser atribuída à algum livro.', 'success');</script>";
                $_SESSION['sweet'] = null;
            } else if ($_SESSION["sweet"] == "Falha ao remover a categoria.") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'Desculpe-nos, tente novamente mais tarde :)', 'error');</script>";
                $_SESSION['sweet'] = null;
            }
        endif;
        ?>

        <script>

                function editar(id, nome){

                    swal({
                            text: "Renomeie essa categoria: ",
                            content: {
                            element: "input",
                                    attributes: {
                                        value: nome,
                                        type: "text",
                                    },
                            },
                    }).then((value) => {
                            if (value !== '' && value.replace(/ /g, "") !== "") {
                                $.ajax({
                                url: '../controller/CategoriaController.php',
                                        type: 'post',
                                        data: {
                                                editar: "editar",
                                                nome: value,
                                                id: id
                                        }
                                }).done(function () {
                                    swal("Você editou a categoria: " + value).then(function () {
                                        location.reload();
                                    });
                                });
                            } else {
                                swal('Dados não modificados! ');
                            }
                        });
                }

        </script>
    </body>
</html>
