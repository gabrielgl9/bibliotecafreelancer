<?php
if (!session_id()) {
    session_start();
}

require_once '../controller/CategoriaController.php';
require_once '../controller/UsuarioController.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.0/jquery.mask.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Cadastrar Livro</title>
    </head>
    <script>
        $(document).ready(function () {
            $('select').formSelect();
            $('.mascara-telefone').mask('(00)00000-0000', {reverse: false});


        });
    </script>
    <body class="grey lighten-5">
        <nav>
            <div class="nav-wrapper teal">
                <div class="col s12">
                    <?php if (!isset($_SESSION["usuario"])): ?>
                        <a href="telaTodosLivros.php" class="breadcrumb">Tela Administrador</a>
                    <?php else: ?>
                        <a href="telaPrincipal.php" class="breadcrumb">Menu principal</a>
                    <?php endif; ?>

                    <a href="" class="breadcrumb">Cadastrar Livro</a>
                </div>
            </div>
        </nav>
        <h3 class="center blue-grey-text">Cadastrar Livro</h3><br>
        <div class="row">
            <div class="col s10 m8 l6 offset-s1 offset-m2 offset-l3 card-panel">

                <form class="col s12" method="post" action="../controller/LivroController.php" enctype="multipart/form-data">
                    <input type="hidden" name="MAX_FILE_SIZE" value="10485760">
                    <input type="hidden" name="cadastrarLivro" value="cadastrarLivro"/>
                    <div class="row">
                        <div class="input-field col s12">
                            <i class="material-icons prefix">create</i>
                            <input id="titulo" type="text" name="titulo" class="validate" required maxlength="80">
                            <label for="titulo">Título</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <i class="material-icons prefix">person</i>
                            <input id="autor" type="text" name="autor" class="validate" required maxlength="40">
                            <label for="autor">Autor</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <i class="material-icons prefix">book</i>
                            <input id="editora" type="text" name="editora" class="validate" required maxlength="40">
                            <label for="editora">Editora</label>
                        </div>
                        <div class="input-field col s6">
                            <i class="material-icons prefix">find_in_page</i>
                            <input id="edicao" type="text" name="edicao" class="validate" required maxlength="10">
                            <label for="edicao">Edição</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <i class="material-icons prefix">confirmation_number</i>
                            <input id="numPag" type="number" name="numPag" class="validate" required maxlength="6">
                            <label for="numPag">Número de páginas</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="file-field input-field col s12">
                            <div class="btn">
                                <span>Arquivo</span>
                                <input id="arquivo" name="arquivo" type="file" multiple>
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text" placeholder="Faça upload de uma imagem.">
                            </div>
                        </div>
                    </div>

                    <div class="input-field col s12">
                        <?php
                        $array = CategoriaController::mostrarTodasCategoriasParaCadastro();
                        //print_r($array);
                        ?>
                        <select multiple name="categoria[]">
                            <option value="" disabled selected>Escolha o(s) gênero(s) do livro</option>
                            <?php
                            print_r($array);
                            if (!empty($array)):
                                foreach ($array as $value):
                                    //echo "<script>alert(".$value[0].")</script>";
                                    ?>
                                    <option value = "<?= $value[0]; ?>"><?= $value[1]; ?></option>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </select>
                        <label>Gênero: </label>
                    </div>

                    <span class="grey-text">Disponibilidade:</span><br><br>
                    <div class="row center">
                        <label style="padding-right: 10%;">
                            <input name="disponibilidade" value="Mensal" type="radio"/>
                            <span>Mensal </span>
                        </label>
                        <label style="padding-right: 10%;">
                            <input name="disponibilidade" value="Bimestral" type="radio"/>
                            <span>Bimestral </span>
                        </label>
                        <label style="padding-right: 10%;">
                            <input name="disponibilidade" value="Trimestral" type="radio"/>
                            <span>Trimestral </span>
                        </label>
                    </div>
                    <?php
                    if (!isset($_SESSION["usuario"])):
                        ?>
                        <div class="input-field col s12">
                            <select name="idUsuario">
                                <option value="" disabled selected>Escolha uma opção</option>
                                <?php
                                $usuario = UsuarioController::mostrarTodosUsuarios();
                                foreach ($usuario as $valor):
                                    ?> 
                                    <option value="<?= $valor[0]; ?>"><?= $valor[1]; ?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                            <label>Selecione o usuário proprietário</label>
                        </div>

                        <?php
                    else:
                        require_once '../controller/UsuarioController.php';
                        $array = UsuarioController::mostrarUsuariosPorId($_SESSION["usuario"][0]);
                        ?>
                        <input type="hidden" name="nomeCompleto" value="<?php echo $array[1]; ?>">
                        <input type="hidden" name="email" value="<?php echo $array[2]; ?>">
                        <input type="hidden" class="mascara-telefone" name="telefone" value="<?php echo $array[4]; ?>">
                    <?php
                    endif;
                    ?>
                    <button class="btn waves-effect waves-light col s12" type="submit" name="action">Cadastrar
                    </button>
                </form>
            </div>
        </div>
        <?php
        if (!empty($_SESSION['sweet']) && isset($_SESSION['sweet'])) {
            if ($_SESSION['sweet'] == "A devolução do livro foi concluída.") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'O livro foi removido da lista de emprestimos!', 'error');</script>";
                $_SESSION['sweet'] = null;
            }
            if ($_SESSION['sweet'] == "Arquivo muito pesado!" || $_SESSION['sweet'] == "Arquivo inválido! Você deve enviar uma imagem.") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'Falha no upload!', 'error');</script>";
                $_SESSION['sweet'] = null;
            }
        }
        ?>
    </body>
</html>
