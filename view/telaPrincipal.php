<?php
if (!session_id()) {
    session_start();
}

$_SESSION["pesquisa"] = null;
$var = $_SESSION['usuario'];
require_once '../controller/UsuarioController.php';
$array2 = UsuarioController::mostrarUsuariosPorId($_SESSION["usuario"][0]);
require_once "../controller/CategoriaController.php";
require_once '../controller/LivroController.php';

if (!isset($_SESSION["usuario"])) {
    header("location:../controller/sessionDestroy.php");
}

if (!isset($_SESSION["aviso"])):
    $aviso = LivroController::mostrarEmprestimosPorId($array2[0]);
    if (!empty($aviso)):
        foreach ($aviso as $av) :
            $data1 = new DateTime();
            $dataEng = str_replace("/", "-", $av[3]);
            $data2 = new DateTime($dataEng);
            $intervalo = $data1->diff($data2);
            $contDias = $intervalo->format('%r%a') + 1;
            if ($contDias <= 2):
                $msg = "Você deve entregar o livro " . $av[4] . " em " . $contDias . " dia(s). Não esqueça disso!";
                if ($contDias <= 0):
                    $msg = "Entregue, urgentemente, o livro " . $av[4];
                endif;
            //echo $msg;
            endif;
        endforeach;
        $_SESSION["aviso"] = "avisado!";
    endif;
endif;
?>


<?php
if (isset($_GET["categoria"])) {

    $categoria = $_GET["categoria"];
    $arrayTeste = LivroController::mostrarLivrosPorCategoria($_SESSION["idCat"]);
    foreach ($arrayTeste as $value) {
        $array[] = array($value[1], $value[2], $value[3], $value[4], $value[5], $value[6], $value[7], $value[8], $value[9], $value[10], $value[11], $value[12]);
    }
    
    $inicial = ($categoria - 1) * 5;
    $final = $inicial + 5;
    $contador = 0;

    foreach ($array as $valorPag):
        if ($contador >= $inicial and $contador < $final):
            $arrayPag[] = $valorPag;
        endif;
        $contador++;
    endforeach;
    
} elseif (!isset($_GET["pesquisa"])) {
    if (isset($_GET["pagina"])):
        $pagina = $_GET["pagina"];
    else:
        $pagina = 1;
    endif;

    $array = LivroController::mostrarTodosLivros();

    $inicial = ($pagina - 1) * 5;
    $final = $inicial + 5;
    $contador = 0;

    foreach ($array as $valorPag):
        if ($contador >= $inicial and $contador < $final):
            $arrayPag[] = $valorPag;
        endif;
        $contador++;
    endforeach;
}else {
    $array = LivroController::mostrarLivrosPorPesquisa($_SESSION["value"]);

    $inicial = ($_GET["pesquisa"] - 1) * 5;
    $final = $inicial + 5;
    $contador = 0;

    foreach ($array as $valorPag):
        if ($contador >= $inicial and $contador < $final):
            $arrayPag[] = $valorPag;
        endif;
        $contador++;
    endforeach;
}
?>



<html>
    <head>
        <meta charset="UTF-8">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Inicial</title>
    </head>
    <script>
        $(document).ready(function () {
            $('.modal').modal();
            $('.sidenav').sidenav();
            $(".dropdown-trigger").dropdown();
            $(".collection-header").click(function () {
                $("tr").hide();
                $("tr").show();
                $("#paginacao").show();
            });
            //$("#paginacao").hide();
            //console.log(JSON.parse(json))
            //json = JSON.parse(json);

            /*if (json.lenght > 5){
             
             $("#paginacao").show();
             }*/

            /*if (Number(JSON.parse(json).length) > 5){
             
             /*var totalPages = Math.floor(JSON.parse(json).length) / 5;
             $("page").each(function (idx2, el2) {
             alert(Number(el2))
             });
             }*/



            /*$(".collection-item").click(function () {
             var contador = 0;
             $("tbody tr").hide();
             $("#paginacao").hide();
             var catClick = $(this).attr("name");
             $($(".categorias")).each(function (idx, el) {
             if (catClick === $(el).attr("name")) {
             $("tr").each(function (idx2, el2) {
             if ($(el2).attr("name") === $(el).attr("title")) {
             $("tbody").show();
             $(el2).show();
             }
             });
             }
             });
             });*/

            $("input.autocomplete").autocomplete({
                data: {
<?php
if (!empty($array)):
    foreach ($array as $valor):
        echo "'$valor[1]' : null,";
    endforeach;
endif;
?>
                },
                limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
                minLength: 1 // The minimum length of the input for the autocomplete to start. Default: 1.
            });
            $("#pesquisa").focus(function () {
                $(this).keyup(function () {
                    $.get("../controller/recebeAutoComplete.php", {"value": $("#pesquisa").val()}, function (data) {
                        $("#dados").html(data);
                    });
                });
            });
        });</script>
    <body class="grey lighten-5">

        <nav class="nav-extended">
            <!-- Drop down -->

            <ul id="dropdown1" class="dropdown-content">
                <li><a href="telaMeuPerfil.php"><i class="material-icons prefix">person</i>Perfil</a></li>
                <li><a href="../controller/sessionDestroy.php"><i class="material-icons prefix">exit_to_app</i>Sair</a></li>
            </ul>

            <?php
            $notif = 0;
            if (!empty(LivroController::mostrarLivrosPorPessoa($var[1], $var[2]))) {
                foreach (LivroController::mostrarLivrosPorPessoa($var[1], $var[2]) as $cont) {
                    if ($cont[10] == 2) {
                        $notif++;
                    }
                }
            }
            ?>

            <div class="nav-wrapper teal">
                <a href="telaPrincipal.php" class="brand-logo"><img class="responsive-img" src="../imagens/Logo 2.png" /></a>
                <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><i class="material-icons prefix">add</i></li>
                    <li><a href="telaCadastrarLivro.php">Cadastrar novo livro</a></li>
                    <li><i class="material-icons prefix">collections</i></li>
                    <li><a href="telaSeusLivros.php">
                            Visualizar seus livros
                            <?php if (isset($notif) and $notif != 0): ?>
                                <span class="new badge blue"><?= $notif; ?></span> 
                            <?php endif; ?> 
                        </a>
                    </li>
                    <li><i class="material-icons prefix">notifications</i></li>
                    <li><a href="telaLivrosParaEntregar.php">Livros para entregar</a></li>
                    <li><i class="material-icons prefix">book</i></li>
                    <li><a href="telaLivrosReservados.php">Livros reservados</a></li>
                    <li><a class="dropdown-trigger" href="#!" data-target="dropdown1">Meu perfil<i class="material-icons right">face</i></a></li>
                </ul>
            </div>



            <div class="nav-content teal"><br>
                <ul class="tabs tabs-transparent row">
                    <?php
                    if (UsuarioController::identificarTipo($array2[0]) == 1) {
                        $_SESSION["admin"] = true;
                        ?>
                        <li class="tab col s2"><a href="telaGerenciarCategorias.php">Gerenciar Categorias</a></li>
                        <?php
                    }
                    ?>
                    <li class="tab col s4" style="margin-left: 10px;">
                        <div class="input-field">
                            <input id="pesquisa" type="text" class="grey lighten-5" autocomplete="off" placeholder="Digite algo aqui" style="border: 1px solid grey; border-radius: 2%;" id="autocomplete-input" class="autocomplete black-text">
                        </div>
                    </li>
                    <li class="tab col s2"></li>
                    <li class="tab">
                </ul><br>
            </div>

        </nav>

        <ul class="sidenav" id="mobile-demo">
            <li><a href="telaCadastrarLivro.php">Cadastrar Livro</a></li>
            <li>
                <a href="telaSeusLivros.php">
                    <?php if (isset($notif) and $notif != 0): ?>
                        <span class="new badge blue"><?= $notif; ?></span> 
                    <?php endif; ?> 
                    Visualizar seus livros
                </a>
            </li>
            <li><a href="telaLivrosParaEntregar.php">Livros para entregar</a></li>
            <li><a href="telaLivrosReservados.php">Livros reservados</a></li>
            <li><a href="../controller/sessionDestroy.php">Sair</a></li>
        </ul>



        <?php
        $nome = explode(" ", $array2[1]);
        ?>


        <div class="row">
            <div class="col s4 m3 l2">
                <ul class="collection with-header" style="margin-top: 0px; margin-left: -10px;">
                    <li class="collection-header center-align" ><a href=""><h4><i>Categorias</i></h4></a></li>
                    <?php
                    $todasCategorias = CategoriaController::mostrarAutoComplete();
                    foreach ($todasCategorias as $cat):
                        echo '<a href="#"><li class="collection-item" name="' . $cat[0] . '">' . $cat[1] . '</li></a>';
                    endforeach;
                    ?>
                </ul>
            </div>
            <div class="col s8 offset-s1">
                <h3 class="center blue-grey-text">Bem vindo <?php echo $nome[0]; ?></h3>
                <br>
                <?php
                if (!empty($arrayPag)):
                    ?>
                    <div id="dados">
                        <table class="centered responsive-table">
                            <thead>
                                <tr>
                                    <th>Título</th>
                                    <th>Autor</th>
                                    <th>Estado</th>
                                    <th></th>
                                    <th></th>

                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                foreach ($arrayPag as $val):
                                    ?>

                                    <tr name="<?= $val[0]; ?>">
                                        <td><?php echo $val[1]; ?></td>
                                        <td><?php echo $val[2]; ?></td>
                                        <td>
                                            <?php
                                            $disponivel = false;
                                            if ($val[10] == 0):
                                                echo "Disponível";
                                                $disponivel = true;
                                            endif;
                                            if ($val[10] == 1):
                                                echo "Indisponível";
                                            endif;
                                            if ($val[10] == 2):
                                                echo "Reservado";
                                            endif;
                                            ?>
                                        </td>
                                        <td><a class="modal-trigger" href="#modalVisibility<?php echo $val[0]; ?>"><i class="material-icons prefix blue-text">visibility</i></a></td>
                                        <?php
                                        $dono = UsuarioController::buscarUsuariosPorEmail($val[9]);
                                        if ($disponivel and $array2[2] != $dono[2]) {
                                            $disponivel = false;
                                            ?>
                                            <td>
                                                <form method="POST" action="../controller/LivroController.php">
                                                    <input type="hidden" name="reservar" value="reservar"/>
                                                    <input type="hidden" name="idLivro" value="<?php echo $val[0]; ?>"/>
                                                    <input type="hidden" name="idUsuario" value="<?php echo $var[0]; ?>"/>
                                                    <button class="btn waves-effect waves-light orange white-text" type="submit" name="action">Reservar
                                                    </button>
                                                </form>
                                            </td>
                                        <?php } else { ?>
                                            <td></td>
                                        <?php }
                                        ?>

                                        <!-- MODAL VISIBILITY -->
                                <div id="modalVisibility<?php echo $val[0]; ?>" class="modal" name="boi">
                                    <div class="modal-content">
                                        <div class="row">
                                            <div class="col s6" style="border: 1px solid grey">
                                                <h4 class="center"><?php echo $val[1]; ?></h4>
                                                <?php echo "<b>Autor: </b>" . $val[2]; ?> <br>
                                                <?php echo "<b>Edição: </b>" . $val[3]; ?> <br>
                                                <?php echo "<b>Editora: </b>" . $val[4]; ?> <br>
                                                <?php echo "<b>Número de páginas: </b>" . $val[5]; ?> <br>
                                                <?php
                                                echo "<b>Categorias: </b>";
                                                $categorias = CategoriaController::mostrarCategoriasPorLivro($val[0]);
                                                if (!empty($categorias)):
                                                    foreach ($categorias as $valorr):
                                                        ?>
                                                        <div class="categorias" title="<?= $val[0]; ?>" name="<?php echo $valorr[0]; ?>"> <?php echo "♦ " . $valorr[1]; ?></div><?php
                                                    endforeach;
                                                endif;
                                                ?>
                                                <?php echo "<b>Disponibilidade: </b>" . $val[6]; ?> <br>
                                                <?php
                                                echo "<b>Estado: </b>";
                                                if ($val[9] == 0) {
                                                    echo 'Disponível';
                                                }
                                                if ($val[9] == 1) {
                                                    echo 'Indisponível';
                                                }
                                                if ($val[9] == 2) {
                                                    echo 'Reservado';
                                                }
                                                ?> <br>
                                                <hr>
                                                <?php echo "<b>Pertence à: </b>" . $dono[1]; ?> <br>
                                                <?php echo "<b>Telefone: </b>" . $dono[4]; ?> <br>
                                                <?php echo "<b>Email: </b>" . $dono[2]; ?> <br>

                                                <?php
                                                $historico = LivroController::mostrarHistoricoPorLivro($val[0]);
                                                if (!empty($historico)) {
                                                    echo "<hr><b><center>Histórico do livro</center> </b>";
                                                    foreach ($historico as $v) {
                                                        if (!empty($v[2])) {
                                                            $usuarioHistorico = UsuarioController::mostrarUsuariosPorId($v[1]);
                                                            echo "<b>Utilizado por: </b>" . $usuarioHistorico[1] . "<br>";
                                                            echo "<b>Email: </b>" . $usuarioHistorico[2] . "<br>";
                                                            echo "<b>Data inicial: </b>" . $v[3] . "<br>";
                                                            echo "<b>Data da entrega: </b>" . $v[2] . "<br>";
                                                            echo "<b>Data limite da entrega: </b>" . $v[4] . "<br><br>";
                                                        }
                                                    }
                                                }
                                                ?>
                                            </div>
                                            <div class="col s4 right">
                                                <?php if (isset($val[11]) && !empty($val[11])) {
                                                    ?>
                                                    <h4 class="center">Capa do livro</h4>
                                                    <img src="../upload/<?php echo $val[11]; ?>" class="responsive-img" style="height: 250px; border: 1px solid grey"/>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <div class="modal-content">
                                                        <h4 class="center blue-grey-text">Não encontramos nenhuma imagem da capa deste livro.</h4>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="" class="modal-close waves-effect waves-green btn-flat">Fechar</a>
                                    </div>
                                </div>
                                </tr>

                                <?php
                            endforeach;
                            ?>
                            </tbody>
                        </table>
                        <div class="row">
                            <?php
                            if (isset($_GET["categoria"])) {
                                $pagina = $categoria;
                                if (count($array) > 0) {
                                    $numero = ((count($array) - 0.1) / 5);
                                    $numeroPagina = floor((count($array) - 0.1) / 5);
                                } else {
                                    $numero = 1;
                                    $numeroPagina = 1;
                                }
                                //echo $numeroPagina;
                                if ($numero > 1) {
                                    ?>
                                    <ul class="pagination center " id="paginacao">
                                        <?php if ($pagina == 1) { ?>
                                            <li class="disabled"><i class="material-icons">chevron_left</i></li>
                                        <?php } else { ?>
                                            <li class="waves-effect"><a href="telaPrincipal.php?categoria=<?= $pagina - 1; ?>"><i class="material-icons">chevron_left</i></a></li>
                                        <?php } ?>

                                        <?php
                                        for ($x = 0; $x <= $numeroPagina; $x++) {
                                            if ($pagina == $x + 1) {
                                                ?>
                                                <li class="active"><a href="#!"><?= $x + 1; ?></a></li>
                                            <?php } else { ?>
                                                <li class="waves-effect page"><a href="telaPrincipal.php?categoria=<?= $x + 1; ?>"><?= $x + 1; ?></a></li>
                                                <?php
                                            }
                                        }
                                        if ($numeroPagina + 1 == $pagina) {
                                            ?>
                                            <li class="disabled"><i class="material-icons">chevron_right</i></li>
                                        <?php } else { ?>
                                            <li class="waves-effect"><a href="telaPrincipal.php?categoria=<?= $pagina + 1; ?>"><i class="material-icons">chevron_right</i></a></li>
                                        <?php } ?>
                                    </ul>
                                    <?php
                                }
                            } elseif (!isset($_GET["pesquisa"])) {

                                if (count($array) > 0) {
                                    $numero = ((count($array) - 0.1) / 5);
                                    $numeroPagina = floor((count($array) - 0.1) / 5);
                                } else {
                                    $numero = 1;
                                    $numeroPagina = 1;
                                }
                                //echo $numeroPagina;
                                if ($numero > 1) {
                                    ?>
                                    <ul class="pagination center " id="paginacao">
                                        <?php if ($pagina == 1) { ?>
                                            <li class="disabled"><i class="material-icons">chevron_left</i></li>
                                        <?php } else { ?>
                                            <li class="waves-effect"><a href="telaPrincipal.php?pagina=<?= $pagina - 1; ?>"><i class="material-icons">chevron_left</i></a></li>
                                        <?php } ?>

                                        <?php
                                        for ($x = 0; $x <= $numeroPagina; $x++) {
                                            if ($pagina == $x + 1) {
                                                ?>
                                                <li class="active"><a href="#!"><?= $x + 1; ?></a></li>
                                            <?php } else { ?>
                                                <li class="waves-effect page"><a href="telaPrincipal.php?pagina=<?= $x + 1; ?>"><?= $x + 1; ?></a></li>
                                                <?php
                                            }
                                        }
                                        if ($numeroPagina + 1 == $pagina) {
                                            ?>
                                            <li class="disabled"><i class="material-icons">chevron_right</i></li>
                                        <?php } else { ?>
                                            <li class="waves-effect"><a href="telaPrincipal.php?pagina=<?= $pagina + 1; ?>"><i class="material-icons">chevron_right</i></a></li>
                                        <?php } ?>
                                    </ul>
                                    <?php
                                }
                            } else {
                                $pagina = $_GET["pesquisa"];
                                if (count($array) > 0) {
                                    $numero = ((count($array) - 0.1) / 5);
                                    $numeroPagina = floor((count($array) - 0.1) / 5);
                                } else {
                                    $numero = 1;
                                    $numeroPagina = 1;
                                }
                                //echo $numeroPagina;
                                if ($numero > 1) {
                                    ?>
                                    <ul class="pagination center">
                                        <?php if ($pagina == 1) { ?>
                                            <li class="disabled"><i class="material-icons">chevron_left</i></li>
                                        <?php } else { ?>
                                            <li class="waves-effect"><a href="telaPrincipal.php?pesquisa=<?= $pagina - 1; ?>"><i class="material-icons">chevron_left</i></a></li>
                                        <?php } ?>

                                        <?php
                                        for ($x = 0; $x <= $numeroPagina; $x++) {
                                            if ($pagina == $x + 1) {
                                                ?>
                                                <li class="active"><a href="#!"><?= $x + 1; ?></a></li>
                                            <?php } else { ?>
                                                <li class="waves-effect"><a href="telaPrincipal.php?pesquisa=<?= $x + 1; ?>"><?= $x + 1; ?></a></li>
                                                <?php
                                            }
                                        }
                                        if ($numeroPagina + 1 == $pagina) {
                                            ?>
                                            <li class="disabled"><i class="material-icons">chevron_right</i></li>
                                        <?php } else { ?>
                                            <li class="waves-effect"><a href="telaPrincipal.php?pesquisa=<?= $pagina + 1; ?>"><i class="material-icons">chevron_right</i></a></li>
                                            <?php } ?>
                                    </ul>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <?php
                endif;
                ?>
            </div>
        </div>



        <?php
        if (!empty($_SESSION['sweet']) && isset($_SESSION['sweet'])) {
            if ($_SESSION['sweet'] == "Livro reservado com sucesso.") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'O livro não poderá ser emprestado para outra pessoa!', 'success');</script>";
                $_SESSION['sweet'] = null;
            }

            if ($_SESSION["sweet"] == "Você já está com muitas reservas ou empréstimos!") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', '', 'error');</script>";
                $_SESSION['sweet'] = null;
            }

            if ($_SESSION['sweet'] == "Livro cadastrado com sucesso.") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'Você acaba de disponibilizar um livro no sistema!', 'success');</script>";
                $_SESSION['sweet'] = null;
            }
        }

        if (!empty($msg) && isset($msg)) {
            echo "<script> swal('" . $msg . "', 'O livro não pertence a você', 'warning');</script>";
            $msg = null;
        }
        ?>


        <script>
            $(".collection-item").click(function () {
                $.get("../controller/recebeAutoComplete.php", {"valueCat": $(this).attr("name")}, function (json) {
                    if (JSON.parse(json) !== null) {
                        $("tbody").show();
                        if (Number(JSON.parse(json).length) > 5) {
                            /*$("#paginacao").show();
                             $(".page").each(function (idx2, el2) {
                             if ((Math.floor((JSON.parse(json).length + 1) / 5)) < Number($(el2).text())) {
                             $(el2).hide();
                             }
                             });*/
                            $.get("../controller/recebeAutoComplete.php", {"valueCat2": json, "var": 1}, function (data) {
                                $("#dados").html(data);
                            });
                        } else {
                            $("#paginacao").hide();
                            $.get("../controller/recebeAutoComplete.php", {"valueCat2": json, "var": 0}, function (data) {
                                $("#dados").html(data);
                            });
                        }
                    } else {
                        $("#paginacao").hide();
                        $("tbody").hide();
                    }
                });
            });


        </script>

    </body>
</html>
