<?php
if (!session_id()) {
    session_start();
}

if (!isset($_SESSION["admin"])) {
    header("location:../controller/sessionDestroy.php");
}

require_once '../controller/LivroController.php';
require_once "../controller/CategoriaController.php";
?>

<html>
    <head>
        <meta charset="UTF-8">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <script type="text/javascript" src="../static/main.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Todos Livros</title>
    </head>

    <script>
        $(document).ready(function () {
            $('.modal').modal();
            $('.sidenav').sidenav();
            $(".dropdown-trigger").dropdown();

            $("input.autocomplete").autocomplete({
                data: {
<?php
$todosLivros = LivroController::mostrarTodosLivros();
if (!empty($todosLivros)):
    foreach ($todosLivros as $valor):
        echo "'$valor[1]' : null,";
    endforeach;
endif;
?>
                },
                limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
                minLength: 1 // The minimum length of the input for the autocomplete to start. Default: 1.
            });

        });
    </script>

    <body class="grey lighten-5">
        <nav>
            <div class="nav-wrapper teal">
                <a href="#!" class="brand-logo"><img class="responsive-img" src="../imagens/Logo 2.png" /></a>
                <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                    <li><i class="material-icons prefix">group</i></li>
                    <li><a href="telaGerenciarAdmins.php">Gerenciar Administradores</a></li>
                    <li><i class="material-icons prefix">add</i></li>
                    <li><a href="telaCadastrarLivro.php">Cadastrar um novo livro</a></li>
                    <li><i class="material-icons prefix">library_books</i></li>
                    <li><a href="telaGerenciarEmprestimos.php">Visualizar Empréstimos</a></li>
                    <li><i class="material-icons prefix">exit_to_app</i></li>
                    <li><a href="../controller/sessionDestroy.php">Sair</a></li>
                </ul>
            </div>
        </nav>

        <ul class="sidenav" id="mobile-demo">
            <li><a href="telaGerenciarAdmins.php">Gerenciar Administradores</a></li>
            <li><a href="telaCadastrarLivro.php">Cadastrar um novo livro</a></li>
            <li><a href="telaGerenciarEmprestimos.php">Visualizar Empréstimos</a></li>
            <li><a href="../controller/sessionDestroy.php">Sair</a></li>
        </ul>

        <?php
        if (isset($_GET["pagina"])) {
            $pagina = $_GET["pagina"];
        } else {
            $pagina = 1;
        }
        ?>

        <h3 class="center blue-grey-text">Livros Disponíveis</h3><br>
        <div class="row">

            <!-- Pesquisar livros -->
            <div class="col s10">
                <form method="POST" action="../controller/LivroController.php">
                    <input type="hidden" name="pesquisar" value="pesquisar" >
                    <div class="input-field col s5 m4 l3">
                        <input type="text" autocomplete="off" name="pesquisa" style="border: 1px solid" placeholder="Digite o nome do livro" id="autocomplete-input" class="autocomplete black-text"/>
                    </div>
                    <div class="input-field col s2">
                        <button class="btn waves-effect waves-light blue darken-4 btn-large" style="margin-left: -17px;" type="submit" name="action">Pesquisar</button>
                    </div>
                </form>
            </div>
        </div>


        <div class="row">
            <div class="col s10 offset-s1">


                <?php
                if (isset($_GET["pesquisa"])) {
                    $array = LivroController::pesquisarTodosLivrosPorPagination($_GET["pesquisa"]);
                } else {
                    $array = LivroController::mostrarPorPagination($pagina);
                }


                if (!empty($array)):
                    ?>
                    <table class="centered responsive-table">
                        <thead>
                            <tr>
                                <th>Título</th>
                                <th>Autor</th>
                                <th>Estado</th>
                                <th></th>
                                <th></th>
                                <th></th>

                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            foreach ($array as $val):
                                ?>
                                <tr>
                                    <td><?php echo $val[1]; ?></td>
                                    <td><?php echo $val[2]; ?></td>
                                    <td>
                                        <?php
                                        if ($val[10] == 0):
                                            echo "Disponível";
                                        endif;
                                        if ($val[10] == 1):
                                            echo "Indisponível";
                                        endif;
                                        if ($val[10] == 2):
                                            echo "Reservado";
                                        endif;
                                        ?>
                                    </td>
                                    <td><a class="modal-trigger" href="#modalVisibility<?php echo $val[0]; ?>"><i class="material-icons prefix blue-text">visibility</i></a></td>
                                    <td>
                                        <form method="POST" action="telaEditarLivro.php">
                                            <input type="hidden" name="idLivro" value="<?php echo $val[0]; ?>"/>
                                            <a class="btn-flat" style="margin-top: 10%;" onclick='this.parentNode.submit();
                                                            return false;' ><i class="material-icons prefix green-text">edit</i></a>
                                        </form>
                                    </td>
                                    <td><a class="modal-trigger" href="#modalRemove<?php echo $val[0]; ?>"><i class="material-icons prefix red-text">delete</i></a></td>
                                </tr>

                                <!-- MODAL DELETE -->
                            <div id="modalRemove<?php echo $val[0]; ?>" class="modal">
                                <div class="modal-content">
                                    <center><img src="../imagens/Ponto de exclamacao.jpg" style="height: 100px;"/></center><br>
                                    <h4 class="center blue-grey-text">Você tem certeza disso?</h4>
                                    <p class="center blue-grey-text">O livro  "<?php echo $val[1]; ?>" será removido e isso não poderá ser revertido. </p>
                                    <center class='white-text'>
                                        <form method="POST" action="../controller/LivroController.php">
                                            <input type="hidden" name="deletarLivro" value="deletarLivro"/>
                                            <input type="hidden" name="idLivro" value="<?php echo $val[0]; ?>"/>
                                            <button class="modal-close waves-effect waves-light btn-small blue" type="submit" name="action">Confirmar</button>
                                        </form>
                                        <a class="modal-close waves-effect waves-light btn-small red">Cancelar</a>
                                    </center>
                                </div>
                            </div>


                            <!-- MODAL VISIBILITY -->
                            <div id="modalVisibility<?php echo $val[0]; ?>" class="modal">
                                <div class="modal-content">
                                    <div class="row">
                                        <div class="col s6" style="border: 1px solid grey">
                                            <h4 class="center"><?php echo $val[1]; ?></h4>
                                            <?php echo "<b>Autor: </b>" . $val[2]; ?> <br>
                                            <?php echo "<b>Edição: </b>" . $val[3]; ?> <br>
                                            <?php echo "<b>Editora: </b>" . $val[4]; ?> <br>
                                            <?php echo "<b>Número de páginas: </b>" . $val[5]; ?> <br>
                                            <?php
                                            echo "<b>Categorias: </b>";
                                            $categorias = CategoriaController::mostrarCategoriasPorLivro($val[0]);
                                            if (!empty($categorias)):
                                                foreach ($categorias as $valorr):
                                                    echo $valorr[1] . "; ";
                                                endforeach;
                                            endif;
                                            ?><br>
                                            <?php echo "<b>Disponibilidade: </b>" . $val[5]; ?> <br>
                                            <?php
                                            echo "<b>Estado: </b>";
                                            if ($val[10] == 0) {
                                                echo 'Disponível';
                                            }
                                            if ($val[10] == 1) {
                                                echo 'Indisponível';
                                            }
                                            if ($val[10] == 2) {
                                                echo 'Reservado';
                                            }
                                            ?> <br>
                                            <hr>
                                            <?PHP $dono = UsuarioController::buscarUsuariosPorEmail($val[9]) ?>
                                            <?php echo "<b>Pertence à: </b>" . $dono[1]; ?> <br>
                                            <?php echo "<b>Telefone: </b>" . $dono[4]; ?> <br>
                                            <?php echo "<b>Email: </b>" . $dono[2]; ?> <br>

                                            <?php
                                            $historico = LivroController::mostrarHistoricoPorLivro($val[0]);
                                            if (!empty($historico)) {
                                                echo "<hr><b><center>Histórico do livro</center> </b>";
                                                foreach ($historico as $v) {
                                                    if (!empty($v[2])) {
                                                        $usuarioHistorico = UsuarioController::mostrarUsuariosPorId($v[1]);
                                                        echo "<b>Utilizado por: </b>" . $usuarioHistorico[1] . "<br>";
                                                        echo "<b>Email: </b>" . $usuarioHistorico[2] . "<br>";
                                                        echo "<b>Data inicial: </b>" . $v[3] . "<br>";
                                                        echo "<b>Data da entrega: </b>" . $v[2] . "<br>";
                                                        echo "<b>Data limite da entrega: </b>" . $v[4] . "<br><br>";
                                                    }
                                                }
                                            }
                                            ?>
                                        </div>
                                        <div class="col s4 right">
                                            <?php if (isset($val[11]) && !empty($val[11])) {
                                                ?>
                                                <h4 class="center">Capa do livro</h4>
                                                <img src="../upload/<?php echo $val[11]; ?>" class="responsive-img" style="height: 250px; border: 1px solid grey"/>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="modal-content">
                                                    <h4 class="center blue-grey-text">Não encontramos nenhuma imagem da capa deste livro.</h4>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <a href="" class="modal-close waves-effect waves-green btn-flat">Fechar</a>
                                </div>
                            </div>


                            <?php
                        endforeach;
                        ?>
                        </tbody>
                    </table>
                    <?php
                endif;
                ?>
            </div>
        </div>
        <div class="row center">

            <?php
            if (isset($array)) {
                if (isset($_GET["pesquisa"])) {

                    if ($_SESSION["countPesquisa"] > 0) {
                        $numeroPagina = floor(($_SESSION["countPesquisa"] - 0.1) / 5);
                    } else {
                        $numeroPagina = 1;
                    }
                    ?>
                    <ul class="pagination">
                        <?php if ($_GET["pesquisa"] == 1) { ?>
                            <li class="disabled"><i class="material-icons">chevron_left</i></li>
                        <?php } else { ?>
                            <li class="waves-effect"><a href="telaTodosLivros.php?pesquisa=<?= $_GET["pesquisa"] - 1; ?>"><i class="material-icons">chevron_left</i></a></li>
                        <?php } ?>

                        <?php
                        for ($x = 0; $x <= $numeroPagina; $x++) {
                            if ($_GET["pesquisa"] == $x + 1) {
                                ?>
                                <li class="active"><a href="#!"><?= $x + 1; ?></a></li>
                            <?php } else { ?>
                                <li class="waves-effect"><a href="telaTodosLivros.php?pesquisa=<?= $x + 1; ?>"><?= $x + 1; ?></a></li>
                                <?php
                            }
                        }
                        if ($numeroPagina + 1 == $_GET["pesquisa"]) {
                            ?>
                            <li class="disabled"><i class="material-icons">chevron_right</i></li>
                        <?php } else { ?>
                            <li class="waves-effect"><a href="telaTodosLivros.php?pesquisa=<?= $_GET["pesquisa"] + 1; ?>"><i class="material-icons">chevron_right</i></a></li>
                        <?php } ?>
                    </ul>
                    <?php
                } else {


                    //verifica numero de paginas. Se não houver categorias, não posso fazer 0/10, pois haverá uma página;
                    if (count($todosLivros) > 0) {
                        $numeroPagina = floor((count($todosLivros) - 0.1) / 5);
                    } else {
                        $numeroPagina = 1;
                    }
                    ?>

                    <ul class="pagination">
                        <?php if ($pagina == 1) { ?>
                            <li class="disabled"><i class="material-icons">chevron_left</i></li>
                        <?php } else { ?>
                            <li class="waves-effect"><a href="telaTodosLivros.php?pagina=<?= $pagina - 1; ?>"><i class="material-icons">chevron_left</i></a></li>
                        <?php } ?>

                        <?php
                        for ($x = 0; $x <= $numeroPagina; $x++) {
                            if ($pagina == $x + 1) {
                                ?>
                                <li class="active"><a href="#!"><?= $x + 1; ?></a></li>
                            <?php } else { ?>
                                <li class="waves-effect"><a href="telaTodosLivros.php?pagina=<?= $x + 1; ?>"><?= $x + 1; ?></a></li>
                                <?php
                            }
                        }
                        if ($numeroPagina + 1 == $pagina) {
                            ?>
                            <li class="disabled"><i class="material-icons">chevron_right</i></li>
                        <?php } else { ?>
                            <li class="waves-effect"><a href="telaTodosLivros.php?pagina=<?= $pagina + 1; ?>"><i class="material-icons">chevron_right</i></a></li>
                            <?php } ?>
                    </ul>

                <?php
                }
            }
            ?>
        </div>


        <?php
        //$_SESSION["pesquisar"] = null;
        if (!empty($_SESSION['sweet']) && isset($_SESSION['sweet'])) {
            if ($_SESSION['sweet'] == "Livro excluído com sucesso.") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'O livro não pertence mais a esta associação!', 'success');</script>";
                $_SESSION['sweet'] = null;
            } else if ($_SESSION["sweet"] == "Livro cadastrado com sucesso.") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'O livro já está pronto para ser emprestado!', 'success');</script>";
                $_SESSION['sweet'] = null;
            }
            if ($_SESSION['sweet'] == "Livro editado com sucesso.") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'O livro foi modificado e está novamente na lista!', 'success');</script>";
                $_SESSION['sweet'] = null;
            }
        }
        ?>


    </body>
</html>

