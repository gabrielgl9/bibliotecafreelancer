<?php
if (!session_id()) {
    session_start();
}
require_once '../controller/UsuarioController.php';
$array = UsuarioController::mostrarUsuariosPorId($_SESSION["usuario"][0]);

if (!isset($_SESSION["usuario"])) {
    header("location:../controller/sessionDestroy.php");
}
?>
<!-- Esta é uma nova tela -->
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Meu Perfil</title>
    </head>
    <body>
    <body class="grey lighten-5">
        <nav>
            <div class="nav-wrapper teal">
                <div class="col s12">
                    <a href="telaPrincipal.php" class="breadcrumb">Menu principal</a>
                    <a href="" class="breadcrumb">Meu Perfil</a>
                </div>
            </div>
        </nav>
        <h3 class="center blue-grey-text">Meu Perfil</h3><br>
        <div class="row">
            <div class="col s10 m8 l6 offset-s1 offset-m2 offset-l3 card-panel">
                <div class="row">

                    <div class="input-field col s6">
                        <i class="material-icons prefix">account_circle</i>
                        <label>NOME COMPLETO: <?= $array[1]; ?></label>
                    </div>
                    <div class="input-field col s6">
                        <i class="material-icons prefix">settings_applications</i>
                        <label>CPF: <?= $array[6]; ?></label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <i class="material-icons prefix">phone_android</i>
                        <label>CELULAR: <?= $array[5]; ?></label>
                    </div>
                    <div class="input-field col s6">
                        <i class="material-icons prefix">phone</i>
                        <label>TELEFONE: <?= $array[4]; ?></label>

                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <i class="material-icons prefix">email</i>
                        <label>E-MAIL: <?= $array[2]; ?></label>

                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <i class="material-icons prefix">home</i>
                        <label>CEP: <?= $array[7]; ?></label>

                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <i class="material-icons prefix">public</i>
                        <label>ESTADO: <?= $array[8]; ?></label>

                    </div>
                    <div class="input-field col s6">
                        <i class="material-icons prefix">location_city</i>
                        <label>CIDADE: <?= $array[9]; ?></label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <i class="material-icons prefix">domain</i>
                        <label>BAIRRO: <?= $array[10]; ?></label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <!--domain -->
                        <i class="material-icons prefix">streetview</i>
                        <label>LOGRADOURO: <?= $array[11]; ?></label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s6">
                        <i class="material-icons prefix">add_location</i>
                        <label>NÚMERO DA RESIDÊNCIA: <?= $array[12]; ?></label>

                    </div>
                    <div class="input-field col s6">
                        <i class="material-icons prefix">settings_applications</i>
                        <label>COMPLEMENTO: <?= $array[13]; ?></label>
                    </div>
                </div><br>
            </div>
        </div>

        <div class="center">
            <a href="telaEditarPerfil.php" class="btn waves-effect waves-light col s12">Editar Perfil
            </a>
        </div>

        <?php
        if (!empty($_SESSION['sweet']) && isset($_SESSION['sweet'])) {
            if ($_SESSION['sweet'] == "Editado com sucesso.") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'Os seus dados foram modificados', 'success');</script>";
                $_SESSION['sweet'] = null;
            }
        }
        ?>
    </body>
</html>
