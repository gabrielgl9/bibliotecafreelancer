<?php
if (!session_id()) {
    session_start();
}

if (!isset($_SESSION["usuario"])) {
    header("location:../controller/sessionDestroy.php");
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <script type="text/javascript" src="../static/main.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Livros Reservados</title>
    </head>
    <body class="grey lighten-5">
        <nav>
            <div class="nav-wrapper teal">
                <div class="col s12">
                    <a href="telaPrincipal.php" class="breadcrumb">Menu principal</a>
                    <a href="" class="breadcrumb">Livros Reservados</a>
                </div>
            </div>
        </nav>
        <?php
        require_once '../controller/LivroController.php';
        require_once "../controller/CategoriaController.php";
        require_once '../model/LivroModel.php';

        $array = LivroController::mostrarLivrosReservados($_SESSION['usuario'][0]);
        if (!empty($array)):
            ?>
            <h3 class="center blue-grey-text">Livros reservados</h3><br>

            <div class="row">
                <div class="col s10 offset-s1">

                    <table class="centered responsive-table">
                        <thead>
                            <tr>
                                <th>Título</th>
                                <th>Autor</th>
                                <th>Dono</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            foreach ($array as $val):
                                $data1 = new DateTime();
                                $dataEng = str_replace("/", "-", $val[13]);
                                $data2 = new DateTime($dataEng);
                                $intervalo = $data1->diff($data2);
                                $contDias = $intervalo->format('%r%a') + 1;
                                if ($contDias <= 2):
                                    echo "<tr class='orange accent-1'>";
                                    $var = true;
                                    if ($contDias <= 0):
                                        $livroModel = new LivroModel();
                                        $livroModel->setIdLivro($val[0]);
                                        $livroModel->removerLivroReservado();
                                        header("location: telaLivrosReservados.php");
                                    endif;
                                else:
                                    echo "<tr>";
                                endif;
                                ?>

                                <?PHP $dono = UsuarioController::buscarUsuariosPorEmail($val[9]) ?>
                            <td><?php echo $val[1]; ?></td>
                            <td><?php echo $val[2]; ?></td>
                            <td><?php echo $dono[1]; ?></td>
                            <td><a class="modal-trigger" href="#modalVisibility<?php echo $val[0]; ?>"><i class="material-icons prefix blue-text">visibility</i></a></td>
                            <td><a class="modal-trigger" href="#modalRemove<?php echo $val[0]; ?>"><i class="material-icons prefix red-text">cancel</i></a></td>
                            <?php if (isset($var) && !empty($var)): ?>
                                <td><a class='modal-trigger white-text btn orange darken-4' href="#modalExclamacao<?php echo $val[0]; ?>">TEMPO SE ESGOTANDO!</a></td>

                                <?php
                                $var = null;
                            endif;
                            ?>
                            </tr>

                            <!-- MODAL REMOVE-->
                            <div id="modalRemove<?php echo $val[0]; ?>" class="modal">
                                <div class="modal-content">
                                    <center><img src="../imagens/cancelar.png" style="height: 100px;"/></center><br>
                                    <h4 class="center blue-grey-text">Você tem certeza disso?</h4>
                                    <p class="center blue-grey-text">O livro  "<?php echo $val[1]; ?>" será removido da sua lista de reservas. </p>
                                    <center class='white-text'>
                                        <form method="POST" action="../controller/LivroController.php">
                                            <input type="hidden" name="removerLivroReservado" value="removerLivroReservado"/>
                                            <input type="hidden" name="idLivro" value="<?php echo $val[0]; ?>"/>
                                            <button class="modal-close waves-effect waves-light btn-small blue" type="submit" name="action">Confirmar</button>
                                        </form>
                                        <a class="modal-close waves-effect waves-light btn-small red">Cancelar</a>
                                    </center>
                                </div>
                            </div>

                            <!-- MODAL EXCLAMACAO-->
                            <div id="modalExclamacao<?php echo $val[0]; ?>" class="modal">
                                <div class="modal-content">
                                    <center><img src="../imagens/Ponto de exclamacao.jpg" style="height: 100px;"/></center><br>
                                    <h4 class="center blue-grey-text">Você deverá pegar o livro emprestado ou cancelar a reserva.</h4>
                                    <p class="center blue-grey-text">Se você não se mexer, vamos tirá-lo daqui! </p>
                                </div>
                            </div>

                            <!-- MODAL VISIBILITY -->
                            <div id="modalVisibility<?php echo $val[0]; ?>" class="modal">
                                <div class="modal-content">
                                    <div class="row">
                                        <div class="col s6" style="border: 1px solid grey">
                                            <h4 class="center"><?php echo $val[1]; ?></h4>
                                            <?php echo "<b>Autor: </b>" . $val[2]; ?> <br>
                                            <?php echo "<b>Edição: </b>" . $val[3]; ?> <br>
                                            <?php echo "<b>Editora: </b>" . $val[4]; ?> <br>
                                            <?php echo "<b>Número de páginas: </b>" . $val[5]; ?> <br>
                                            <?php
                                            echo "<b>Categorias: </b>";
                                            $categorias = CategoriaController::mostrarCategoriasPorLivro($val[0]);
                                            if (!empty($categorias)):
                                                foreach ($categorias as $valorr):
                                                    echo $valorr[1] . "; ";
                                                endforeach;
                                            endif;
                                            ?><br>
                                            <?php echo "<b>Disponibilidade: </b>" . $val[6]; ?> <br>
                                            <?php
                                            echo "<b>Estado: </b>";
                                            if ($val[10] == 0) {
                                                echo 'Disponível';
                                            }
                                            if ($val[10] == 1) {
                                                echo 'Indisponível';
                                            }
                                            if ($val[10] == 2) {
                                                echo 'Reservado';
                                            }
                                            ?> <br>
                                            <hr>
                                            <?PHP $dono = UsuarioController::buscarUsuariosPorEmail($val[9]) ?>
                                            <?php echo "<b>Pertence à: </b>" . $dono[1]; ?> <br>
                                            <?php echo "<b>Telefone: </b>" . $dono[4]; ?> <br>
                                            <?php echo "<b>Email: </b>" . $dono[2]; ?> <br>

                                            <?php
                                            $historico = LivroController::mostrarHistoricoPorLivro($val[0]);
                                            if (!empty($historico)) {
                                                echo "<hr><b><center>Histórico do livro</center> </b>";
                                                foreach ($historico as $v) {
                                                    if (!empty($v[2])) {
                                                        $usuarioHistorico = UsuarioController::mostrarUsuariosPorId($v[1]);
                                                        echo "<b>Utilizado por: </b>" . $usuarioHistorico[1] . "<br>";
                                                        echo "<b>Email: </b>" . $usuarioHistorico[2] . "<br>";
                                                        echo "<b>Data inicial: </b>" . $v[3] . "<br>";
                                                        echo "<b>Data da entrega: </b>" . $v[2] . "<br>";
                                                        echo "<b>Data limite da entrega: </b>" . $v[4] . "<br><br>";
                                                    }
                                                }
                                            }
                                            ?>
                                            <hr>
                                            <b>Você reservou o livro em: </b> <?= $val[12]; ?><br>
                                            <b>Estará reservado até: </b> <?= $val[13]; ?><br>
                                        </div>
                                        <div class="col s4 right">
                                            <?php if (isset($val[11]) && !empty($val[11])) {
                                                ?>
                                                <h4 class="center">Capa do livro</h4>
                                                <img src="../upload/<?php echo $val[11]; ?>" class="responsive-img" style="height: 250px; border: 1px solid grey"/>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="modal-content">
                                                    <h4 class="center blue-grey-text">Não encontramos nenhuma imagem da capa deste livro.</h4>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <a href="" class="modal-close waves-effect waves-green btn-flat">Fechar</a>
                                </div>
                            </div>


                            <?php
                        endforeach;
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php
        else:
            echo "<br><br><h1 class='center blue-grey-text'>Nenhum resultado foi encontrado.</h1><br><br>";
            echo "<h3 class='center blue-grey-text'>Você não reservou nenhum livro.</h3><br><br>";
        endif;
        if (!empty($_SESSION['sweet']) && isset($_SESSION['sweet'])) {
            if ($_SESSION['sweet'] == "Livro removido da lista de reservas.") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'O livro está disponível agora!', 'success');</script>";
                $_SESSION['sweet'] = null;
            }
        }
        ?>

    </body>
</html>
