<?php
if (!session_id()) {
    session_start();
}

if (!isset($_SESSION["usuario"])) {
    header("location:../controller/sessionDestroy.php");
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <script type="text/javascript" src="../static/main.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Livros Para Entregar</title>
    </head>
    <body class="grey lighten-5">
        <nav>
            <div class="nav-wrapper teal">
                <div class="col s12">
                    <a href="telaPrincipal.php" class="breadcrumb">Menu principal</a>
                    <a href="" class="breadcrumb">Livros para entregar</a>
                </div>
            </div>
        </nav>

        <?php
        require_once '../controller/LivroController.php';
        require_once "../controller/CategoriaController.php";

        $array = LivroController::mostrarEmprestimosPorId($_SESSION['usuario'][0]);
        if (!empty($array)):
            ?>
            <h3 class="center blue-grey-text">Livros em empréstimo.</h3><br>
            <div class="row">
                <div class="col s10 offset-s1">

                    <table class="centered responsive-table">
                        <thead>
                            <tr>
                                <th>Título</th>
                                <th>Autor</th>
                                <th>Data Inicial</th>
                                <th>Data a ser entregue</th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            foreach ($array as $val):
                                ?>
                                <tr>
                                    <td><?php echo $val[4]; ?></td>
                                    <td><?php echo $val[5]; ?></td>
                                    <td><?php echo $val[2]; ?></td>
                                    <td><?php echo $val[3]; ?></td>
                                    <td><a class="modal-trigger" href="#modalVisibility<?php echo $val[1]; ?>"><i class="material-icons prefix blue-text">visibility</i></a></td>
                                </tr>

                                <?php $value = LivroController::mostrarLivrosPorId($val[1]); ?>


                                <!-- MODAL VISIBILITY -->
                            <div id="modalVisibility<?php echo $val[1]; ?>" class="modal">
                                <div class="modal-content">
                                    <div class="row">
                                        <div class="col s6" style="border: 1px solid grey">
                                            <h4 class="center"><?php echo $value[1]; ?></h4>
                                            <?php echo "<b>Autor: </b>" . $value[2]; ?> <br>
                                            <?php echo "<b>Edição: </b>" . $value[3]; ?> <br>
                                            <?php echo "<b>Editora: </b>" . $value[4]; ?> <br>
                                            <?php echo "<b>Número de páginas: </b>" . $value[5]; ?> <br>

                                            <?php
                                            echo "<b>Categorias: </b>";
                                            $categorias = CategoriaController::mostrarCategoriasPorLivro($val[1]);
                                            if (!empty($categorias)):
                                                foreach ($categorias as $valorr):
                                                    echo $valorr[1] . "; ";
                                                endforeach;
                                            endif;
                                            ?><br>
                                            <?php echo "<b>Disponibilidade: </b>" . $value[6]; ?> <br>
                                            <?php
                                            echo "<b>Estado: </b>";
                                            if ($value[10] == 0) {
                                                echo 'Disponível';
                                            }
                                            if ($value[10] == 1) {
                                                echo 'Indisponível';
                                            }
                                            if ($value[10] == 2) {
                                                echo 'Reservado';
                                            }
                                            ?> <br>
                                            <hr>
                                            <?PHP $dono = UsuarioController::buscarUsuariosPorEmail($value[9]) ?>
                                            <?php echo "<b>Pertence à: </b>" . $dono[1]; ?> <br>
                                            <?php echo "<b>Telefone: </b>" . $dono[4]; ?> <br>
                                            <?php echo "<b>Email: </b>" . $dono[2]; ?> <br>

                                            <?php
                                            $historico = LivroController::mostrarHistoricoPorLivro($value[0]);
                                            if (!empty($historico)) {
                                                echo "<hr><b><center>Histórico do livro</center> </b>";
                                                foreach ($historico as $v) {
                                                    if (!empty($v[2])) {
                                                        $usuarioHistorico = UsuarioController::mostrarUsuariosPorId($v[1]);
                                                        echo "<b>Utilizado por: </b>" . $usuarioHistorico[1] . "<br>";
                                                        echo "<b>Email: </b>" . $usuarioHistorico[2] . "<br>";
                                                        echo "<b>Data inicial: </b>" . $v[3] . "<br>";
                                                        echo "<b>Data da entrega: </b>" . $v[2] . "<br>";
                                                        echo "<b>Data limite da entrega: </b>" . $v[4] . "<br><br>";
                                                    }
                                                }
                                            }
                                            ?>
                                        </div>
                                        <div class="col s4 right">
                                            <?php if (isset($value[11]) && !empty($value[11])) {
                                                ?>
                                                <h4 class="center">Capa do livro</h4>
                                                <img src="../upload/<?php echo $value[11]; ?>" class="responsive-img" style="height: 250px; border: 1px solid grey"/>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="modal-content">
                                                    <h4 class="center blue-grey-text">Não encontramos nenhuma imagem da capa deste livro.</h4>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <a href="" class="modal-close waves-effect waves-green btn-flat">Fechar</a>
                                </div>
                            </div>
                            <?php
                        endforeach;
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php
        else:
            echo "<br><br><h1 class='center blue-grey-text'>Nenhum resultado foi encontrado.</h1><br><br>";
            echo "<h3 class='center blue-grey-text'>Você não está com nenhum livro emprestado.</h3><br><br>";
        endif;
        ?>

    </body>
</html>
