<?php
if (!session_id()) {
    session_start();
}

if (!isset($_SESSION["admin"])) {
    header("location:../controller/sessionDestroy.php");
}
require_once '../controller/LivroController.php';
require_once '../controller/UsuarioController.php';
$array = UsuarioController::mostrarTodosAdmins();
?>



<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <script type="text/javascript" src="../static/main.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7"></script>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Gerenciamento de Administradores</title>
    </head>

    <script>
        $(document).ready(function () {
            $('.modal').modal();
            $('.sidenav').sidenav();
            var botao;
        });
    </script>
    <body class="grey lighten-5">
        <nav>
            <div class="nav-wrapper teal">
                <div class="col s12">
                    <a href="telaTodosLivros.php" class="breadcrumb">Menu Principal</a>
                    <a href="" class="breadcrumb">Gerenciar Administradores</a>
                </div>
            </div>
        </nav>
        <div>
            <div style="margin: 10px;" class="right"><br>
                <a id="cadastrar" class="btn-large waves-effect waves-light blue">Novo administrador</a>
            </div>
        </div>
        <br><h3 class="center blue-grey-text">Administradores atuais</h3><br><br>

        <div class="row">
            <div class="col s10 offset-s1" id="table">
                <?php
                if (!empty($array)):
                    ?>
                    <table class="centered responsive-table">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Email</th>
                                <th>Telefone</th>
                                <th>Celular</th>
                                <th>Remover papel</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            foreach ($array as $value):
                                ?>
                                <tr>
                                    <td><?php echo $value[1]; ?></td>
                                    <td><?php echo $value[2]; ?></td>
                                    <td><?php echo $value[4]; ?></td>
                                    <td><?php echo $value[5]; ?></td>
                                    <td><a class="modal-trigger" href="#modalRemove<?php echo $value[0]; ?>"><i class="material-icons prefix red-text">delete</i></a></td>
                                </tr>

                                <!-- MODAL REMOVE-->
                            <div id="modalRemove<?php echo $value[0]; ?>" class="modal">
                                <div class="modal-content">
                                    <center><img src="../imagens/cancelar.png" style="height: 100px;"/></center><br>
                                    <h4 class="center blue-grey-text">Você tem certeza disso?</h4>
                                    <p class="center blue-grey-text">O usuario selecionado não será mais administrador deste sistema. </p>
                                    <center class='white-text'>
                                        <form method="POST" action="../controller/UsuarioController.php">
                                            <input type="hidden" name="cancelarPapel" value="cancelarPapel"/>
                                            <input type="hidden" name="idUsuario" value="<?php echo $value[0]; ?>"/>
                                            <button class="modal-close waves-effect waves-light btn-small blue" type="submit" name="action">Confirmar</button>
                                        </form>
                                        <a class="modal-close waves-effect waves-light btn-small red">Cancelar</a>
                                    </center>
                                </div>
                            </div>

                            <?php
                        endforeach;
                        ?>
                        </tbody>
                    </table>
                    <?php
                endif;
                ?>
            </div>
        </div>

        <script>
            $("#cadastrar").click(function () {
                swal({
                title: 'Selecione um usuário para se tornar administrador',
                        input: 'select',
                        inputOptions: {
<?php
if (!empty(UsuarioController::mostrarTodosUsuarios())) {
    foreach (UsuarioController::mostrarTodosUsuarios() as $val) {
        if ($val["14"] == 0) {
            echo "'$val[0]' : '$val[1]', ";
        }
    }
}
?>
                        },
                        inputPlaceholder: 'Clique aqui',
                        showCancelButton: true,
                }).then((value) => {
                botao = value;
                });
                        $(".swal2-confirm").click(function () {

                    if (botao != "") {
                        $.ajax({
                            method: "POST",
                            url: "../controller/UsuarioController.php",
                            data:
                                    {
                                        tornarAdmin: "tornar Admin",
                                        id: botao.value,
                                    }
                        }).done(function (value) {
                            swal("O usuário selecionado tornou-se administrador").then(function () {
                                location.reload();
                            });
                        }).fail(function (value) {
                            alert("erro: " + value)
                        });
                    }
                });
            });
        </script>

        <?php
        if (!empty($_SESSION['sweet']) && isset($_SESSION['sweet'])):
            if ($_SESSION['sweet'] == "Usuário Removido.") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'O usuário foi removido com sucesso. Agora ele não faz mais parte do grupo de administradores deste sistema ', 'success');</script>";
                $_SESSION['sweet'] = null;
            }
        endif;
        ?>
    </body>
</html>
