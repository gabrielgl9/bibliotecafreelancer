<?php

if (!session_id()) {
    session_start();
}

if (!isset($_SESSION["usuario"])) {
    header("location:../controller/sessionDestroy.php");
}


require_once '../controller/UsuarioController.php';
$array = UsuarioController::mostrarUsuariosPorId($_SESSION["usuario"][0]);
?>
<html>
    <head>
        <meta charset="UTF-8">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.0/jquery.mask.js"></script>
        <script type="text/javascript" src="../static/webService/webService.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Editar Perfil</title>
    </head>
    <body class="grey lighten-5">
        <script>
            $(document).ready(function () {
                $('.mascara-telefone').mask('(00)00000-0000', {reverse: false});
                $('.mascara-cep').mask('00000-000', {reverse: false});
                $('.mascara-cpf').mask('000.000.000-00', {reverse: false});
            });
        </script>
        <nav>
            <div class="nav-wrapper teal">
                <div class="col s12">
                    <a href="telaPrincipal.php" class="breadcrumb">Menu principal</a>
                    <a href="telaMeuPerfil.php" class="breadcrumb">Meu Perfil</a>
                    <a href="" class="breadcrumb">Editar Perfil</a>

                </div>
            </div>
        </nav>
        <h3 class="center blue-grey-text">Editar Perfil</h3><br>
        <div class="row">
            <div class="col s6 m6 l6 offset-s3 offset-m3 offset-l3 card-panel">

                <form class="col s12" method="post" action="../controller/UsuarioController.php">
                    <input type="hidden" name="editarPerfil" value="editarPerfil"/>
                    <input type="hidden" name="idUsuario" value="<?= $array[0]; ?>">
                    <div class="row">
                        <div class="input-field col s6">
                            <i class="material-icons prefix">account_circle</i>
                            <input id="nomeCompleto" type="text" name="nomeCompleto" value="<?= $array[1]; ?>" class="validate" required maxlength="35">
                            <label for="nomeCompleto">Nome completo</label>
                        </div>
                        <div class="input-field col s6">
                            <i class="material-icons prefix">settings_applications</i>
                            <input id="cpf" type="tel" class="mascara-cpf" name="cpf" value="<?= $array[6]; ?>" class="validate" required maxlength="14">
                            <label for="cpf">CPF</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <i class="material-icons prefix">phone_android</i>
                            <input id="celular" type="text" class="mascara-telefone" name="celular" value="<?= $array[5]; ?>" class="validate" maxlength="35">
                            <label for="celular">Celular</label>
                        </div>
                        <div class="input-field col s6">
                            <i class="material-icons prefix">phone</i>
                            <input id="telefone" type="tel" class="mascara-telefone" name="telefone" value="<?= $array[4]; ?>" class="validate" maxlength="14">
                            <label for="telefone">Telefone</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <i class="material-icons prefix">home</i>
                            <input id="cep" type="text" name="cep" class="mascara-cep" value="<?= $array[7]; ?>" class="validate" required maxlength="9">
                            <label for="cep">Cep</label>
                        </div>
                    </div>

                    <div style="margin-top: -80px;">
                        <div class="row">
                            <div class="input-field col s6">
                                <i class="material-icons prefix" style="margin-top:8%;">public</i>
                                <label for="uf">Estado
                                    <input id="uf" type="text" name="uf" value="<?= $array[8]; ?>" class="validate">
                                </label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix" style="margin-top:8%;">location_city</i>

                                <label>Cidade
                                    <input id="cidade" type="text" value="<?= $array[9]; ?>" name="cidade" class="validate">
                                </label>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="input-field col s12">
                                <i class="material-icons prefix" style="margin-top:4%;">domain</i>
                                <label>Bairro
                                    <input id="bairro" type="text" value="<?= $array[10]; ?>" name="bairro" class="validate">
                                </label>
                            </div>
                        </div><br>

                        <div class="row">
                            <div class="input-field col s12">
                                <!--domain -->
                                <i class="material-icons prefix" style="margin-top:4%;">streetview</i>
                                <label>Rua
                                    <input name="rua" type="text" value="<?= $array[11]; ?>" id="rua" />
                                </label>
                            </div>
                        </div>
                    </div><br><br><br>
                    <div class="row margin-top: 10%;">
                        <div class="input-field col s6">
                            <i class="material-icons prefix">add_location</i>
                            <input id="numeroResidencia" type="number" name="numeroResidencia" value="<?= $array[12]; ?>" class="validate" required maxlength="10">
                            <label for="numeroResidencia">Número de residência</label>
                        </div>
                        <div class="input-field col s6">
                            <i class="material-icons prefix">settings_applications</i>
                            <input id="complemento" type="text" name="complemento" class="validate" value="<?= $array[13]; ?>" maxlength="8">
                            <label for="complemento">Complemento</label>
                        </div>
                    </div>
                    <button class="btn waves-effect waves-light col s12" type="submit" name="action">Editar Usuario
                    </button>
                </form>
            </div>
        </div>

        <?php
        if (!empty($_SESSION['sweet']) && isset($_SESSION['sweet'])):
            echo "<script> swal('" . $_SESSION['sweet'] . "', 'Falha ao editar', 'error');</script>";
            $_SESSION['sweet'] = null;
        endif;
        ?>



    </body>
</html>
