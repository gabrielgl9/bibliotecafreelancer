<?php
if (!session_id()) {
    session_start();
}

if (!isset($_SESSION["admin"])) {
    header("location:../controller/sessionDestroy.php");
}
require_once '../controller/LivroController.php';
require_once '../controller/UsuarioController.php';
$todosEmprestimos = LivroController::mostrarEmprestimos();
?>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <script type="text/javascript" src="../static/main.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Admin</title>
    </head>
    <script>
        $(document).ready(function () {
            $('.modal').modal();
            $('.sidenav').sidenav();
            $("input.autocomplete").autocomplete({
                data: {
<?php
if (!empty($todosEmprestimos)):
    foreach ($todosEmprestimos as $valor):
        $dadosLivro = LivroController::mostrarLivrosPorId($valor[1]);
        echo "'$dadosLivro[1]' : null,";
    endforeach;
endif;
?>
                },
                limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
                minLength: 1 // The minimum length of the input for the autocomplete to start. Default: 1.
            });


            $("#botao").click(function () {
                $.get("../controller/recebeAutoComplete.php", {"valueEmp": $("#pesquisa").val()}, function (data) {
                    $("#table").html(data);
                });
            });
        });
    </script>
    <body class="grey lighten-5">
        <nav>
            <div class="nav-wrapper teal">
                <div class="col s12">
                    <a href="telaTodosLivros.php" class="breadcrumb">Menu Principal</a>
                    <a href="" class="breadcrumb">Livros em empréstimo</a>
                </div>
            </div>
        </nav>


        <?php
        if (isset($_GET["pagina"])) {
            $pagina = $_GET["pagina"];
        } else {
            $pagina = 1;
        }
        ?>

        <br><h3 class="center blue-grey-text">Livros em empréstimo</h3><br><br>

        <div class="row">
            <!-- Pesquisar livros -->
            <div class="col s10">
                <form method="POST" action="../controller/LivroController.php">
                    <input type="hidden" name="pesquisarEmprestimos" value="pesquisarEmprestimos" >
                    <div class="input-field col s5 m4 l3">
                        <input type="text" autocomplete="off" name="pesquisa" style="border: 1px solid" placeholder="Digite o nome do livro" id="autocomplete-input" class="autocomplete black-text"/>
                    </div>
                    <div class="input-field col s2">
                        <button class="btn waves-effect waves-light blue darken-4 btn-large" style="margin-left: -17px;" type="submit" name="action">Pesquisar</button>
                    </div>
                </form>
            </div>
        </div>


        <div class="row">
            <div class="col s10 offset-s1">

                <?php
                if (isset($_GET["pesquisa"])) {
                    $array = LivroController::pesquisarEmprestimosPorPagination($_GET["pesquisa"]);
                } else {
                    $array = LivroController::mostrarEmprestimosPorPagination($pagina);
                }
                //$array = LivroController::mostrarEmprestimos();
                if (!empty($array)):
                    ?>
                    <table class="centered responsive-table">
                        <thead>
                            <tr>
                                <th>Título do livro</th>
                                <th>Usuário</th>
                                <th>Data inicial</th>
                                <th>Data a ser entregue</th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            foreach ($array as $value):
                                $dadosUsuario = UsuarioController::mostrarUsuariosPorId($value[0]);
                                $dadosLivro = LivroController::mostrarLivrosPorId($value[1]);
                                ?>
                                <tr>
                                    <td><?php echo $dadosLivro[1]; ?></td>
                                    <td><?php echo $dadosUsuario[1]; ?></td>
                                    <td><?php echo $value[2]; ?></td>
                                    <td><?php echo $value[3]; ?></td>
                                    <td><a class="modal-trigger" href="#modalVisibility<?php echo $value[1]; ?>"><i class="material-icons prefix blue-text">visibility</i></a></td>
                                </tr>

                                <!-- MODAL VISIBILITY -->
                            <div id="modalVisibility<?php echo $value[1]; ?>" class="modal">
                                <div class="modal-content">
                                    <?php echo "<h4 class='center blue-grey-text'>" . $dadosLivro[1] . "</h4>"; ?> <br>
                                    <?php $val = UsuarioController::mostrarUsuariosPorId($value[0]); ?>
                                    <?php echo "<b>Está sendo usado por: </b>" . $val[1]; ?> <br>
                                    <?php echo "<b>Email: </b>" . $val[2]; ?> <br>
                                    <?php echo "<b>Telefone: </b>" . $val[4]; ?> <br>
                                    <?php echo "<b>Celular: </b>" . $val[5]; ?> <br>

                                </div>
                                <div class="modal-footer">
                                    <a href="" class="modal-close waves-effect waves-green btn-flat">Fechar</a>
                                </div>
                            </div>

                            <?php
                        endforeach;
                        ?>
                        </tbody>
                    </table>
                    <?php
                endif;
                ?>
            </div>
        </div>

        <div class="row center">

            <?php
            if (isset($array)) {
                if (isset($_GET["pesquisa"])) {

                    if ($_SESSION["countPesquisa"] > 0) {
                        $numeroPagina = floor(($_SESSION["countPesquisa"] - 0.1) / 5);
                    } else {
                        $numeroPagina = 1;
                    }
                    ?>
                    <ul class="pagination">
                        <?php if ($_GET["pesquisa"] == 1) { ?>
                            <li class="disabled"><i class="material-icons">chevron_left</i></li>
                        <?php } else { ?>
                            <li class="waves-effect"><a href="telaGerenciarEmprestimos.php?pesquisa=<?= $_GET["pesquisa"] - 1; ?>"><i class="material-icons">chevron_left</i></a></li>
                        <?php } ?>

                        <?php
                        for ($x = 0; $x <= $numeroPagina; $x++) {
                            if ($_GET["pesquisa"] == $x + 1) {
                                ?>
                                <li class="active"><a href="#!"><?= $x + 1; ?></a></li>
                            <?php } else { ?>
                                <li class="waves-effect"><a href="telaGerenciarEmprestimos.php?pesquisa=<?= $x + 1; ?>"><?= $x + 1; ?></a></li>
                                <?php
                            }
                        }
                        if ($numeroPagina + 1 == $_GET["pesquisa"]) {
                            ?>
                            <li class="disabled"><i class="material-icons">chevron_right</i></li>
                        <?php } else { ?>
                            <li class="waves-effect"><a href="telaGerenciarEmprestimos.php?pesquisa=<?= $_GET["pesquisa"] + 1; ?>"><i class="material-icons">chevron_right</i></a></li>
                        <?php } ?>
                    </ul>
                    <?php
                } else {


                    //verifica numero de paginas. Se não houver categorias, não posso fazer 0/10, pois haverá uma página;
                    if (count($todosEmprestimos) > 0) {
                        $numeroPagina = floor((count($todosEmprestimos) - 0.1) / 5);
                    } else {
                        $numeroPagina = 1;
                    }
                    ?>

                    <ul class="pagination">
                        <?php if ($pagina == 1) { ?>
                            <li class="disabled"><i class="material-icons">chevron_left</i></li>
                        <?php } else { ?>
                            <li class="waves-effect"><a href="telaGerenciarEmprestimos.php?pagina=<?= $pagina - 1; ?>"><i class="material-icons">chevron_left</i></a></li>
                        <?php } ?>

                        <?php
                        for ($x = 0; $x <= $numeroPagina; $x++) {
                            if ($pagina == $x + 1) {
                                ?>
                                <li class="active"><a href="#!"><?= $x + 1; ?></a></li>
                            <?php } else { ?>
                                <li class="waves-effect"><a href="telaGerenciarEmprestimos.php?pagina=<?= $x + 1; ?>"><?= $x + 1; ?></a></li>
                                <?php
                            }
                        }
                        if ($numeroPagina + 1 == $pagina) {
                            ?>
                            <li class="disabled"><i class="material-icons">chevron_right</i></li>
                        <?php } else { ?>
                            <li class="waves-effect"><a href="telaGerenciarEmprestimos.php?pagina=<?= $pagina + 1; ?>"><i class="material-icons">chevron_right</i></a></li>
                            <?php } ?>
                    </ul>

                    <?php
                }
            }
            ?>
        </div>


    </body>
</html>
