<?php
if (!session_id()) {
    session_start();
}

if (!isset($_SESSION["usuario"])) {
    header("location:../controller/sessionDestroy.php");
}

require_once '../controller/LivroController.php';
require_once '../controller/UsuarioController.php';
require_once "../controller/CategoriaController.php";
$array = UsuarioController::mostrarUsuariosPorId($_SESSION["usuario"][0]);
$seusLivros = LivroController::mostrarLivrosPorPessoa($array[1], $array[2]);
?>

<html>
    <head>
        <meta charset="UTF-8">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <script type="text/javascript" src="../static/main.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Seus Livros</title>
    </head>

    <script>
        $(document).ready(function () {
            $('.modal').modal();
            $('.sidenav').sidenav();

            $("input.autocomplete").autocomplete({
                data: {
<?php
if (!empty($seusLivros)):
    foreach ($seusLivros as $valor):
        echo "'$valor[1]' : null,";
    endforeach;
endif;
?>
                },
                limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
                minLength: 1 // The minimum length of the input for the autocomplete to start. Default: 1.
            });

        });
    </script>

    <body class="grey lighten-5">
        <nav>
            <div class="nav-wrapper teal">
                <div class="col s12">
                    <a href="telaPrincipal.php" class="breadcrumb">Menu principal</a>
                    <a href="" class="breadcrumb">Visualizar seus livros</a>
                </div>
            </div>
        </nav>

        <?php
        if (isset($_GET["pagina"])) {
            $pagina = $_GET["pagina"];
        } else {
            $pagina = 1;
        }
        ?>
        <h3 class="center blue-grey-text">Livros disponibilizados</h3><br>

        <div class="row">
            <!-- Pesquisar livros -->
            <div class="col s10">
                <form method="POST" action="../controller/LivroController.php">
                    <input type="hidden" name="pesquisarSeusLivros" value="pesquisarSeusLivros">
                    <div class="input-field col s5 m4 l3">
                        <input type="hidden" name="email" value="<?= $array[2]; ?>" />
                        <input type="text" autocomplete="off" name="pesquisa" style="border: 1px solid" placeholder="Digite o nome do livro" id="autocomplete-input" class="autocomplete black-text"/>
                    </div>
                    <div class="input-field col s2">
                        <button class="btn waves-effect waves-light blue darken-4 BTN-LARGE" style="margin-left: -17px;" type="submit" name="action">Pesquisar</button>
                    </div>
                </form>
            </div>
        </div>

        <?php
        if (isset($_GET["pesquisa"])) {
            $arrayPagination = LivroController::pesquisarSeusLivrosPorPagination($_GET["pesquisa"], $array[2]);
        } else {
            $arrayPagination = LivroController::mostrarSeusLivrosPorPagination($pagina, $array[2]);
        }
        if (!empty($arrayPagination)):
            ?>

            <div class="row">
                <div class="col s10 offset-s1">

                    <table class="centered responsive-table">
                        <thead>
                            <tr>
                                <th>Título</th>
                                <th>Autor</th>
                                <th>Estado</th>
                                <th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            foreach ($arrayPagination as $livro):
                                ?>
                                <tr>
                                    <td><?php echo $livro[1]; ?></td>
                                    <td><?php echo $livro[2]; ?></td>
                                    <td>
                                        <?php
                                        if ($livro[10] == 0):
                                            echo "Disponível";
                                        endif;
                                        if ($livro[10] == 1):
                                            echo "Emprestado";
                                            $emprestimo = true;
                                        endif;
                                        if ($livro[10] == 2):
                                            echo "<a href='#modalReserva" . $livro[0] . "' class='btn orange modal-trigger'>Reservado</a>";
                                        endif;
                                        ?>
                                    </td>
                                    <td><a class="modal-trigger" href="#modalVisibility<?php echo $livro[0]; ?>"><i class="material-icons prefix blue-text">visibility</i></a></td>
                                    <td>
                                        <form method="POST" action="telaEditarLivro.php">
                                            <input type="hidden" name="idLivro" value="<?php echo $livro[0]; ?>"/>
                                            <a class="btn-flat" style="margin-top: 10%;" onclick='this.parentNode.submit();
                                                            return false;' ><i class="material-icons prefix green-text">edit</i></a>
                                        </form>
                                    </td>
                                    <td><a class="modal-trigger" href="#modalRemove<?php echo $livro[0]; ?>"><i class="material-icons prefix red-text">delete</i></a></td>
                                    <?php if (isset($emprestimo) and $emprestimo):
                                        ?>
                                        <td><a class="modal-trigger waves-effect waves-light btn green" href="#modalEntregue<?php echo $livro[0]; ?>">Entregue</a></td>
                                        <?php
                                    else:
                                        echo '<td></td>';
                                    endif;
                                    ?>
                                </tr>

                                <!-- MODAL PARA ENTREGA DE LIVROS -->
                            <div id="modalEntregue<?php echo $livro[0]; ?>" class="modal">
                                <div class="modal-content">
                                    <center><img src="../imagens/success.png" style="height: 200px;"/></center><br>
                                    <h3 class="blue-grey-text center">Livro entregue</h3>
                                    <p class="center-align" style="font-size: 24px;">O livro <?php echo $livro[1]; ?> está sendo entregue
                                    <div class="center">
                                        <form method="POST" action="../controller/LivroController.php">
                                            <input type="hidden" name="entregarLivro" value="entregarLivro"/>
                                            <input type="hidden" name="idLivro" value="<?= $livro[0]; ?>">
                                            <button class="modal-close waves-effect waves-light btn-small blue" type="submit" name="action">Confirmar</button>
                                        </form>
                                        <a class="modal-close waves-effect waves-light btn-small red">Cancelar</a>
                                    </div>
                                    </p>
                                </div>
                            </div>

                            <!-- MODAL DELETE -->
                            <div id="modalRemove<?php echo $livro[0]; ?>" class="modal">
                                <div class="modal-content">
                                    <center><img src="../imagens/Ponto de exclamacao.jpg" style="height: 100px;"/></center><br>
                                    <h4 class="center blue-grey-text">Você tem certeza disso?</h4>
                                    <p class="center blue-grey-text">O livro  "<?php echo $livro[1]; ?>" será removido e isso não poderá ser revertido. </p>
                                    <center class='white-text'>
                                        <form method="POST" action="../controller/LivroController.php">
                                            <input type="hidden" name="deletarLivro" value="deletarLivro"/>
                                            <input type="hidden" name="idLivro" value="<?php echo $livro[0]; ?>"/>
                                            <button class="modal-close waves-effect waves-light btn-small blue" type="submit" name="action">Confirmar</button>
                                        </form>
                                        <a class="modal-close waves-effect waves-light btn-small red">Cancelar</a>
                                    </center>
                                </div>
                            </div>

                            <!-- MODAL RESERVADO -->
                            <div id="modalReserva<?php echo $livro[0]; ?>" class="modal">
                                <div class="modal-content">
                                    <center><img src="../imagens/ExclamacaoPreto.png" style="height: 100px;"/></center><br>
                                    <h4 class="center blue-grey-text">Pedido de empréstimo!</h4>
                                    <p class="blue-grey-text" style="font-size: 25px;">
                                        <?php
                                        $usuarioPedinte = LivroController::mostrarReservaDoLivro($livro[0]);
                                        echo "<br> <B> Nome:</B> " . $usuarioPedinte[1];
                                        echo "<br> <B> Email:</B> " . $usuarioPedinte[2];
                                        echo "<br> <B> Telefone:</B> " . $usuarioPedinte[3];
                                        echo "<br> <B> Celular:</B> " . $usuarioPedinte[4];
                                        ?>
                                    </p>
                                    <center class="blue-grey-text" style="font-size: 25px;">Você aceita emprestar o livro <?= $livro[1]; ?> para essa pessoa?</center><br>
                                    <center class='white-text'>
                                        <div class="row">
                                            <div class="col s4" >
                                                <form method="POST" action="../controller/LivroController.php">
                                                    <input type="hidden" name="emprestarLivro" value="emprestarLivro"/>
                                                    <input type="hidden" name="idLivro" value="<?php echo $livro[0]; ?>"/>
                                                    <input type="hidden" name="idUsuario" value="<?= $usuarioPedinte[0]; ?>"/>
                                                    <button class="modal-close waves-effect waves-light btn-small blue" type="submit" name="action" style="width: 110px; height: 50px;">Confirmar</button>
                                                </form>
                                            </div>
                                            <div class="col s4">
                                                <form method="POST" action="../controller/LivroController.php">
                                                    <input type="hidden" name="removerLivroReservado" value="removerLivroReservado"/>
                                                    <input type="hidden" name="idLivro" value="<?php echo $livro[0]; ?>"/>
                                                    <input type="hidden" name="idUsuario" value="<?= $usuarioPedinte[0]; ?>"/>
                                                    <button class="modal-close waves-effect waves-light btn-small red" type="submit" name="action" style="width: 110px; height: 50px;">Recusar</button>
                                                </form>
                                            </div>
                                            <div class="col s4" style="padding-left: 60px;">
                                                <button class="modal-close waves-effect waves-light btn-small green" style="width: 110px; height: 50px; margin-top: 5%;">Cancelar</button>
                                            </div>
                                        </div>
                                    </center>
                                </div>
                            </div>


                            <!-- Modal Visibility-->
                            <div id="modalVisibility<?php echo $livro[0]; ?>" class="modal">
                                <div class="modal-content">
                                    <div class="row">
                                        <div class="col s6" style="border: 1px solid grey">
                                            <h4 class="center"><?php echo $livro[1]; ?></h4>
                                            <?php
                                            echo "<b>Autor: </b>" . $livro[2];
                                            echo "<br><b>Edição: </b>" . $livro[3];
                                            echo "<br><b>Editora: </b>" . $livro[4];
                                            echo "<br><b>Número de páginas: </b>" . $livro[5];
                                            echo "<br><b>Categorias: </b>";
                                            $categorias = CategoriaController::mostrarCategoriasPorLivro($livro[0]);
                                            if (!empty($categorias)):
                                                foreach ($categorias as $valorr):
                                                    echo $valorr[1] . "; ";
                                                endforeach;
                                            endif;
                                            echo "<br><b>Disponibilidade: </b>" . $livro[6];

                                            echo "<br><b>Estado: </b>";
                                            if ($livro[10] == 0) {
                                                echo 'Disponível';
                                            }
                                            if ($livro[10] == 1) {
                                                echo 'Emprestado';
                                            }
                                            if ($livro[10] == 2) {
                                                echo 'Reservado';
                                            }
                                            if (isset($emprestimo) && $emprestimo):
                                                $emprestimo = false;
                                                $valorSession = UsuarioController::mostrarUsuariosPorId($_SESSION["usuario"][0]);
                                                $array = LivroController::mostrarLivrosEmprestados($valorSession[1], $valorSession[2]);
                                                foreach ($array as $value) :
                                                    if ($value[1] == $livro[0]):
                                                        $dadosUsuario = UsuarioController::mostrarUsuariosPorId($value[0]);
                                                        echo "<br><hr><b>Está com: </b>" . $dadosUsuario[1];
                                                        echo "<br><b>Telefone: </b>" . $dadosUsuario[4];
                                                        echo "<br><b>Email: </b>" . $dadosUsuario[2];
                                                        echo "<br><b>Ele pegou seu livro em: </b> " . $value[2];
                                                        echo "<br><b>Ele deverá entregar seu livro em: </b> " . $value[3];
                                                    endif;
                                                endforeach;
                                            endif;
                                            ?>

                                            <?php
                                            $historico = LivroController::mostrarHistoricoPorLivro($livro[0]);
                                            if (!empty($historico)) {
                                                echo "<hr><b><center>Histórico do livro</center> </b>";
                                                foreach ($historico as $v) {
                                                    if (!empty($v[2])) {
                                                        $usuarioHistorico = UsuarioController::mostrarUsuariosPorId($v[1]);
                                                        echo "<b>Utilizado por: </b>" . $usuarioHistorico[1] . "<br>";
                                                        echo "<b>Email: </b>" . $usuarioHistorico[2] . "<br>";
                                                        echo "<b>Data inicial: </b>" . $v[3] . "<br>";
                                                        echo "<b>Data da entrega: </b>" . $v[2] . "<br>";
                                                        echo "<b>Data limite da entrega: </b>" . $v[4] . "<br><br>";
                                                    }
                                                }
                                            }
                                            ?>
                                        </div>
                                        <div class="col s4 right">
                                            <?php if (isset($livro[11]) && !empty($livro[11])) {
                                                ?>
                                                <h4 class="center">Capa do livro</h4>
                                                <img src="../upload/<?php echo $livro[11]; ?>" class="responsive-img" style="height: 250px; border: 1px solid grey"/>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="modal-content">
                                                    <h4 class="center blue-grey-text">Não encontramos nenhuma imagem da capa deste livro.</h4>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <a href="" class="modal-close waves-effect waves-green btn-flat">Fechar</a>
                                </div>
                            </div>

                            <?php
                        endforeach;
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <?php
        else:
            echo "<br><br><h1 class='center blue-grey-text'>Nenhum resultado foi encontrado.</h1><br><br>";
            echo "<h3 class='center blue-grey-text'>Não há livros disponibilizados por você.</h3><br><br>";
        endif;
        ?>


        <div class="row center">

            <?php
            if (isset($array)) {
                if (isset($_GET["pesquisa"])) {

                    if ($_SESSION["countPesquisa"] > 0) {
                        $numero = ($_SESSION["countPesquisa"] - 0.1) / 5;
                        $numeroPagina = floor(($_SESSION["countPesquisa"] - 0.1) / 5);
                    } else {
                        $numero = 1;
                        $numeroPagina = 1;
                    }

                    if ($numero > 1) {
                        ?>
                        <ul class="pagination">
                            <?php if ($_GET["pesquisa"] == 1) { ?>
                                <li class="disabled"><i class="material-icons">chevron_left</i></li>
                            <?php } else { ?>
                                <li class="waves-effect"><a href="telaSeusLivros.php?pesquisa=<?= $_GET["pesquisa"] - 1; ?>"><i class="material-icons">chevron_left</i></a></li>
                            <?php } ?>

                            <?php
                            for ($x = 0; $x <= $numeroPagina; $x++) {
                                if ($_GET["pesquisa"] == $x + 1) {
                                    ?>
                                    <li class="active"><a href="#!"><?= $x + 1; ?></a></li>
                                <?php } else { ?>
                                    <li class="waves-effect"><a href="telaSeusLivros.php?pesquisa=<?= $x + 1; ?>"><?= $x + 1; ?></a></li>
                                    <?php
                                }
                            }
                            if ($numeroPagina + 1 == $_GET["pesquisa"]) {
                                ?>
                                <li class="disabled"><i class="material-icons">chevron_right</i></li>
                            <?php } else { ?>
                                <li class="waves-effect"><a href="telaSeusLivros.php?pesquisa=<?= $_GET["pesquisa"] + 1; ?>"><i class="material-icons">chevron_right</i></a></li>
                            <?php } ?>
                        </ul>
                        <?php
                    }
                } else {


                    //verifica numero de paginas. Se não houver categorias, não posso fazer 0/10, pois haverá uma página;
                    if (count($seusLivros) > 0) {
                        $numero = ((count($seusLivros) - 0.1) / 5);
                        $numeroPagina = floor((count($seusLivros) - 0.1) / 5);
                    } else {
                        $numero = 1;
                        $numeroPagina = 1;
                    }
                    //echo $numeroPagina;
                    if ($numero > 1) {
                        ?>

                        <ul class="pagination">
                            <?php if ($pagina == 1) { ?>
                                <li class="disabled"><i class="material-icons">chevron_left</i></li>
                            <?php } else { ?>
                                <li class="waves-effect"><a href="telaSeusLivros.php?pagina=<?= $pagina - 1; ?>"><i class="material-icons">chevron_left</i></a></li>
                            <?php } ?>

                            <?php
                            for ($x = 0; $x <= $numeroPagina; $x++) {
                                if ($pagina == $x + 1) {
                                    ?>
                                    <li class="active"><a href="#!"><?= $x + 1; ?></a></li>
                                <?php } else { ?>
                                    <li class="waves-effect"><a href="telaSeusLivros.php?pagina=<?= $x + 1; ?>"><?= $x + 1; ?></a></li>
                                    <?php
                                }
                            }
                            if ($numeroPagina + 1 == $pagina) {
                                ?>
                                <li class="disabled"><i class="material-icons">chevron_right</i></li>
                            <?php } else { ?>
                                <li class="waves-effect"><a href="telaSeusLivros.php?pagina=<?= $pagina + 1; ?>"><i class="material-icons">chevron_right</i></a></li>
                                <?php } ?>
                        </ul>

                        <?php
                    }
                }
            }
            ?>
        </div>    



        <?php
        if (!empty($_SESSION['sweet']) && isset($_SESSION['sweet'])) {
            if ($_SESSION['sweet'] == "Livro excluído com sucesso.") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'O livro não pertence mais a esta associação!', 'success');</script>";
                $_SESSION['sweet'] = null;
            }

            if ($_SESSION['sweet'] == "O empréstimo do livro foi recusado!") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', '', 'success');</script>";
                $_SESSION['sweet'] = null;
            }

            if ($_SESSION['sweet'] == "A devolução do livro foi concluída.") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'Parabéns por emprestar o seu livro!', 'success');</script>";
                $_SESSION['sweet'] = null;
            }

            if ($_SESSION['sweet'] == "Livro editado com sucesso.") {
                echo "<script> swal('" . $_SESSION['sweet'] . "', 'O livro foi modificado e está novamente na lista!', 'success');</script>";
                $_SESSION['sweet'] = null;
            }
        }
        ?>



    </body>
</html>
